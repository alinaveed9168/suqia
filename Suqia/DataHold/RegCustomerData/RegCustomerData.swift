//
//  Data.swift
//
//  Created by Union Coop on 6/16/19
//  Copyright (c) . All rights reserved.
//
//
//

import Foundation
import ObjectMapper
import SwiftyJSON

@objcMembers class RegCustomerData: NSObject, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let staffID = "staffID"
    static let points = "Points"
    static let language = "Language"
    static let cardStatus = "CardStatus"
    static let customerNo = "CustomerNo"
    static let emirate = "Emirate"
    static let phoneNo = "PhoneNo"
    static let address = "Address"
    static let password = "Password"
    static let sSID = "SSID"
    static let registeredCustomerId = "RegisteredCustomerId"
    static let loginB4 = "LoginB4"
    static let gender = "Gender"
    static let validEmail = "ValidEmail"
    static let fName = "FName"
    static let totalPurchase = "TotalPurchase"
    static let familyNo = "FamilyNo"
    static let savedPercentage = "SavedPercentage"
    static let cardNo = "CardNo"
    static let pOBox = "POBox"
    static let recFlag = "recFlag"
    static let mobileNo = "MobileNo"
    static let userID = "UserID"
    static let nationality = "Nationality"
    static let discountAmt = "DiscountAmt"
    static let sHNO = "SHNO"
    static let lName = "LName"
    
    static let  faceTouchUniqueID = "faceTouchUniqueID"
    static let  isStaff = "isStaff"
    static let  dob = "dob"
    static let  discountAmount = "discountAmount"
    static let  taxRegNo = "taxRegNo"
    static let  mobileNoWithMask = "mobileNoWithMask"
    static let  email = "email"
    static let  token = "token"
    static let  redemptionAmount = "redemptionAmount"
    static let  shAddressNo = "shAddressNo"
    
    
    static let isEmpRegisteredForTAS = "isEmpRegisteredForTAS";
    static let isEmpRegisteredForTASCurrentDevice = "isEmpRegisteredForTASCurrentDevice";
    static let enableTAS = "enableTAS";
  }

  // MARK: Properties
  var email: String?
 var points: String?
  var language: String?
  var cardStatus: String?
  var customerNo: String?
  var emirate: String?
  var phoneNo: String?
  var address: String?
  var password: String?
  var sSID: String?
  var registeredCustomerId: String?
  var loginB4: String?
  var gender: String?
  var validEmail: String?
  var fName: String?
  var totalPurchase: String?
  var familyNo: String?
  var savedPercentage: String?
  var cardNo: String?
  var pOBox: String?
  var recFlag: String?
  var mobileNo: String?
  var userID: String?
  var nationality: String?
  //var discountAmt: String?
  var sHNO: String?
  var lName: String?
    
    var faceTouchUniqueID : String?
    var isStaff : String?
    var dob : String?
    var discountAmount : String?
    var taxRegNo : String?
    var mobileNoWithMask : String?
    var token : String?
    var redemptionAmount : String?
    var shAddressNo : String?
    var staffID : String?
    
    var enableTAS : Bool?
    var isEmpRegisteredForTASCurrentDevice : Bool?
    var isEmpRegisteredForTAS : Bool?
    
    
    init(  email: String,
           points: String,
           language: String,
           cardStatus: String,
           customerNo: String,
           emirate: String,
           phoneNo: String,
           address: String,
           password: String,
           sSID: String,
           registeredCustomerId: String,
           loginB4: String,
           gender: String,
           validEmail: String,
           fName: String,
           totalPurchase: String,
           familyNo: String,
           savedPercentage: String,
           cardNo: String,
           pOBox: String,
           recFlag: String,
           mobileNo: String,
           userID: String,
           nationality: String,
           discountAmt: String,
           sHNO: String,
           lName: String,
           faceTouchUniqueID : String,
           isStaff : String,
            dob : String,
            discountAmount : String,
            taxRegNo : String,
            mobileNoWithMask : String,
            token : String,
            redemptionAmount : String,
            shAddressNo : String,
            staffID : String,
            enableTAS : Bool,
            isEmpRegisteredForTASCurrentDevice : Bool,
            isEmpRegisteredForTAS  : Bool) {
            
       
           self.email    = email
            self.points    = points
            self.language    = language
            self.cardStatus    = cardStatus
            self.customerNo    = customerNo
            self.emirate    = emirate
            self.phoneNo    = phoneNo
            self.address    = address
            self.password    = password
            self.sSID    = sSID
            self.registeredCustomerId    = registeredCustomerId
            self.loginB4    = loginB4
            self.gender    = gender
            self.validEmail    = validEmail
            self.fName    = fName
            self.totalPurchase    = totalPurchase
            self.familyNo    = familyNo
            self.savedPercentage    = savedPercentage
            self.cardNo    = cardNo
            self.pOBox    = pOBox
            self.recFlag    = recFlag
            self.mobileNo    = mobileNo
            self.userID    = userID
            self.nationality    = nationality
//            self.discountAmt    = discountAmt
            self.sHNO    = sHNO
            self.lName    =  lName
        
           self.faceTouchUniqueID = faceTouchUniqueID
           self.isStaff = isStaff
           self.dob = dob
           self.discountAmount = discountAmount
           self.taxRegNo = taxRegNo
           self.mobileNoWithMask = mobileNoWithMask
           self.email = email
           self.token = token
           self.redemptionAmount = redemptionAmount
           self.shAddressNo = shAddressNo
           self.staffID = staffID
        
            self.enableTAS = enableTAS
            self.isEmpRegisteredForTASCurrentDevice = isEmpRegisteredForTASCurrentDevice
            self.isEmpRegisteredForTAS = isEmpRegisteredForTAS
        
    }

    override init(){
        
    }
  // MARK: NSCoding Protocol
  required  init(coder aDecoder: NSCoder) {

    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.points = aDecoder.decodeObject(forKey: SerializationKeys.points) as? String
    self.language = aDecoder.decodeObject(forKey: SerializationKeys.language) as? String
    self.cardStatus = aDecoder.decodeObject(forKey: SerializationKeys.cardStatus) as? String
    self.customerNo = aDecoder.decodeObject(forKey: SerializationKeys.customerNo) as? String
    self.emirate = aDecoder.decodeObject(forKey: SerializationKeys.emirate) as? String
    self.phoneNo = aDecoder.decodeObject(forKey: SerializationKeys.phoneNo) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
    self.sSID = aDecoder.decodeObject(forKey: SerializationKeys.sSID) as? String
    self.registeredCustomerId = aDecoder.decodeObject(forKey: SerializationKeys.registeredCustomerId) as? String
    self.loginB4 = aDecoder.decodeObject(forKey: SerializationKeys.loginB4) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? String
    self.validEmail = aDecoder.decodeObject(forKey: SerializationKeys.validEmail) as? String
    self.fName = aDecoder.decodeObject(forKey: SerializationKeys.fName) as? String
    self.totalPurchase = aDecoder.decodeObject(forKey: SerializationKeys.totalPurchase) as? String
    self.familyNo = aDecoder.decodeObject(forKey: SerializationKeys.familyNo) as? String
    self.savedPercentage = aDecoder.decodeObject(forKey: SerializationKeys.savedPercentage) as? String
    self.cardNo = aDecoder.decodeObject(forKey: SerializationKeys.cardNo) as? String
    self.pOBox = aDecoder.decodeObject(forKey: SerializationKeys.pOBox) as? String
    self.recFlag = aDecoder.decodeObject(forKey: SerializationKeys.recFlag) as? String
    self.mobileNo = aDecoder.decodeObject(forKey: SerializationKeys.mobileNo) as? String
    self.userID = aDecoder.decodeObject(forKey: SerializationKeys.userID) as? String
    self.nationality = aDecoder.decodeObject(forKey: SerializationKeys.nationality) as? String
//    self.discountAmt = aDecoder.decodeObject(forKey: SerializationKeys.discountAmt) as? String
    self.sHNO = aDecoder.decodeObject(forKey: SerializationKeys.sHNO) as? String
    self.lName = aDecoder.decodeObject(forKey: SerializationKeys.lName) as? String
    
    self.faceTouchUniqueID = aDecoder.decodeObject(forKey: SerializationKeys.faceTouchUniqueID) as? String
    self.isStaff = aDecoder.decodeObject(forKey: SerializationKeys.isStaff) as? String
    self.dob = aDecoder.decodeObject(forKey: SerializationKeys.dob) as? String
    self.discountAmount = aDecoder.decodeObject(forKey: SerializationKeys.discountAmt) as? String
    self.taxRegNo = aDecoder.decodeObject(forKey: SerializationKeys.taxRegNo) as? String
    self.mobileNoWithMask = aDecoder.decodeObject(forKey: SerializationKeys.mobileNoWithMask) as? String
    self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
    self.redemptionAmount = aDecoder.decodeObject(forKey: SerializationKeys.redemptionAmount) as? String
    self.shAddressNo = aDecoder.decodeObject(forKey: SerializationKeys.shAddressNo) as? String
    self.staffID = aDecoder.decodeObject(forKey: SerializationKeys.staffID) as? String
    
    self.enableTAS = aDecoder.decodeBool(forKey: SerializationKeys.enableTAS)
    self.isEmpRegisteredForTASCurrentDevice = aDecoder.decodeBool(forKey: SerializationKeys.isEmpRegisteredForTASCurrentDevice)
    self.isEmpRegisteredForTAS = aDecoder.decodeBool(forKey: SerializationKeys.isEmpRegisteredForTAS)
  }

   func encode(with aCoder: NSCoder) {
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(points, forKey: SerializationKeys.points)
    aCoder.encode(language, forKey: SerializationKeys.language)
    aCoder.encode(cardStatus, forKey: SerializationKeys.cardStatus)
    aCoder.encode(customerNo, forKey: SerializationKeys.customerNo)
    aCoder.encode(emirate, forKey: SerializationKeys.emirate)
    aCoder.encode(phoneNo, forKey: SerializationKeys.phoneNo)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(password, forKey: SerializationKeys.password)
    aCoder.encode(sSID, forKey: SerializationKeys.sSID)
    aCoder.encode(registeredCustomerId, forKey: SerializationKeys.registeredCustomerId)
    aCoder.encode(loginB4, forKey: SerializationKeys.loginB4)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(validEmail, forKey: SerializationKeys.validEmail)
    aCoder.encode(fName, forKey: SerializationKeys.fName)
    aCoder.encode(totalPurchase, forKey: SerializationKeys.totalPurchase)
    aCoder.encode(familyNo, forKey: SerializationKeys.familyNo)
    aCoder.encode(savedPercentage, forKey: SerializationKeys.savedPercentage)
    aCoder.encode(cardNo, forKey: SerializationKeys.cardNo)
    aCoder.encode(pOBox, forKey: SerializationKeys.pOBox)
    aCoder.encode(recFlag, forKey: SerializationKeys.recFlag)
    aCoder.encode(mobileNo, forKey: SerializationKeys.mobileNo)
    aCoder.encode(userID, forKey: SerializationKeys.userID)
    aCoder.encode(nationality, forKey: SerializationKeys.nationality)
//    aCoder.encode(discountAmt, forKey: SerializationKeys.discountAmt)
    aCoder.encode(sHNO, forKey: SerializationKeys.sHNO)
    aCoder.encode(lName, forKey: SerializationKeys.lName)
    
    aCoder.encode(faceTouchUniqueID , forKey:SerializationKeys.faceTouchUniqueID)
    aCoder.encode(isStaff , forKey:SerializationKeys.isStaff)
    aCoder.encode(dob ,forKey: SerializationKeys.dob)
    aCoder.encode(discountAmount , forKey:SerializationKeys.discountAmt)
    aCoder.encode(taxRegNo , forKey:SerializationKeys.taxRegNo)
    aCoder.encode(mobileNoWithMask ,forKey: SerializationKeys.mobileNoWithMask)
    aCoder.encode(token , forKey:SerializationKeys.token)
    aCoder.encode(redemptionAmount , forKey:SerializationKeys.redemptionAmount)
    aCoder.encode(shAddressNo , forKey:SerializationKeys.shAddressNo)
    aCoder.encode(staffID , forKey:SerializationKeys.staffID)
    
    aCoder.encode(enableTAS , forKey:SerializationKeys.enableTAS)
    aCoder.encode(isEmpRegisteredForTASCurrentDevice , forKey:SerializationKeys.isEmpRegisteredForTASCurrentDevice)
    aCoder.encode(isEmpRegisteredForTAS , forKey:SerializationKeys.isEmpRegisteredForTAS)
    
    
  }

    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        email = json[SerializationKeys.email].string
        points = json[SerializationKeys.points].string
        language = json[SerializationKeys.language].string
        cardStatus = json[SerializationKeys.cardStatus].string
        customerNo = json[SerializationKeys.customerNo].string
        emirate = json[SerializationKeys.emirate].string
        phoneNo = json[SerializationKeys.phoneNo].string
        address = json[SerializationKeys.address].string
        password = json[SerializationKeys.password].string
        sSID = json[SerializationKeys.sSID].string
        registeredCustomerId = json[SerializationKeys.registeredCustomerId].string
        loginB4 = json[SerializationKeys.loginB4].string
        gender = json[SerializationKeys.gender].string
        validEmail = json[SerializationKeys.validEmail].string
        fName = json[SerializationKeys.fName].string
        totalPurchase = json[SerializationKeys.totalPurchase].string
        familyNo = json[SerializationKeys.familyNo].string
        savedPercentage = json[SerializationKeys.savedPercentage].string
        cardNo = json[SerializationKeys.cardNo].string
        pOBox = json[SerializationKeys.pOBox].string
        recFlag = json[SerializationKeys.recFlag].string
        mobileNo = json[SerializationKeys.mobileNo].string
        userID = json[SerializationKeys.userID].string
        nationality = json[SerializationKeys.nationality].string
//        discountAmt = json[SerializationKeys.discountAmt].string
        sHNO = json[SerializationKeys.sHNO].string
        lName = json[SerializationKeys.lName].string
        
        faceTouchUniqueID = json[SerializationKeys.faceTouchUniqueID].string
        isStaff = json[SerializationKeys.isStaff].string
        dob = json[SerializationKeys.dob].string
        discountAmount = json[SerializationKeys.discountAmt].string
        taxRegNo = json[SerializationKeys.taxRegNo].string
        mobileNoWithMask = json[SerializationKeys.mobileNoWithMask].string
        token = json[SerializationKeys.token].string
        redemptionAmount = json[SerializationKeys.redemptionAmount].string
        shAddressNo = json[SerializationKeys.shAddressNo].string
        staffID = json[SerializationKeys.staffID].string
        
        enableTAS = json[SerializationKeys.enableTAS].bool
        isEmpRegisteredForTASCurrentDevice = json[SerializationKeys.isEmpRegisteredForTASCurrentDevice].bool
        isEmpRegisteredForTAS = json[SerializationKeys.isEmpRegisteredForTAS].bool

    }
    
    func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = points { dictionary[SerializationKeys.points] = value }
        if let value = language { dictionary[SerializationKeys.language] = value }
        if let value = cardStatus { dictionary[SerializationKeys.cardStatus] = value }
        if let value = customerNo { dictionary[SerializationKeys.customerNo] = value }
        if let value = emirate { dictionary[SerializationKeys.emirate] = value }
        if let value = phoneNo { dictionary[SerializationKeys.phoneNo] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = password { dictionary[SerializationKeys.password] = value }
        if let value = sSID { dictionary[SerializationKeys.sSID] = value }
        if let value = registeredCustomerId { dictionary[SerializationKeys.registeredCustomerId] = value }
        if let value = loginB4 { dictionary[SerializationKeys.loginB4] = value }
        if let value = gender { dictionary[SerializationKeys.gender] = value }
        if let value = validEmail { dictionary[SerializationKeys.validEmail] = value }
        if let value = fName { dictionary[SerializationKeys.fName] = value }
        if let value = totalPurchase { dictionary[SerializationKeys.totalPurchase] = value }
        if let value = familyNo { dictionary[SerializationKeys.familyNo] = value }
        if let value = savedPercentage { dictionary[SerializationKeys.savedPercentage] = value }
        if let value = cardNo { dictionary[SerializationKeys.cardNo] = value }
        if let value = pOBox { dictionary[SerializationKeys.pOBox] = value }
        if let value = recFlag { dictionary[SerializationKeys.recFlag] = value }
        if let value = mobileNo { dictionary[SerializationKeys.mobileNo] = value }
        if let value = userID { dictionary[SerializationKeys.userID] = value }
        if let value = nationality { dictionary[SerializationKeys.nationality] = value }
       // if let value = discountAmt { dictionary[SerializationKeys.discountAmt] = value }
        if let value = sHNO { dictionary[SerializationKeys.sHNO] = value }
        if let value = lName { dictionary[SerializationKeys.lName] = value }
        
        if let value = faceTouchUniqueID  { dictionary[SerializationKeys.faceTouchUniqueID] = value }
        if let value = isStaff  { dictionary[SerializationKeys.isStaff] = value }
        if let value = dob  { dictionary[SerializationKeys.dob] = value }
        if let value = discountAmount  { dictionary[SerializationKeys.discountAmt] = value }
        if let value = taxRegNo  { dictionary[SerializationKeys.taxRegNo] = value }
        if let value = mobileNoWithMask  { dictionary[SerializationKeys.mobileNoWithMask] = value }
        if let value = token  { dictionary[SerializationKeys.token] = value }
        if let value = redemptionAmount  { dictionary[SerializationKeys.redemptionAmount] = value }
        if let value = shAddressNo  { dictionary[SerializationKeys.shAddressNo] = value }
        if let value = staffID  { dictionary[SerializationKeys.staffID] = value }
        
        if let value = enableTAS  { dictionary[SerializationKeys.enableTAS] = value }
        if let value = isEmpRegisteredForTASCurrentDevice  { dictionary[SerializationKeys.isEmpRegisteredForTASCurrentDevice] = value }
        if let value = isEmpRegisteredForTAS  { dictionary[SerializationKeys.isEmpRegisteredForTAS] = value }

        return dictionary
    }
    
}
