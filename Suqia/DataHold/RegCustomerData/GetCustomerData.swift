//
//  RegCustomerData.swift
//
//  Created by Union Coop on 6/18/19
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

class GetCustomerData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let msg = "Msg"
    static let result = "Result"
    static let data = "Data"
  }

  // MARK: Properties
     var msg: String?
     var result: Int?
     var data: [RegCustomerData]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
     convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
     required init(json: JSON) {
    msg = json[SerializationKeys.msg].string
    result = json[SerializationKeys.result].int
    if let items = json[SerializationKeys.data].array { data = items.map { RegCustomerData(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
     func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = msg { dictionary[SerializationKeys.msg] = value }
    if let value = result { dictionary[SerializationKeys.result] = value }
    if let value = data { dictionary[SerializationKeys.data] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required    init(coder aDecoder: NSCoder) {
    self.msg = aDecoder.decodeObject(forKey: SerializationKeys.msg) as? String
    self.result = aDecoder.decodeObject(forKey: SerializationKeys.result) as? Int
    self.data = aDecoder.decodeObject(forKey: SerializationKeys.data) as? [RegCustomerData]
  }

     func encode(with aCoder: NSCoder) {
    aCoder.encode(msg, forKey: SerializationKeys.msg)
    aCoder.encode(result, forKey: SerializationKeys.result)
    aCoder.encode(data, forKey: SerializationKeys.data)
  }

}
