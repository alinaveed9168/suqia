/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

struct LoginUserDataModel : Mappable {
    var staffID : String?
    var faceTouchUniqueID : String?
    var language : String?
    var isStaff : Bool?
    var dob : String?
    var customerNo : Int?
    var discountAmount : String?
    var taxRegNo : String?
    var mobileNoWithMask : String?
    var fName : String?
    var gender : String?
    var mobileNo : String?
    var email : String?
    var token : String?
    var points : String?
    var shno : String?
    var totalPurchase : String?
    var savedPercentage : String?
    var nationalityAName : String?
    var nationalityEName : String?
    var nationality : String?
    var cardNo : String?
    var redemptionAmount : String?
    var lName : String?
    var shAddressNo : String?
    var cardStatus : String?
    var userID : String?
//    var addressList : [AddressList]?
//    var cart_shipping : Cart_shipping?

    var authToken : String?
    var allowToUpdateEmail : Bool?
    var searchValue : String?
    var searchValueType : String?

    var isEmpRegisteredForTAS : Bool?
    var isEmpRegisteredForTASCurrentDevice : Bool?
    var enableTAS : Bool?

    
	init?(map: Map) {

	}

    init(){
        
    }
	mutating func mapping(map: Map) {

        staffID <- map["staffID"]
        faceTouchUniqueID <- map["faceTouchUniqueID"]
        language <- map["language"]
        isStaff <- map["isStaff"]
        dob <- map["dob"]
        customerNo <- map["customerNo"]
        discountAmount <- map["discountAmount"]
        
        taxRegNo <- map["taxRegNo"]
        mobileNoWithMask <- map["mobileNoWithMask"]
        fName <- map["fName"]
        gender <- map["gender"]
        mobileNo <- map["mobileNo"]
        email <- map["email"]
        token <- map["token"]
        points <- map["points"]
        shno <- map["shno"]
        totalPurchase <- map["totalPurchase"]
        savedPercentage <- map["savedPercentage"]
        nationalityEName <- map["nationalityEName"]
        nationalityAName <- map["nationalityAName"]
        nationality <- map["nationality"]
        cardNo <- map["cardNo"]
        redemptionAmount <- map["redemptionAmount"]
        lName <- map["lName"]
        shAddressNo <- map["shAddressNo"]
        cardStatus <- map["cardStatus"]
        userID <- map["userID"]
        searchValue <- map["searchValue"]
        searchValueType <- map["searchValueType"]

        authToken <- map["authToken"]
        allowToUpdateEmail <- map["allowToUpdate"]
//        cart_shipping <- map["cart_shipping"]
//        addressList <- map["addressList"]
        
        isEmpRegisteredForTAS <- map["isEmpRegisteredForTAS"]
        isEmpRegisteredForTASCurrentDevice <- map["isEmpRegisteredForTASCurrentDevice"]
        enableTAS <- map["enableTAS"]


	}

}
