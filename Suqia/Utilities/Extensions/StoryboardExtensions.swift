//
//  Extensions.swift
//  iTalk
//
//  Created by Manjeet's MAC on 09/08/20.
//  Copyright © 2020 Manjeet's MAC. All rights reserved.
//

import UIKit
import Foundation

enum AppStoryboard : String {
    
    case mainStoryboard = "Main"
    case userStoryboard = "User"
    case generalStoryboard = "General"
    case shStoryboard = "Shareholder"
    case tasStoryboard = "TAS"  
    
    
    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")}
        return scene
    }
    func initialViewController() -> UIViewController? {return instance.instantiateInitialViewController()}
}

extension UIViewController {
    class var storyboardID : String { return "\(self)"}
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {return appStoryboard.viewController(viewControllerClass: self)}
}

extension UINib {
    func instantiate() -> Any? {
        return self.instantiate(withOwner: nil, options: nil).first
    }
}
