//
//  UIViewExtension.swift
//  WeMinder
//
//  Created by Krishna on 21/05/19.
//  Copyright © 2019 Krishna All rights reserved.
//

import UIKit

extension UIView {
    
    func addCornerRadiusWithShadow(color: UIColor, borderColor: UIColor, cornerRadius: CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = cornerRadius
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 0.5
        self.layer.masksToBounds = false
    }
 
    func setCornerRadiusWith(radius: Float, borderWidth: Float, borderColor: UIColor) {
        self.layer.cornerRadius = CGFloat(radius)
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
        self.layer.masksToBounds = true
    }
    
    func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
      let tap = UITapGestureRecognizer(target: target, action: action)
      tap.numberOfTapsRequired = tapNumber
      addGestureRecognizer(tap)
      isUserInteractionEnabled = true
    }
}

// MARK: - Extension of UIView For adding the border to UIView at any position.
extension UIView {
    
    /// A Enum UIPosition Describes the Different Postions.
    ///
    /// - top: Will add border at top of The UIView.
    /// - left: Will add border at left of The UIView.
    /// - bottom: Will add border at bottom of The UIView.
    /// - right: Will add border at right of The UIView.
    enum UIPosition {
        case top
        case left
        case bottom
        case right
    }
    
    /// This method is used to add the border to perticular UIView at any position.
    ///
    /// - Parameters:
    ///   - position: Pass the enum value of UIPosition , according to the enum value it will add the border for that position.
    ///   - color: Pass the UIColor which you want to see in a border.
    ///   - width: CGFloat value - (Optional - if you are passing nil then method will automatically set this value same as Parent's width) OR pass how much width you want for border. For top and bottom UIPosition you can pass nil.
    ///   - height: CGFloat value - (Optional - if you are passing nil then method will automatically set this value same as Parent's height) OR pass how much height you want for border. For left and right UIPosition you can pass nil.
    func addBorder(position:UIPosition , color:UIColor , width:CGFloat? , height:CGFloat?) {
        
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
        
        switch position {
            
        case .top:
            
            borderLayer.frame = CGRect(origin: .zero, size: CGSize(width: (width ?? self.CViewWidth), height: (height ?? 0.0)))
            
        case .left:
            
            borderLayer.frame = CGRect(origin: .zero, size: CGSize(width: (width ?? 0.0), height: (height ?? self.CViewHeight)))
            
        case .bottom:
            
            borderLayer.frame = CGRect(origin: CGPoint(x: 0.0, y: (self.CViewHeight - (height ?? 0.0))), size: CGSize(width: (width ?? self.CViewWidth), height: (height ?? 0.0)))
            
        case .right:
            
            borderLayer.frame = CGRect(origin: CGPoint(x: (self.CViewWidth - (width ?? 0.0)), y: 0.0), size: CGSize(width: (width ?? 0.0), height: (height ?? self.CViewHeight)))
            
        }
        
        self.layer.addSublayer(borderLayer)
    }
    
}

extension UIView {
    
    var CViewSize:CGSize {
        return self.frame.size
    }
    
    var CViewOrigin:CGPoint {
        return self.frame.origin
    }
    
    var CViewWidth:CGFloat {
        return self.CViewSize.width
    }
    
    var CViewHeight:CGFloat {
        return self.CViewSize.height
    }
    
    var CViewX:CGFloat {
        return self.CViewOrigin.x
    }
    
    var CViewY:CGFloat {
        return self.CViewOrigin.y
    }
    
    var CViewCenter:CGPoint {
        return CGPoint(x: self.CViewWidth/2.0, y: self.CViewHeight/2.0)
    }
    
    var CViewCenterX:CGFloat {
        return CViewCenter.x
    }
    
    var CViewCenterY:CGFloat {
        return CViewCenter.y
    }
    
}
