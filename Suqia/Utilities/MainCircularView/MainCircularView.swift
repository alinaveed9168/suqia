//
//  ucsCustomView.swift
//  unionCoop
//
//  Created by Union Coop on 6/19/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import MBCircularProgressBar
@IBDesignable
@objcMembers class MainCircularView: UIView {
    let kCONTENT_XIB_NAME = "MainCircularView"
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var navView: UIView!
   public var viewcontroller: UINavigationController!
    @IBOutlet weak var progressBar: MBCircularProgressBarView!
    
  
    
    var view = UIView()
    
    
//    
//    @IBInspectable public var hideBell: Bool = false {
//        didSet {
//            self.bellArrow.isHidden = hideBell
//        }
//    }
//    
//    @IBInspectable public var hideBack: Bool = false {
//        didSet {
//            self.bckArrow.isHidden = hideBack
//        }
//    }
//    
//    @IBInspectable public var hideSearch: Bool = false {
//        didSet {
//            self.searchArrow.isHidden = hideSearch
//        }
//    }
//    
//    @IBInspectable public var hideHome: Bool = false {
//        didSet {
//            self.homeArrow.isHidden = hideHome
//        }
//    }
//    
    
    
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        initialConfig()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        initialConfig()
    }
    
    private func initialConfig() {
        
//        navView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        
//        UIView.animate(withDuration: 2, delay: 0.3, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
//            self.progressBar.value = 100.0 - self.progressBar.value;
//
//        }, completion: nil)
    }
    
    
    // MARK: - Nib handlers
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        //'UIViewAutoresizing' has been renamed to 'UIView.AutoresizingMask'
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: kCONTENT_XIB_NAME, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        //setup your view with the settings
    }
    
}
