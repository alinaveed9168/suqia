//
//  Macros.h
//

#define MAIN_STORYBOARD (([LANG_INT boolValue]) ? @"Main" : @"Main")


// App Information
#define AppName                 [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"]
#define AppVersion              [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
//#define AppDelegate(type)       ((type *)[[UIApplication sharedApplication] delegate])
#define NSAppDelegate(type)     ((type *)[[NSApplication sharedApplication] delegate])
#define SharedApp               [UIApplication sharedApplication]
#define NSSharedApp             [NSApplication sharedApplication]
#define Bundle                  [NSBundle mainBundle]
#define MainScreen              [UIScreen mainScreen]
#define DEVICE_FRAME [UIScreen mainScreen].bounds

#define IS_IPHONE        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_IPHONE_5      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS  (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_IPHONE_X      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 812.0)
#define IS_RETINA        ([[UIScreen mainScreen] scale] == 2.0)

#define IS_IPAD_DEVICE   [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"]

#define DEVICE_HEIGHT [UIScreen mainScreen].bounds.size.height
#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width

#define DO_NOT_SHOW_HELP @"DO_NOT_SHOW_HELP"
#define DO_NOT_Prefered_Language @"DO_NOT_Prefered_Language"


#define handle_tap(view, delegate, selector) do {\
view.userInteractionEnabled = YES;\
[view addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget:delegate action:selector]];\
} while(0)


// OS Information
#define IOS_5_OR_LATER [[[UIDevice currentDevice] systemVersion] compare:@"5.0" options:NSNumericSearch] != NSOrderedAscending
#define IOS_4_OR_LATER [[[UIDevice currentDevice] systemVersion] compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending
#define ONLY_IF_AT_LEAST_IOS_4(action) if ([[[UIDevice currentDevice] systemVersion] compare:@"4.0" options:NSNumericSearch] != NSOrderedAscending) { action; }
#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define IS_IPHONE6 (([[UIScreen mainScreen] bounds].size.height-667)?NO:YES)
#define IS_IPHONE6_PLUS (([[UIScreen mainScreen] bounds].size.height-736)?NO:YES)
#define CALENDAR_VIEW_IS_OS_7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// Actions
#define OpenURL(urlString) [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:urlString]]

// Preferences

#define UDObject(x,y)           [[NSUserDefaults standardUserDefaults] setObject:(y) forKey:(x)]
#define UDValue(x)              [[NSUserDefaults standardUserDefaults] valueForKey:(x)]
#define UDBool(x)               [[NSUserDefaults standardUserDefaults] boolForKey:(x)]
#define UDInteger(x)            [[NSUserDefaults standardUserDefaults] integerForKey:(x)]
#define UDSetValue(x, y)        [[NSUserDefaults standardUserDefaults] setValue:(y) forKey:(x)]
#define UDSetBool(x, y)         [[NSUserDefaults standardUserDefaults] setBool:(y) forKey:(x)]
#define UDSetInteger(x, y)      [[NSUserDefaults standardUserDefaults] setInteger:(y) forKey:(x)]
#define UDObserveValue(x, y)    [[NSUserDefaults standardUserDefaults] addObserver:y forKeyPath:x options:NSKeyValueObservingOptionOld context:nil];
#define UDRemoveKey(x)          [[NSUserDefaults standardUserDefaults] removeObjectForKey:(x)]
#define UDSync(ignored)         [[NSUserDefaults standardUserDefaults] synchronize]


// Debugging
#define StartTimer(ignored)     NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
#define EndTimer(msg)           NSTimeInterval stop = [NSDate timeIntervalSinceReferenceDate]; NSLog(@"%@", [NSString stringWithFormat:@"%@ Time = %f", msg, stop-start]);

// DISPLAY ALERT
#define ALERT_DIALOG(__title__,__message__)     UIAlertView *alert_Dialog = [[UIAlertView alloc] initWithTitle:(__title__) message:(__message__) delegate:nil cancelButtonTitle:AMLocalizedString(@"Ok",nil) otherButtonTitles:nil];\
[alert_Dialog show];\

#define NSDictionary(objects,keys)          [NSDictionary dictionaryWithObjects:objects forKeys:keys]

/* key, observer, object */
#define ObserveValue(x, y, z) [(z) addObserver:y forKeyPath:x options:NSKeyValueObservingOptionOld context:nil];

//#define AppColor [UIColor colorWithRed:255.0/255.0 green:80.0/255.0 blue:1.0/255.0 alpha:1]
#define AppColor [UIColor colorWithRed:51.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1]
#define AppNavigationColor [UIColor colorWithRed:124/255.0 green:117/255.0 blue:101/255.0 alpha:1]

// User Interface Related
#define HexColor(c)                         [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define NSHexColor(c)                       [NSColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define NSRGB(r, g, b)                      [NSColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define NSRGBA(r, g, b, a)                  [NSColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define ShowNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define SetNetworkActivityIndicator(x)      [UIApplication sharedApplication].networkActivityIndicatorVisible = x
#define NavBar                              self.navigationController.navigationBar
#define NavTitle                            self.navigationItem.title
#define TabBar                              self.tabBarController.tabBar
#define NavBarHeight                        self.navigationController.navigationBar.bounds.size.height
#define TabBarHeight                        self.tabBarController.tabBar.bounds.size.height
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define TouchHeightDefault                  44
#define TouchHeightSmall                    32
#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y

// Rect stuff
#define CGWidth(rect)                   rect.size.width
#define CGHeight(rect)                  rect.size.height
#define CGOriginX(rect)                 rect.origin.x
#define CGOriginY(rect)                 rect.origin.y
#define CGRectCenter(rect)              CGPointMake(NSOriginX(rect) + NSWidth(rect) / 2, NSOriginY(rect) + NSHeight(rect) / 2)
#define CGRectModify(rect,dx,dy,dw,dh)  CGRectMake(rect.origin.x + dx, rect.origin.y + dy, rect.size.width + dw, rect.size.height + dh)
#define NSLogRect(rect)                 NSLog(@"%@", NSStringFromCGRect(rect))
#define NSLogSize(size)                 NSLog(@"%@", NSStringFromCGSize(size))
#define NSLogPoint(point)               NSLog(@"%@", NSStringFromCGPoint(point))

#define TEXT_FEILD_BORDER_WIDTH 1.0f
#define TEXT_FEILD_CORNER_RADIUS 18.0f
#define BUTTON_CORNER_RADIUS 24.0f

#define LANG_ID [LANG_INT boolValue] ? @"1" : @"2"
#define THEME_RED_COLOR [UIColor colorWithHexString:@"aa1c18"]
#define TextFieldTHEME_COLOR [UIColor colorWithRed:RGB()]
#define NAVBAR_FONT [UIFont fontWithName:@"Roboto-Medium" size:18]

#define FUTURA_15 [UIFont fontWithName:@"Futura" size:15]
#define FUTURA_14 [UIFont fontWithName:@"Futura" size:14]
#define FUTURA_16 [UIFont fontWithName:@"Futura" size:16]
#define FUTURA_18 [UIFont fontWithName:@"Futura" size:18]

#define TITLE_FONT_17  [UIFont fontWithName:@"Futura MD BT" size:17]
#define TITLE_FONT_16  [UIFont fontWithName:@"Futura Md BT" size:16]
#define TITLE_FONT_AR_16 [UIFont fontWithName:@"Arial" size:16]

#define GSSMedium_15 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:15]
#define GSSMedium_16 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:16]
#define GSSMedium_17 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:17]
#define GSSMedium_18 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:18]
#define GSSMedium_19 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:19]
#define GSSMedium_22 [UIFont fontWithName:@"GESSTwoMedium-Medium" size:24]

#define GSSLight_15 [UIFont fontWithName:@"GESSTwoLight-Light" size:15]
#define GSSLight_12 [UIFont fontWithName:@"GESSTwoLight-Light" size:12]
#define GSSLight_14 [UIFont fontWithName:@"GESSTwoLight-Light" size:14]

#define CHECK_NULL_STRING(str) ([str isKindOfClass:[NSNull class]] || !str)?@"":str
#define ADNOC_BLUE [UIColor colorWithRed:35.0f/255.0f green:164.0f/255.0f blue:255.0f/255.0f alpha:1.0]
#define DoubleToNSstring(value) [NSString stringWithFormat:@"%.2f",value]

#define Log(fmt, ...) NSLog(fmt, ##__VA_ARGS__);

