//
//  LocationManager.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import CoreLocation

//-------------------------------------------------------------------------------------------------------------------------------------------------
class LocationManager: NSObject, CLLocationManagerDelegate {

	var locationManager: CLLocationManager?
	var coordinate = CLLocationCoordinate2D()

	//---------------------------------------------------------------------------------------------------------------------------------------------
	static let shared: LocationManager = {
		let instance = LocationManager()
		return instance
	} ()

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func start() {
		shared.locationManager?.startUpdatingLocation()
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func stop() {
		shared.locationManager?.stopUpdatingLocation()
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func latitude() -> CLLocationDegrees {
		return shared.coordinate.latitude
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	class func longitude() -> CLLocationDegrees {
		return shared.coordinate.longitude
	}

	// MARK: - Instance methods
	//---------------------------------------------------------------------------------------------------------------------------------------------
	override init() {

		super.init()

		locationManager = CLLocationManager()
        locationManager?.distanceFilter  = 20
		locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters
		locationManager?.requestWhenInUseAuthorization()
        locationManager?.delegate = self

//        LocationManager.start()
        
//        self.locationManager = [[CLLocationManager alloc] init];
//        self.locationManager.distanceFilter = 20;//kCLDistanceFilterNone; // whenever we move
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
//        self.locationManager.delegate = self;
//
//        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
//            [self.locationManager requestWhenInUseAuthorization];
//        }
//        [self.locationManager startUpdatingLocation];
        
        
	}

	// MARK: - CLLocationManagerDelegate
	//---------------------------------------------------------------------------------------------------------------------------------------------
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

		if let location = locations.last {
			coordinate = location.coordinate
		}
	}

	//---------------------------------------------------------------------------------------------------------------------------------------------
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {

	}
    
    func locationIsEnabled() -> Bool{
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    globalPrint(dataToPrint:"No access")
                    return false
                case .authorizedAlways, .authorizedWhenInUse:
                    globalPrint(dataToPrint:"Access")
                    return true
                @unknown default:
                    return false
                
            }
            } else {
                globalPrint(dataToPrint:"Location services are not enabled")
                return false
        }
    }
}
