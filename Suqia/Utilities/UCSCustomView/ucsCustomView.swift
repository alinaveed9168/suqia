//
//  ucsCustomView.swift
//  unionCoop
//
//  Created by Union Coop on 6/19/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
@IBDesignable public class ucsCustomView: UIView {

     @IBInspectable public var topLeft: Bool = false      { didSet { updateCorners() } }
     @IBInspectable public var topRight: Bool = false     { didSet { updateCorners() } }
     @IBInspectable public var bottomLeft: Bool = false   { didSet { updateCorners() } }
     @IBInspectable public var bottomRight: Bool = false  { didSet { updateCorners() } }
     @IBInspectable public var cornerRadius: CGFloat = 0  { didSet { updateCorners() } }

     public override init(frame: CGRect = .zero) {
         super.init(frame: frame)
         updateCorners()
     }

     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         updateCorners()
     }
    @IBInspectable var borderColor : UIColor = .green{
        didSet{self.layer.borderColor = borderColor.cgColor}
    }
  
    @IBInspectable var borderWidth : CGFloat = 0.0{
        didSet{self.layer.borderWidth = borderWidth}
    }
    
   @IBInspectable var addShadow:Bool = true{

       didSet(newValue) {
           if(newValue == true){
               //self.layer.masksToBounds = false
               self.layer.masksToBounds = true
               self.layer.shadowColor = UIColor.black.cgColor
               self.layer.shadowOpacity = 0.5
               self.layer.shadowOffset = CGSize(width: 2, height: 3)
               self.layer.shadowRadius = 3

               self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
               self.layer.shouldRasterize = true
               self.layer.rasterizationScale =  UIScreen.main.scale
            globalPrint(dataToPrint:"trying to use shadow")
            
           }
       }
   }
    
    @IBInspectable var customAddShadow:Bool = false{

        didSet(newValue) {
              if(newValue == true){
               self.layer.shadowColor = UIColor.lightGray.cgColor
                self.layer.shadowOffset = CGSize(width: 0, height: 3)
                self.layer.shadowOpacity = 1.0
                self.layer.shadowRadius = 15
                self.layer.cornerRadius = 15
                self.layer.borderColor = UIColor.black.cgColor
                self.layer.borderWidth = 1.0
                self.layer.masksToBounds = false
             
            }
        }
    }
    
    
    
}

private extension ucsCustomView {
    func updateCorners() {
        var corners = CACornerMask()

        if topLeft     { corners.formUnion(.layerMinXMinYCorner) }
        if topRight    { corners.formUnion(.layerMaxXMinYCorner) }
        if bottomLeft  { corners.formUnion(.layerMinXMaxYCorner) }
        if bottomRight { corners.formUnion(.layerMaxXMaxYCorner) }

        layer.maskedCorners = corners
        layer.cornerRadius = cornerRadius
    }
    
    
}

@IBDesignable public class ucsCustomButton: UIButton {

@objc @IBInspectable public var ucsCorner: CGFloat = 8.0 {

    didSet {
                layer.cornerRadius = ucsCorner
    }
  }

}

