//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation

class WizardConstant {
    
   static let  EMPTY_WIDGET = 0;
   static let  USER_INFO_WIDGET = 1;
   static let  OFFER_NEWS_WIDGET = 2;
   static let  BANNER_WIDGET = 3;
   static let  NEWS_WIDGET = 4;
   static let  SERVICE_WIDGET = 5;
   static let  BULK_WIDGET = 6;
   static let  GRID_FOUR_CATEGORY_WIDGET = 7;
   static let  TAB_PRODUCT_WIDGET = 8;
   static let  MALL_STORE_WIDGET = 9;
   static let  PRODUCT_CAROUSEL_WIDGET  = 10;
   static let  IMAGE_STRIP_WIDGET  = 11;
   static let  RECIPE_WIDGET  = 12;
   static let  RESTAURANT_WIDGET  = 13;
   static let  GRID_THREE_WIDGET  = 14;
    static let  OFFER_WIDGET_BIG  = 21;
    static let  HOME_WORLD  = 22;

}
