//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation

// 1 -  DEV
// 2 -  UAT
// 1 -  PRO

let ServiceType = 3

enum WebserviceType : Int {
    case typeDEV = 1
    case typeUAT
    case typePROD
}

class APIConstants {
    
    static let baseURL_General                        = "https://ucsmartapp.unioncoop.ae/";
    static let baseURL_CommonService_url              = "https://appservice.unioncoop.ae/ucappservice/V2/";
//    static let baseURL_Corporate                    = "https://corporate.unioncoop.ae/wp-json/custom/v1/";


    static let apiEmail                                = "admin@ucs.ae";
    static let apiKey                                  = "ad9bd1ecdbe54f302912a190fde2d49c";

    
    //GROCERY INFO
    //"https://dev.unioncoop.ae/newunionplus/index.php/mobileapiv2/";
    
    static let baseURL_Grocery_Dev                    = "https://uat.unioncoop.ae/index.php/mobileapiv2/"
//    static let baseURL_Grocery_UAT                    = "https://uat.unioncoop.ae/index.php/mobileapiv2/"
    static let baseURL_Grocery_Pro                    = "https://www.unioncoop.ae/mobileapiv2/";
    static let baseURL_Grocery_UAT                    = "https://awsbeta.unioncoop.ae/mobileapiv2/";

    static let groceryUsername_Dev_UAT                 = "unioncoopuatheader@uncioncoop.ae";
    static let groceryPassword_Dev_UAT                 = "1wQx9c6hiRgQb/WcNqVrtvnAZRhXfXm9bvR43SGAnoX4BD+FUE6A4ZqZwwY6xeTA@123";
    
    static let groceryUsername_Pro                     = "unioncoopmagentomobileadmin@uncioncoop.ae";
    static let groceryPassword_Pro                     = "cQfxxTjxxxx212232Wn444xxxZxxq234tx7w!z%C*F-JxxssaN4dRxx22sgUkXp2s";
    

    
    //TRADING INFO
    static let baseURL_Trading_Dev                          = "https://iapmdev.unioncoop.ae/";
    static let baseURL_Trading_UAT                          = "https://iapmuat.unioncoop.ae/";
    static let baseURL_Trading_Pro                          = "https://iapm.unioncoop.ae/";
    
    static let tradingAuthUsername_dev                        = "##@@$$51216evmajdor1121@#!$*&Kjxaeudnnadb0600332ytrade@ucs.ae";
    static let tradingAuthPasswd_dev                          = "51Kjxaeudnnadb060520190ADMIN33development060520190345@ucs.ae";
    
    static let tradingAuthUsername_Pro                        = "##@@$$512161121@#!$*&Kjxaeudnnadb060520190332monday@ucs.ae";
    static let tradingAuthPasswd_Pro                          = "##@@$$5121P@$$SWORD61121@#!$*&Kjxaeudnnadb060520190ADMIN332monday@ucs.ae";
    
    
    //SINGLE SIGN ON
    static let baseURL_SingleSignOn_DEV                  = "https://ssodev.unioncoop.ae/api/account/";
    static let baseURL_SingleSignOn_UAT                  = "https://ssouat.unioncoop.ae/api/account/";
    static let baseURL_SingleSignOn_PRO                  = "https://sso.unioncoop.ae/api/account/";

    static let SingleSinOnAuthUsername_DEV             = "##@@$$51216evmajsingledor1121@#!$*&Kwayjxaeudnnatodb06003login32y";
    static let SingleSinOnAuthPassword_DEV                = "51Kjxaeudnnadb060single520190signADMIN3306052019045inucs.ae202020";

    
    static let SingleSinOnAuthUsername_UAT             = "##@@$$51216evmajsingledor1121@#!$*&Kwayjxaeudnnatodb06003login32y";
    static let SingleSinOnAuthPassword_UAT            = "51Kjxaeudnnadb060single520190signADMIN3306052019045inucs.ae202020";

    static let SingleSinOnAuthUsername_PRO             = "RaBFRDc2flSUoARDc2JCNfsSBzj&!@31";
    static let SingleSinOnAuthPassword_PRO            = "cnPUekQ9wXpmdw+wTQ2jlWt9ioQ+CnEzrblHYX8pgY0Swa85DS8UdcUOyW9P7";


}


//TRADING Info
func getTradingBaseURL() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.baseURL_Trading_Dev
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
        return APIConstants.baseURL_Trading_UAT
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.baseURL_Trading_Pro
    }
    return nil
}

func getTradingAuthUsernameKey() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.tradingAuthUsername_dev
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
            return APIConstants.tradingAuthUsername_dev
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.tradingAuthUsername_Pro
    }
    return nil
}

func getTradingAuthPasswordKey() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.tradingAuthPasswd_dev
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
           return APIConstants.tradingAuthPasswd_dev
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.tradingAuthPasswd_Pro
    }
    return nil
}


//Grocery Service
func getGroceryBaseURL() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.baseURL_Grocery_Dev
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
        return APIConstants.baseURL_Grocery_UAT
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.baseURL_Grocery_Pro
    }
    return nil
}

func getGroceryAuthUsernameKey() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.groceryUsername_Dev_UAT
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
            return APIConstants.groceryUsername_Pro
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.groceryUsername_Pro
    }
    return nil
}

func getGroceryAuthPasswordKey() -> String? {
    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.groceryPassword_Dev_UAT
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
           return APIConstants.groceryPassword_Pro
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.groceryPassword_Pro
    }
    return nil
}


//SingleSignOn Information
func getSingleSignOnBaseURL() -> String? {

    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.baseURL_SingleSignOn_DEV
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
        return APIConstants.baseURL_SingleSignOn_UAT
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.baseURL_SingleSignOn_PRO
    }
    return nil
}


func getSingleSignOnAuthUsernameKey() -> String? {

    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.SingleSinOnAuthUsername_DEV
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
            return APIConstants.SingleSinOnAuthUsername_UAT
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.SingleSinOnAuthUsername_PRO
    }
    return nil
}

func getSingleSignOnAuthPasswordKey() -> String? {

    if WebserviceType.typeDEV.rawValue == ServiceType {
        return APIConstants.SingleSinOnAuthPassword_DEV
    }else if WebserviceType.typeUAT.rawValue == ServiceType {
           return APIConstants.SingleSinOnAuthPassword_UAT
    }else if WebserviceType.typePROD.rawValue == ServiceType {
        return APIConstants.SingleSinOnAuthPassword_PRO
    }
    return nil
}
