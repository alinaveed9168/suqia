//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation

let UCSUCCESS                                = "Success";
let UCFAIL                                   = "Fail";
let UCAlert                                  = "ALERT";
let appName                                  = "Union Coop"

// Notification EN TOPIC
let ENGuestUsers                                  = "ENGuest"
let ENGeneralUsers                                = "ENGeneral"
let ENShareholderUsers                            = "ENShareholder"
let ENEmployee                                   = "ENEmployee"

// Notification AR TOPIC
let ARGuestUsers                                  = "ARGuest"
let ARGeneralUsers                                = "ARGeneral"
let ARShareholderUsers                            = "ARShareholder"
let AREmployee                                   = "AREmployee"


///Color Code
let ucsBlueColor                                                 =  UIColor(named:"ucsBlue")!;
let ucsGreenColor                                                =  UIColor(named:"ucsGreen")!;
let UCtext_grayColor                                             =  UIColor(named:"UCtext_gray")!;
let ucsDarkblueColor                                             =  UIColor(named:"ucsDarkblue")!;
let ucsBGColor                                                 =  UIColor(named:"UCBG")!;
let ucslightMore                                                 =  UIColor(named:"UClightMore")!;
let ucsYellow                                                 =  UIColor(named:"ucsYellow")!;
let ucsRed                                                 =  UIColor(named:"UCred_color")!;

let suqiaBlue                                                 =  UIColor(named:"SuqiaBlue")!;


let ucsPoppinRegular                         =  UIFont(name:"Poppins-Regular",size:12)!;
let ucsDroidKufiRegular                         =  UIFont(name:"DroidArabicKufi",size:12)!;

let ucsPoppinBold                         =  UIFont(name:"Poppins-Bold",size:12)!;



////////////////////////////////////////////////////UserDefault Key //////////////////////////////////////////////////////////////////////

let UCLoginUserInfo                    = "LoginUserInfo"
let UCGrocerySelectedAddress           = "GrocerySelectedAddress"
////////////////////////////////////////////////////UserDefault Key //////////////////////////////////////////////////////////////////////


let UCNoInternetConnection                    = "Please Check your Internet Connection"
let UCInternerError                           = "Make sure your device is connected to the internet."
let UCService_Error                           = "Service currently unavailable. Please try again later.";


///Login Screen
let UCLogin_Email                              = "Login_Email";
let UCMobile_Number                            = "Mobile Number";
let UCLogin_with_mobile_number                  = "Login with mobile number";
let UCLogin_with_email                          = "Login with email";

let UCCreate_Account                            = "Enter_below_details";
let UCUpdate_Account                            = "Update Account Information";

let UCForgot_Username_Password                           = "Forgot_Username_Password";


let UCForgetTitle_Mobile                          = "Forget password through mobile number";
let UCForgetTitle_Email                          = "Forget password through email";


let UCButton_Title                             = "Submit";
let UCForgotButton                             = "Forgot your password";
let UCGuest                                    = "Continue_as_Guest";
let UCChangePassword                           = "Change_Password";
let UCRegister                             = "Register";

let UCSh_login                           = "sh_login";
let UCSkip                           = "skip";

let GoldCard                                   = "GOLD";
let SilverCard                                 = "SILVER";


let Silver_Tamayaz_Card_Label                    = "Silver Tamayaz Card for general customer";
let Gold_Tamayaz_Card_Label                      = "Gold Tamayaz Card for Shareholder";

let UCNoColor                                = "F5C129";
let SELLING_UNIT_GRAM                                = "gram";
let SELLING_UNIT_KG                                = "kg";

let UCProceed = "Proceed";
let UCAccountCreate = "AccountCreate";

let HOME_DELIVERY                                = "home_delivery";
let CLICK_COLLECT                                = "clickandcollect";
let EXPRESS_DELIVERY                             = "express";

let UCEditProfile                             = "Update_Profile";
let UCCARTITEMS                                   = "cartItems";

let UCEmail                              = "Email";
let UCMobile                              = "profile_mobile";

let UCCreateAccount = "Don't have account ? Create Account";
let UCCA = "Account_underline";
let UCEnterShareholderNumber                      = "Please enter shareholder number";

let UCEnter_below_details = "Enter_below_details";
let UCEnterPassword = "enter_password";
let UCResendOTP =  "Did not receive OTP ? Resend";

let UCFName =  "Fname";
let UCLName =  "Lname";

let UCPassword =  "Password";

let UCNationality =  "Nationality";

let UCGender = "Gender";

let UCTax_Reg_No = "Tax_Reg_No";

let UCDOB = "DOB";

let UCAddtoCart = "Add_to_cart"
let UCProduct = "Product"
let UCTotalItem = "total_item"

let UCNoData = "NoResultFound"

let UCProducts =  "Products"
let UCOutOfStock =  "outofstock"
let UCInStock =  "instock"
let UCAllPriceIncludeVat = "All Price Include VAT"
let UCRecipeDetail = "Recipe Detail"
let UCRecipeProducts = "Recipes Product"
let UCAddAllToCart = "Add_All_to_Cart"
let UCDuration = "Duration"

let UCYES = "YES"
let UCNO = "NO"
let UCProductDetail = "Product_Details"
let UCSelectCustomOption = "Select Custom Option";
let UCSelectCityAndArea = "Select City and Area";
let UCPleaseSelectArea = "Please_select_area";
let UCConfirm = "Confirm"
let UCSearchByAreaName = "search_by_area_name"
let UCDone = "Done";
let UCPleaseEnterYourName  = "Please_enter_your_name"
let UCMobileNumber = "Mobile_Number";
let UCSubmit = "Submit";
let UCTamayazCardNumber = "Tamayaz_card_number";
let UCWriteComment = "Write_comment";
let UCFeedbackTitle = "Feedback_Title";
let UCComplaint = "complaint";
let UCSuggestion = "suggestion";
let UCEnquiry = "enquiry";
let UCSuggenstionAndComplain = "Complain_Suggestion";
let UCMale = "Male";
let UCFemale = "Female";
let UCSelectOption = "Select_Option";
let UCRecipes = "Recipes";


let UCCity = "City"
let UCArea =  "Area"
let UCBuilding_Villa_Office_Name =    "Building_Villa_Office_Name"
let UCMakani_LandMark = "Makani/LandMark"
let UCAddress = "Address"
let UCAdd_Address = "Add_Address"
let UCUpdateAddress = "Update Address"
let UCSearchYourProducts = "Search_Product";
let UCViewAll = "ViewAll";
let UCBanner = "Banner";
let UCDoyouWanttoLogout = "do_you_want_logout?";
let UCGuest_User = "Guest_User";
let UCMyCart = "My Cart";
let UCClick_n_Collect = "Click_n_Collect";
let UCHome_Delivery = "Home_Delivery";
let UCContinueShopping = "Continue Shopping";
let UCCheckout = "Checkout";
let UCWelcomeGuest = "Welcome, Guest";
let UCWelcome = "Welcome , ";


let UCGrandTotal = "Grand_Total"
let UCVAT = "VAT"
let UCservice_n_handling = "service_n_handling"
let UCShipping = "Shipping"
let UCSubTotal = "Sub_Total"
let UCRemoveOutOfStock = "removeOutOfStock";
let UCRemoveFRomCart = "Remove_From_Cart"


let UCHolding_amt = "Holding_amt"
let UCShipping_Address_Order = "Shipping_Address_Order"
let UCpayment_method = "payment_method"
let UCunavailable_items = "unavailable_items"
let UCtime_slot = "time_slot"
let UCmessage_notes = "message_notes"
let UCOrder_Confirmation = "Order_Confirmation";
let UCPlace_Order = "Place_Order";
let UCcollect_from_store = "collect_from_store";
let UCExpress_delivery = "express_delivery";

let UCSelect_option_unavailable = "select_option_unavailable"
let UCSelect_Slot = "Select_Slot"
let UCTime_slot_occupied = "time_slot_occupied"
let UCSelect_Payment_mode = "Select_Payment_mode"
let UCAED = "AED"


let UCOrderPlacedSuccessfull = "Your Order has been Placed Successfully"
let UCThankyou = "Thankyou"
let UCOrderFailed = "Your Order has been Failed";
let UCSorry = "sorry";

let UCView_more = "view_more";
let UCOrderID = "order_id";
let UCNo_of_items = "no_of_items";
let UCNotifications = "Notifications";
let UCPayment = "Payment";
let UCLogin_Register = "Login/Register";
let UCContinue = "Continue";
let UCTamayazCard = "Tamayaz_card";
let UCClose = "Close";


let UCRecently_added_recipe = "Recently_added_recipe";
let UCCook_book = "Cook_book";

let UCless_than_100_click_n_collect = "less_than_100_click_n_collect";
let UCless_than_100_home_delivery = "less_than_100_home_delivery";

let UCChange_Address_alert = "change_Address_alert";
let UCMailServicesarenotavailable =  "Mail_services_are_not_available";
let UCFromYouLocation = "from_Your_location";
let UCDesc = "Desc";
let UCBranchDetails = "Branch_Details";
let UCBranches = "Branches";
let UCPublish_Date = "Publish_Date";
let UCProject_Details = "Project_Details";
let UCOurProject = "OurProject";
let UCUnder_Construction = "under_construction";
let UCFuture_Proect = "future_proect";
let UCQuantity = "Quantity";
let UCTotalPrice = "Total_Price";

let UCOrderDetails  = "order_details";
let UCDate  =   "Sh_Date";
let UCPriceDetails =   "Price_Details";
let UCShippingAmt  =   "Shipping_Charge";
let UCDisAmt  =   "Dic_Amt";
let UCPaymentMode  =   "Payment_Mode";
let UCRating = "OverallRating";
let UCShipping_Address = "Shipping_Address";
let UCPaymentMethod = "PaymentMethod";
let UCDo_you_want_reorder = "do_you_want_reorder";

let UCOrderHistory = "order_history";
let UCAddress_List = "Address_List";
let UCSmart_receipt = "smart_receipt";
let UCSearch = "Search";
let UCClear = "Clear";
let UCStart_Date = "Start_Date";
let UCEnd_Date = "End_Date";
let UCReviews = "Reviews";
let UCMyProfile = "My_Profile";
let UCShareholder_number = "Shareholder_number";

let UCProductResult = "ProductResult";
let UCUpdateCart = "update_cart";
let UCSelectDate = "Select Date";
let UCViewMore = "ViewMore";


let UCEnterFirstName =  "PleaseEnteryourfirstName";
let UCEnterLastName =  "PleaseEnteryourlastName";
let UCEnterEmailAddress =  "Pleaseenteremailaddress";
let UCEnterValidEmail = "Pleaseentervalidemailaddress";
let UCEnterValidMobile =  "Please_enter_valid_mobile_number";
let UCDateofBirth = "Pleaseselectyourdateofbirth";
let UCSelectNationality = "PleaseSelectYourNationality";
let UCSelectGender = "PleaseSelectYourGender";
           


let UCEnterOldPassword =  "PleaseEnterOldPassword";
let UCEnterNewPassword = "Pleaseenternewpassword";
let UCEnterConfirmPassword = "Pleaseenterconfirmpassword";
let UCPasswordNotMatch = "PasswordandConfirmPasswordmatch";

let UCPleaseEnterValidMobileNumber = "Please_enter_valid_mobile_number";


let UCPleaseSelectCity = "Please_select_city";
let UCPleaseEnterVillaName = "Please_enter_your_building_name_villa_name";
let UCPleaseEnterMakaniNumber = "Please_enter_valid_makani_number";
let UCPleaseEnterAddress = "Please_ente_you_address";


let UCAddressInformation = "Address Information";
let UCMyReceipt = "My_Receipt";

let UCNoInformationFound = "No information found";
let UCAmount = "Amount";
let UCReceipt = "Receipt";
let UCLogOut = "Log Out";
let UCLogIn = "Log In";
let UCProductList = "ProductList";
let UCPleaseEnterOTP =  "Please enter OTP";
let UCPleaseValidOTP =  "Please enter valid OTP";
let UCEnterValidPassword = "Please enter valid Password";

let UCMagazine = "Magazine";
let UCCollectFrom = "Collect From";
let UCDelivery_To = "Delivery_To";

let UCPoints = "Points";
let UCChange = "Change";
let UCGetTamayazPointWithUnionCoop = "Get Tamayaz Point With Union Coop";
let UCCategories = "Categories";
let UCAdd_All_Item_to_cart = "Add_All_Item_to_cart";
let UCStore = "Store";
let UCShare = "Share";
let UCPlease_enter_comment = "Please_enter_comment";
let UCPromotion_catalog = "promotion_catalog";
let UCPlease_select_type_of_complaint = "Please_select_type_of_complaint"


let UCHome = "Home";
let UCShareholder = "Shareholder";
let UCCart = "Cart";
let UCMore = "More";
let UCReorder = "Reorder";
let UCReview_n_Rating = "Review_n_Rating";
let UCAllSmartReciept = "All_Smart_Reciept";
let UCSummary = "Summary"
let UCSearchYourArea = "search_your_area";


let UCSelectAddress = "Select_Address";
let UCSelectBranch = "Select_Branch";

let UCCustomer_Email  = "Customer_Email";

let UCCurrentPassword  = "Current_Password";
let UCNewPassword = "New_Password";
let UCConfirmPassword = "Confirm_Password";


let UCDashboardData                                   = "DashboardData";
let UCDashboardBlock                                   = "DashboardBlock";
let UCDashboardDataPages                                   = "DashboardDataPages";

let UCTas_service_available = "tas_service_available";
let UCRelogin_session = "relogin_session";

let UCPleaseSelectFromDate = "pleaseSelectFromDate";
let UCPleaseSelectToDate = "pleaseSelectToDate";


let UCPrint = "Print";

let UCCapturePhoto = "Capture Photo";
let UCSelectFromLibrary = "Select From Library";
let UCSelectFromCameraRoll = "Select From Camera roll";

let UCCancel = "Cancel";


let UCAttachment = "attachment";
let UCAllProducts = "All Products"
let UCTamayaz = "Tamayaz";

let mainUIStoryboard = UIStoryboard(name: "Main", bundle: nil)
let tabbartUIstoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
let addMenuManagementUIstoryboard = UIStoryboard(name: "MenuManagement", bundle: nil)
let tableManagementUIstoryboard = UIStoryboard(name: "TableManagement", bundle: nil)


let UCPhoneEmailUsername                              = "Phone,email or username";
