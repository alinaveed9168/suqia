//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation



let UCDO_NOT_SHOW_HELP                          = "DO_NOT_SHOW_HELP"
let UCDO_NOT_Prefered_Language                  = "DO_NOT_Prefered_Language"

//Keys
let UCContent_Type_key                        = "Content-Type"
let UCContent_Type_value                       = "application/json"

let UCUsername_key                            = "Api-Key"
let UCPassword_key                            = "Api-Pwd"
let UCDeviceID_key                            = "Device-Id"
var UCDeviceType_key                          = "Device-Type"
let UCDeviceType_Value                        = "iOS"
let UCClient_Key                             = "Client-Id"
let UCUserID_Key                             = "User-Id"

let UCCity_Key                              = "City-Id"
let UCArea_Key                              = "Area-Id"

let UCDeviceToken_key                         = "DEVICETOKEN"
let UCToken_key                              = "Token"
let UCLang                                   = "Lang"

var UCAppVersion                          = "App-Version"
