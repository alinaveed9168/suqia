//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation

class ConstantsURL {

    static let googleApiKey = "AIzaSyCuw7bS0kTQHhoaby6rqRhwuABnT2vHJBY"
    
    static let itunesUnionCoop = "https://itunes.apple.com/ae/app/union-coop/id993180002?mt=8"
    static let facebookUrl = "https://www.facebook.com/union.cooperative"
    static let twitterUrl = "https://twitter.com/UnionCoop"
    static let youtubeUrl = "http://www.youtube.com/user/unioncoop"
    static let instagramUrl = "https://instagram.com/union.coop/"
    static let linkedInUrl = "https://instagram.com/union.coop/"
    static let SingleSinOnClientID_DEV                = "ZGMLMQoyHYbekc1U9cEuctyNGPPxyO0aMMPGRHro2eA/hHAI8D";
    static let SingleSinOnClientID_PRO                = "PPxyO0GRHro2eA/hHAI8DPxyO0aMM";

}

