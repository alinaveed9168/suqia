//
//  ZKCarouselCell.swift
//  ZKCarousel
//
//  Created by Zachary Khan on 8/22/20.
//

 class ZKCarouselCell: UICollectionViewCell {

    static let identifier = "slideCell"
    
    // MARK: - Properties
    public var slide : ZKCarouselSlide? {
        didSet {
            guard let slide = slide else {
                print("ZKCarousel could not parse the slide you provided.")
                return
            }
            parseData(forSlide: slide)
        }
    }
    
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var  titleLabel:UILabel!
    
    @IBOutlet weak var  descriptionLabel: UILabel!
    
    // MARK: - Default Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    
    override func awakeFromNib() {
         super.awakeFromNib()
        setup()
     }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    // MARK: - Actions
    private func setup() {
        backgroundColor = .clear
        clipsToBounds = true
        
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .clear
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.addBlackGradientLayer(frame: self.imageView.bounds)


        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

          
    }
    
    private func parseData(forSlide slide: ZKCarouselSlide) {
        imageView.setImageFromURL(urlString: slide.image!)
        titleLabel.text = slide.title
    }

}
