//
//  ZKCarouselSlide.swift
//  ZKCarousel
//
//  Created by Zachary Khan on 8/22/20.
//

public struct ZKCarouselSlide {
    public var image : String?
    public var title : String?
    
    public init(image: String?, title: String?) {
        self.image = image
        self.title = title
    }
}
