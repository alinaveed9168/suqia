//
//  GlobalStruct.swift
//  unionCoop
//
//  Created by Union Coop on 6/20/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

@objcMembers class Connectivity: NSObject {

//public struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
    
}
