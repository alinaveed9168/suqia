//
//  CoopButton.swift
//  unionCoop
//
//  Created by Union Coop on 7/22/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//
import UIKit

@IBDesignable class ucsButton :UIButton {
    
    @IBInspectable var borderWidth : CGFloat = 0.0 {
        didSet{self.layer.borderWidth = borderWidth}
    }
    
    @IBInspectable var borderRadius : CGFloat = 0.0{
        didSet{self.layer.cornerRadius = borderRadius}
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.clear {
        didSet{self.layer.borderColor = borderColor.cgColor}
    }
}

