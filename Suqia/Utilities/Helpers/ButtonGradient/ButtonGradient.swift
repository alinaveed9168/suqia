//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation

class ButtonGradient: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }

    private lazy var gradientLayer: CAGradientLayer = {
        let l = CAGradientLayer()
        l.frame = self.bounds

        let color1 = ucsBlueColor.cgColor
        let color2 = ucsDarkblueColor.cgColor
        
        l.colors = [color1, color2]
        l.startPoint = CGPoint(x: 0, y: 0.5)
        l.endPoint = CGPoint(x: 1, y: 0.5)
        l.cornerRadius = 23
        layer.insertSublayer(l, at: 0)
        return l
    }()
}
