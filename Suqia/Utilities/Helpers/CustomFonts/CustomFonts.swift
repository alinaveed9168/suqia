//
//  Constants.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit



struct Resources {

    struct Fonts {
        //struct is extended in Fonts
    }
}

extension Resources.Fonts {

    enum Weight: String {
        case light = "Poppins-ExtraLightItalic"
        case regular = "Poppins-Regular"
        case semibold = "Poppins-Bold"
        case italic = "Poppins-Italic"
        
        case regular_ar = "DroidArabicKufi"
        case bold_ar = "DroidArabicKufi-Bold"


    }
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}

extension UIFont {

    @objc class func mySystemFont(ofSize: CGFloat, weight: UIFont.Weight) -> UIFont {
        switch weight {
        case .semibold, .bold, .heavy, .black:
            let semi = (langSwift()) ? Resources.Fonts.Weight.semibold.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            return UIFont(name: semi, size: ofSize)!

        case .medium, .regular , .ultraLight , .thin:
            let regular = (langSwift()) ? Resources.Fonts.Weight.regular.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            return UIFont(name: regular, size: ofSize)!

        default:
            let regular = (langSwift()) ? Resources.Fonts.Weight.regular.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            return UIFont(name: regular, size: ofSize)!
        }
    }

    @objc class func mySystemFont(ofSize size: CGFloat) -> UIFont {
        let regular = (langSwift()) ? Resources.Fonts.Weight.regular.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
        return UIFont(name: regular, size: size)!
    }

    @objc class func myBoldSystemFont(ofSize size: CGFloat) -> UIFont {
        let semibold = (langSwift()) ? Resources.Fonts.Weight.semibold.rawValue : Resources.Fonts.Weight.bold_ar.rawValue
        return UIFont(name: semibold, size: size)!
    }

    @objc class func myItalicSystemFont(ofSize size: CGFloat) -> UIFont {
        let regular = (langSwift()) ? Resources.Fonts.Weight.light.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
        return UIFont(name: regular, size: size)!
    }

    @objc convenience init(myCoder aDecoder: NSCoder) {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
                self.init(myCoder: aDecoder)
                return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontRegularUsage", "CTFontMediumUsage":
            let regular = (langSwift()) ? Resources.Fonts.Weight.regular.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            fontName = regular
        case "CTFontEmphasizedUsage", "CTFontBoldUsage", "CTFontSemiboldUsage","CTFontHeavyUsage", "CTFontBlackUsage":
            let semiBold = (langSwift()) ? Resources.Fonts.Weight.semibold.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            fontName = semiBold
        case "CTFontObliqueUsage":
            let regular = (langSwift()) ? Resources.Fonts.Weight.italic.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            fontName = regular
        default:
            let regular = (langSwift()) ? Resources.Fonts.Weight.light.rawValue : Resources.Fonts.Weight.regular_ar.rawValue
            fontName = regular
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }

    class func overrideDefaultTypography() {
        guard self == UIFont.self else { return }

        if let systemFontMethodWithWeight = class_getClassMethod(self, #selector(systemFont(ofSize: weight:))),
            let mySystemFontMethodWithWeight = class_getClassMethod(self, #selector(mySystemFont(ofSize: weight:))) {
            method_exchangeImplementations(systemFontMethodWithWeight, mySystemFontMethodWithWeight)
        }

        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:))) {
            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
        }

        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:))) {
            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
        }

        if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))),
            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:))) {
            method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
        }

        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))),
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
    }
    
    var isBold: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitBold)
       }

       var isItalic: Bool {
        return fontDescriptor.symbolicTraits.contains(.traitItalic)
       }
    
        var isRegular: Bool {
           return (!fontDescriptor.symbolicTraits.contains(.traitItalic) && !fontDescriptor.symbolicTraits.contains(.traitBold))
          }
}

func setFontFamily(_ view: UIView, andSubviews: Bool) {
    
    var Name = ""
    
    if let label = view as? UILabel {
        Name = (label.font.isBold) ? "Poppins-Italic" : (label.font.isItalic) ? "Poppins-Bold" : "Poppins-Regular"
        let fontLanguage = (langSwift()) ? Name : "DroidArabicKufi"
        label.font = UIFont(name: fontLanguage, size: label.font.pointSize)
    }
    
    if let textView = view as? UITextView {
        Name = (textView.font!.isBold) ? "Poppins-Italic" : (textView.font!.isItalic) ? "Poppins-Bold" : "Poppins-Regular"
        let fontLanguage = (langSwift()) ? Name : "DroidArabicKufi"
        textView.font = UIFont(name: fontLanguage, size: textView.font!.pointSize)
    }
    
    if let btn = view as? UIButton {
        Name = (btn.titleLabel!.font!.isBold) ? "Poppins-Regular" : (btn.titleLabel!.font!.isItalic) ? "Poppins-Regular" : "Poppins-Regular"
        let fontLanguage = (langSwift()) ? Name : "DroidArabicKufi"
        btn.titleLabel?.font = UIFont(name: fontLanguage, size: (btn.titleLabel?.font!.pointSize)!)
    }
    
    if let textField = view as? UITextField {
        Name = (textField.font!.isBold) ? "Poppins-Italic" : (textField.font!.isItalic) ? "Poppins-Italic" : "Poppins-Regular"
        let fontLanguage = (langSwift()) ? Name : "DroidArabicKufi"
        textField.font = UIFont(name: fontLanguage, size: textField.font!.pointSize)
    }
    
    if let tbView = view as? UITableView {
        for view in tbView.subviews {
            for  subview in view.subviews{
                if  subview is UITableViewCell
                {
                    setFontFamily(subview, andSubviews: true)
                }
            }
        }
        
    }
    
    if andSubviews {
        for v in view.subviews {
            setFontFamily(v, andSubviews: true)
        }
    }
}
