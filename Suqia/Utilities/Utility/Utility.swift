//
//  Utility.swift
//  unionCoop
//
//  Created by Union Coop on 7/23/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
class Utility: NSObject {

    static let UtilityMethod = Utility()
    var naviController = UINavigationController()

    func showUIAlertView(title:String,message:String,alertTypes:String)  {

        switch alertTypes {
            
        case "Success":
            naviController.showAlertView(msg:message, theme: .success)
            break
            
        case "Fail":
             naviController.showAlertView(msg:message , theme: .error)
            break
            
        case "Caution":
            naviController.showAlertView(msg:message , theme: .warning)
            break
            
        case "Info":
            naviController.showAlertView(msg:message , theme: .info)
            break
        default:
            break;
        }
    }
    
    func showAlert_and_Pop(title:String,message:String,alertTypes:String,myNavigationController:UINavigationController)  {
        
        naviController = myNavigationController
        let margin = SCLAlertView.SCLAppearance.Margin(buttonSpacing: 30,
                                                       bottom: 30,
                                                       horizontal: 30)
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false, shouldAutoDismiss: true,
            margin: margin
        )
        
        let icon = UIImage(named:"Water-drop-Logo")
        let color = UIColor.blue
         let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton(UCDone.localized()) {
            self.naviController.popViewController(animated: true)
        }
        alertView.showCustom(title, subTitle: message, color: color, circleIconImage: icon!)
        
    }
    
    func setCustomLoginUserShadowView(_ view: UIView?) {
           view?.layer.shadowColor = UIColor.lightGray.cgColor //UIColor(white: 0.0, alpha: 0.5).cgColor
           view?.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
           view?.layer.shadowOpacity = 1.0
           view?.layer.shadowRadius = 3.0
            view?.layer.cornerRadius = 18.0
            view?.clipsToBounds = true

       }
    func removeAllSavedInformation()  {
        globalPrint(dataToPrint: "removeAllSavedInformation")
    }
}
