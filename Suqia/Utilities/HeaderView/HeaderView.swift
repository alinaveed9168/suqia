//
//  ucsCustomView.swift
//  unionCoop
//
//  Created by Union Coop on 6/19/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
@IBDesignable
@objcMembers class HeaderView: UIView {
    let kCONTENT_XIB_NAME = "HeaderView"
    @IBOutlet var contentView: UIView!
    @IBOutlet var bckArrow: UIImageView!
    @IBOutlet var homeArrow: UIImageView!
    @IBOutlet var searchArrow: UIImageView!
    @IBOutlet var bellArrow: UIImageView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var navBarHeight:NSLayoutConstraint!
   public var viewcontroller: UINavigationController!
    
    
    func addTopHeaderProperties()  {
        navBarHeight.constant = navigationBarHeight
    }
    
    @objc func popBack()  {
        viewcontroller?.popViewController(animated: true)
    }
    
    var view = UIView()
    
    
    
    @IBInspectable public var hideBell: Bool = false {
        didSet {
            self.bellArrow.isHidden = hideBell
        }
    }
    
    @IBInspectable public var hideBack: Bool = false {
        didSet {
            self.bckArrow.isHidden = hideBack
        }
    }
    
    @IBInspectable public var hideSearch: Bool = false {
        didSet {
            self.searchArrow.isHidden = hideSearch
        }
    }
    
    @IBInspectable public var hideHome: Bool = false {
        didSet {
            self.homeArrow.isHidden = hideHome
        }
    }
    
    
    
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        initialConfig()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        initialConfig()
    }
    
    private func initialConfig() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(popBack))
        bckArrow.isUserInteractionEnabled = true
        bckArrow.addGestureRecognizer(tapGesture)
        let imagearrow =  (getCurrentLanguagevalue()) ? "new_back" : "new_back_ar"
        bckArrow.image = UIImage(named: imagearrow)
        navView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        

    }
    
    
    // MARK: - Nib handlers
    
    private func xibSetup() {
        view = loadViewFromNib()
        addTopHeaderProperties()
        view.frame = bounds
        //'UIViewAutoresizing' has been renamed to 'UIView.AutoresizingMask'
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: kCONTENT_XIB_NAME, bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        //setup your view with the settings
    }
    
}

extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
}
