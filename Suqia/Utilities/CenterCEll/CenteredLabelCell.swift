//
//  CenteredLabelCell.swift
//  ExpandableCell
//
//  Created by Andrey Zonov on 08/11/2017.
//  Copyright © 2017 Andrey Zonov. All rights reserved.
//

import UIKit

class CenteredLabelCell: UITableViewCell {
    
    // MARK: Private Outlets
    @IBOutlet  var labelValue: UILabel!
    @IBOutlet  var img: UIImageView!
    
    @IBOutlet  var imgWidth: NSLayoutConstraint!

    // MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelValue.text = ""
        img.image = UIImage(named: "logonMain")
        backgroundColor = .white
    }
    
    // MARK: Public
    func configure(with title: String,withImage:String,withFontSize:String) {
        labelValue.text = title
        imgWidth.constant  =  (withImage.count == 0) ? 0 : 118
        img.imageFromURL(urlString: withImage)
        if(withFontSize == "1"){
            labelValue.font.withSize(labelValue.font.pointSize - 5)
            labelValue.textAlignment = .left
        }
    }
}
