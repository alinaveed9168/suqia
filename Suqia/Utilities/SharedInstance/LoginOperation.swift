//
//  LoginOperation.swift
//  unionCoop
//
//  Created by Union Coop on 5/27/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import Foundation

@objcMembers class LoginOperation: NSObject {
    
    static let shared = LoginOperation()
    override init() {
        
    }
    func setCurrentLanguage(lang:String!) {
           UserDefaults.standard.set(lang, forKey:"langSelected")
       }
    func getCurrentLanguage() -> String! {
           return UserDefaults.standard.value(forKey:"langSelected") as? String
       }
       
    func getshowTabbar() {
//        ucTabarVC.testController.hidesBottomBarWhenPushed = false
//        ucTabarVC.testController.tabBar?.selectedTabItem(0, animated: true)
    }
    
    func getLANG_CODEInt() -> Int! {
        return ucSharedUserDefaults.integer(forKey: "intlangSelected")
    }
    func getLANG_CODE() -> String! {
        return ucSharedUserDefaults.value(forKey:"strlangSelected") as? String
    }
    
    func setLanguageChangeFlag(languageChangeFlag:Bool) {
        ucSharedUserDefaults.set(languageChangeFlag, forKey:"languageChangeFlag")
    }
    func getLanguageChangeFlag() -> Bool {
        return ucSharedUserDefaults.bool(forKey:"languageChangeFlag")
    }
        
    func setCardNumber(cardNumber:String!) {
        ucSharedUserDefaults.set(cardNumber, forKey:"cardNumber")
    }
    
    func getCardNumber() -> String! {
        return ucSharedUserDefaults.value(forKey:"cardNumber") as? String
    }
    
    func setSingleSignOnuserID(userid:String!) {
        ucSharedUserDefaults.set(userid, forKey:"userid")
    }
    func getSingleSignOnuserID() -> String! {
        return (ucSharedUserDefaults.value(forKey:"userid") == nil) ? "" : ucSharedUserDefaults.value(forKey:"userid") as! String
    }
    
    ///===
    func getValidMobile() -> String! {
           return ucSharedUserDefaults.value(forKey:"validMobile") as? String
       }
    
    func setGrocerySelectedAddress(address_id:String!) {
        ucSharedUserDefaults.set(address_id, forKey:UCGrocerySelectedAddress)

    }
    func getGrocerySelectedAddress() -> String! {
        return ucSharedUserDefaults.value(forKey:UCGrocerySelectedAddress) as? String
    }
    
    
    func setShippingType(shippingType:String!) {
        UserDefaults.standard.set(shippingType, forKey:"ShippingType")
    }
    func getShippingType() -> String! {
        let shipT = UserDefaults.standard.value(forKey:"ShippingType") as? String
        return  (shipT == nil || shipT == "") ? HOME_DELIVERY: shipT
    }
    
    func setGroceryCustomerID(customerID:String!) {
          ucSharedUserDefaults.set(customerID, forKey:"GroceryCustomerID")
      }
      func getGroceryCustomerID() -> String! {
          return ucSharedUserDefaults.value(forKey:"GroceryCustomerID") as? String
      }
    
    
    func setStoreInfo(storeInfo:NSDictionary!) {
        ucSharedUserDefaults.set(NSKeyedArchiver.archivedData(withRootObject: storeInfo!), forKey:"storeInfo")
    }
    
    func getStoreInfo() -> NSDictionary! {
        let data  = ucSharedUserDefaults.object( forKey: "storeInfo")  as? Data
         if let data = data { return NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary }
        return nil
    }
    
    func setCustomerInfo(customerInfo:NSDictionary!) {
        ucSharedUserDefaults.set(NSKeyedArchiver.archivedData(withRootObject: customerInfo!), forKey:"customerInfo")
    }
    func getCustomerInfo() -> NSDictionary! {
        let data  = ucSharedUserDefaults.object( forKey: "customerInfo")  as? Data
         if let data = data { return NSKeyedUnarchiver.unarchiveObject(with: data) as? NSDictionary }
        return nil
    }

    
    func setCustomerSync(customerSync:Bool) {
        ucSharedUserDefaults.set(customerSync, forKey:"customerSync")
    }
    func getCustomerSync() -> Bool {
        return ucSharedUserDefaults.bool(forKey:"customerSync")
    }
    

    
    func setCustomerDeviceID(deviceID:String!) {
        ucSharedUserDefaults.set(deviceID, forKey:"DeviceID")
    }
    func getCustomerDeviceID() -> String! {
        return ucSharedUserDefaults.value(forKey:"DeviceID") as? String
    }

    
    ////Grocery NSUserDefaults
  
    func setBranchID(BID:String!) {
        ucSharedUserDefaults.set(BID, forKey:"branchID")
    }
    func getBranchID() -> String! {
        return ucSharedUserDefaults.value(forKey:"branchID") as? String
    }
    
    func setBranchName(BName:String!) {
        ucSharedUserDefaults.set(BName, forKey:"branchName")
    }
    func getBranchName() -> String! {
        return ucSharedUserDefaults.value(forKey:"branchName") as? String
    }
    
    func setGroceryAreaID(groceryAreaID:String!) {
        ucSharedUserDefaults.set(groceryAreaID, forKey:"groceryAreaID")
    }
    
    func getGroceryAreaID() -> String! {
        return (ucSharedUserDefaults.value(forKey:"groceryAreaID") == nil) ? "" : ucSharedUserDefaults.value(forKey:"groceryAreaID") as! String
    }
    
    func setGroceryAreaName(groceryAreaName:String!) {
        ucSharedUserDefaults.set(groceryAreaName, forKey:"groceryAreaName")
    }
    
    func getGroceryAreaName() -> String! {
        return (ucSharedUserDefaults.value(forKey:"groceryAreaName") == nil) ? "" : ucSharedUserDefaults.value(forKey:"groceryAreaName") as! String
    }
    
    func setGroceryCityID(groceryCityID:String!) {
        ucSharedUserDefaults.set(groceryCityID, forKey:"groceryCityID")
    }
    
    func getGroceryCityID() -> String! {
        return (ucSharedUserDefaults.value(forKey:"groceryCityID") == nil) ? "" : ucSharedUserDefaults.value(forKey:"groceryCityID") as! String
    }
    
    func setGroceryCityName(groceryCityName:String!) {
        ucSharedUserDefaults.set(groceryCityName, forKey:"groceryCityName") 
    }
    func getGroceryCityName() -> String! {
        return (ucSharedUserDefaults.value(forKey:"groceryCityName") == nil) ? "" : ucSharedUserDefaults.value(forKey:"groceryCityName") as! String
    }
        
    func setSHAddressNo(addressNumber:String!) {
        ucSharedUserDefaults.set(addressNumber, forKey:"ADDRESSNUMBER")
    }
    
    func getSHAddressNo() -> String! {
        return ucSharedUserDefaults.value(forKey:"ADDRESSNUMBER") as? String
    }
    
    func setSHNumber(shno:String!) {
        ucSharedUserDefaults.set(shno, forKey:"SHNO")
    }

    
    func getSHNumber() -> String! {
        return ucSharedUserDefaults.value(forKey:"SHNO") as? String
    }
    
    func setLoginFlag(loginFlag:Bool) {
        ucSharedUserDefaults.set(loginFlag, forKey:"loginFlag")
    }
    
    func getLoginUserDetails() -> LoginUserDataModel{
        return MyUserDefaults.instance.getObject(LoginUserDataModel.self, forKey: UCLoginUserInfo)!
    }
    
    func setLoginUserDetails(loginData:LoginUserDataModel){
        MyUserDefaults.instance.setObject(loginData, forKey:UCLoginUserInfo)
    }
    
    //Shareholder
    
    func setUserInformation(arrinfo:RegCustomerData!) {
        
        let archivedPool = NSKeyedArchiver.archivedData(withRootObject: arrinfo!.dictionaryRepresentation())
//        ucSharedUserDefaults.set(archivedPool, forKey:"UserInfo")
        ucSharedUserDefaults.set(archivedPool, forKey:"UserInfo")
        ucSharedUserDefaults.synchronize()
    }
    
    func getUserInformation() -> RegCustomerData! {
        
        let ud = UserDefaults.standard
                if let val = ud.value(forKey: "UserInfo") as? Data,
                    let obj = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(val) as?  [String: Any]{
                    let objs =  RegCustomerData(object: obj)
                    return objs
                }
        return nil

//        if let dataUserInfo = ucSharedUserDefaults.data(forKey: "UserInfo"),
//            let myPeopleListUserInfo = NSKeyedUnarchiver.unarchiveObject(with: dataUserInfo) as? RegCustomerData {
//            return myPeopleListUserInfo
//        } else {
//            globalPrint(dataToPrint:"There is an issue")
//        }
//        return nil
    }
    
   
    func setLocalDeviceDeviceID(shippingType:String!) {
        UserDefaults.standard.set(shippingType, forKey:"localdeviceID")
    }
    func getLocalDeviceDeviceID() -> String! {
        return UserDefaults.standard.value(forKey:"localdeviceID") as? String
    }
    
    func setCategoryContainerFilterObj(filterType:NSMutableArray!) {
        let assignFilter = (filterType == nil) ? [] : filterType
        ucSharedUserDefaults.set(NSKeyedArchiver.archivedData(withRootObject: assignFilter!), forKey:"categorycontainerfilter")
        ucSharedUserDefaults.synchronize()
    }
    func getCategoryContainerFilterObj() -> NSMutableArray! {

        let data  = ucSharedUserDefaults.object( forKey: "categorycontainerfilter") as? Data
         if let data = data { return NSKeyedUnarchiver.unarchiveObject(with: data) as? NSMutableArray }
        return nil
        
    }

    func getISEmpRegisteredForTASFlag() -> Bool {
        return ucSharedUserDefaults.bool(forKey:"tasfalg")
    }
    
    func setISEmpRegisteredForTASFlag(tasfalg:Bool) {
        ucSharedUserDefaults.set(tasfalg, forKey:"tasfalg")
    }
    
    func getTASCurrentDeviceFlag() -> Bool {
        return ucSharedUserDefaults.bool(forKey:"currentDeviceTasfalg")
    }
    
    func setTASCurrentDeviceFlag(currentDeviceTasfalg:Bool) {
        ucSharedUserDefaults.set(currentDeviceTasfalg, forKey:"currentDeviceTasfalg")
    }
    
    func setDeviceToken(token:String!) {
        ucSharedUserDefaults.set(token, forKey:"deviceToken")
    }
    func getDeviceToken() -> String! {
        return (ucSharedUserDefaults.value(forKey:"deviceToken") == nil) ? "" : ucSharedUserDefaults.value(forKey:"deviceToken") as! String
    }
    
    
    func setTamayazUserID(token:String!) {
        ucSharedUserDefaults.set(token, forKey:"TamayazUserID")
    }
    func getTamayazUserID() -> String! {
        return (ucSharedUserDefaults.value(forKey:"TamayazUserID") == nil) ? "" : ucSharedUserDefaults.value(forKey:"TamayazUserID") as! String
    }
    
    
    func getinitiateGuestMode() -> Bool {
        return ucSharedUserDefaults.bool(forKey:"GuestMode")
    }
    
    func initiateGuestMode(tasfalg:Bool) {
        ucSharedUserDefaults.set(tasfalg, forKey:"GuestMode")
    }
}


