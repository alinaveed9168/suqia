//
//  GlobalObjects.swift
//  unionCoop
//
//  Created by Union Coop on 6/19/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit


func setCityAndArea(areaName:String,areaId:String,cityName:String,cityID:String){
    
    LoginOperation.shared.setGroceryAreaName(groceryAreaName: areaName)
    LoginOperation.shared.setGroceryCityName(groceryCityName: cityName)
    LoginOperation.shared.setGroceryCityID(groceryCityID: cityID)
    LoginOperation.shared.setGroceryAreaID(groceryAreaID: areaId)
}


func setBranchIDandBranchNAme(BName:String,BId:String){
    
    LoginOperation.shared.setBranchID(BID: BId)
    LoginOperation.shared.setBranchName(BName: BName)
}


func setCustomerData(customerModal:LoginUserDataModel)  {
    
    LoginOperation.shared.initiateGuestMode(tasfalg: true)
    let tamayaz : String = customerModal.cardNo!
    let shNo : String = customerModal.shno!
    let shAddressNo : String = customerModal.shno!
    let userToken : String = customerModal.token ?? ""
    let userId : String = customerModal.userID!
    
    Crashlytics.crashlytics().setUserID(userId)

    LoginOperation.shared.setTamayazUserID(token: userToken)
    LoginOperation.shared.setSingleSignOnuserID(userid: userId)
    LoginOperation.shared.setCardNumber(cardNumber: tamayaz)
    LoginOperation.shared.setSHNumber(shno: shNo)
    LoginOperation.shared.setSHAddressNo(addressNumber: shAddressNo)
    LoginOperation.shared.setLoginUserDetails(loginData: customerModal)
    let regCustomerData = RegCustomerData()
    regCustomerData.email  = customerModal.email
    regCustomerData.points  = customerModal.points
    regCustomerData.language  = customerModal.language
    regCustomerData.cardStatus  = customerModal.cardStatus
    regCustomerData.customerNo  = "\(customerModal.customerNo!)"
    regCustomerData.emirate  = ""
    regCustomerData.phoneNo  = ""
    regCustomerData.address  = ""
    regCustomerData.password  = ""
    regCustomerData.sSID  = ""
    regCustomerData.registeredCustomerId  = ""
    regCustomerData.loginB4  = ""
    regCustomerData.gender  = customerModal.gender
    regCustomerData.validEmail  = customerModal.email
    regCustomerData.fName  = customerModal.fName
    regCustomerData.totalPurchase  = customerModal.totalPurchase
    regCustomerData.familyNo  = ""
    regCustomerData.savedPercentage  = customerModal.savedPercentage
    regCustomerData.cardNo  = customerModal.cardNo
    regCustomerData.pOBox  = ""
    regCustomerData.recFlag  = ""
    regCustomerData.mobileNo  = customerModal.mobileNo
    regCustomerData.userID  = ""
    regCustomerData.nationality  = customerModal.nationality
//    regCustomerData.discountAmou  = customerModal.discountAmount
    regCustomerData.sHNO  = customerModal.shno
    regCustomerData.lName  = customerModal.lName
    regCustomerData.faceTouchUniqueID  = customerModal.faceTouchUniqueID
    regCustomerData.isStaff  = (customerModal.isStaff!) ? "1" : "0"
    regCustomerData.dob  = customerModal.dob
    regCustomerData.discountAmount  = customerModal.discountAmount
    regCustomerData.taxRegNo  = customerModal.taxRegNo
    regCustomerData.mobileNoWithMask  = customerModal.mobileNoWithMask
    regCustomerData.token  = customerModal.token ?? ""
    regCustomerData.redemptionAmount  = customerModal.redemptionAmount
    regCustomerData.shAddressNo  = customerModal.shAddressNo
    regCustomerData.staffID  = customerModal.staffID
    
    
    regCustomerData.isEmpRegisteredForTAS  = customerModal.isEmpRegisteredForTAS
    regCustomerData.enableTAS  = customerModal.enableTAS
    regCustomerData.isEmpRegisteredForTASCurrentDevice  = customerModal.isEmpRegisteredForTASCurrentDevice
    
    LoginOperation.shared.setISEmpRegisteredForTASFlag(tasfalg: customerModal.isEmpRegisteredForTAS!)
    LoginOperation.shared.setTASCurrentDeviceFlag(currentDeviceTasfalg:customerModal.isEmpRegisteredForTASCurrentDevice!)
    
    LoginOperation.shared.setUserInformation(arrinfo: regCustomerData)
    
    
}
