//
//  GlobalObjects.swift
//  unionCoop
//
//  Created by Union Coop on 6/19/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

extension String {
    subscript(i: Int) -> String {
        return String(self[index(startIndex, offsetBy: i)])
    }
    func makeSpaceToPercent() -> String {
        self.replacingOccurrences(of: " ", with: "%20")
    }
    
    func makeSpaceToEmpty() -> String {
        self.replacingOccurrences(of: " ", with: "")
    }
}

extension UIApplication {
    class var topViewController: UIViewController? { return getTopViewController() }
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController { return getTopViewController(base: nav.visibleViewController) }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController { return getTopViewController(base: selected) }
        }
        if let presented = base?.presentedViewController { return getTopViewController(base: presented) }
        return base
    }
    
    private class func _share(_ data: [Any],
                              applicationActivities: [UIActivity]?,
                              setupViewControllerCompletion: ((UIActivityViewController) -> Void)?) {
        let activityViewController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        setupViewControllerCompletion?(activityViewController)
        UIApplication.topViewController?.present(activityViewController, animated: true, completion: nil)
    }
    
    class func share(_ data: Any...,
                     applicationActivities: [UIActivity]? = nil,
                     setupViewControllerCompletion: ((UIActivityViewController) -> Void)? = nil) {
        _share(data, applicationActivities: applicationActivities, setupViewControllerCompletion: setupViewControllerCompletion)
    }
    class func share(_ data: [Any],
                     applicationActivities: [UIActivity]? = nil,
                     setupViewControllerCompletion: ((UIActivityViewController) -> Void)? = nil) {
        _share(data, applicationActivities: applicationActivities, setupViewControllerCompletion: setupViewControllerCompletion)
    }
}

extension UIViewController: UITextFieldDelegate {
 
    public func textField(_ textField: UITextField,
                          shouldChangeCharactersIn range: NSRange,
                          replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            //for Mobile Number
            if(textField.tag == 99){
                return (updatedText.count > 10) ? false : true;
            }
            
        }
        return true
    }
}

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            globalPrint(dataToPrint:"Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
    
    
    convenience init?(barcode: String) {
        let data = barcode.data(using: .ascii)
        guard let filter = CIFilter(name: "CICode128BarcodeGenerator") else {
            return nil
        }
        filter.setValue(data, forKey: "inputMessage")
        guard let ciImage = filter.outputImage else {
            return nil
        }
        self.init(ciImage: ciImage)
    }
}

extension UIImageView {
    public func imageFromURL(urlString: String) {
        
        let urlString1 = urlString.replacingOccurrences(of: " ", with: "%20")
        
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        activityIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(activityIndicator)
        }
        
        URLSession.shared.dataTask(with: NSURL(string: urlString1)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                globalPrint(dataToPrint:error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                activityIndicator.removeFromSuperview()
                self.image = image
            })
            
        }).resume()
    }
    
    func setImageFromURL(urlString:String)  {
        
        if(urlString.count > 0 ){
            var imgUrl = urlString
            imgUrl = imgUrl.replacingOccurrences(of: " ", with: "%20")
            let url = URL(string: imgUrl)!
//            self.af.setImage(withURL:url)
            let placeholderImage = UIImage(named: "Water-drop-Logo")!
            self.af.setImage(withURL: url, placeholderImage: placeholderImage)
        }else{
            self.image = UIImage(named: "Water-drop-Logo")!
        }
    }
}

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).cgPath
        self.layer.mask = maskLayer
    }
    
    public class func fromNib() -> Self {
        return fromNib(nibName: nil)
    }
    
    public class func fromNib(nibName: String?) -> Self {
        func fromNibHelper<T>(nibName: String?) -> T where T : UIView {
            let bundle = Bundle(for: T.self)
            let name = nibName ?? String(describing: T.self)
            return bundle.loadNibNamed(name, owner: nil, options: nil)?.first as? T ?? T()
        }
        return fromNibHelper(nibName: nibName)
    }
    
    func dropShadow(scale: Bool = true) {
        
        layer.masksToBounds = false
        
        layer.shadowColor = UIColor.black.cgColor
        
        layer.shadowOpacity = 1
        
        layer.shadowOffset = .zero
        
        layer.shadowRadius = 10
        
        layer.shouldRasterize = true
        
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
    }
    
    // OUTPUT 2
    
    func dropShadow2(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        
        
           layer.masksToBounds = false
           layer.shadowColor = color.cgColor
           layer.shadowOpacity = opacity
           layer.shadowOffset = offSet
           layer.shadowRadius = radius

           layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
           layer.shouldRasterize = true
           layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
//
//        layer.masksToBounds = false
//
//        layer.shadowColor = color.cgColor
//
//        layer.shadowOpacity = 0.5
//
//        layer.shadowOffset = offSet
//
//        layer.shadowRadius = 1
//
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//
//        layer.shouldRasterize = true
//
//        layer.rasterizationScale = UIScreen.main.scale
        
    }
}

func  LanguangeInt() -> Bool! {
    
    if MOLHLanguage.currentAppleLanguage() == "en" {
        return true
    }else{
        return false
    }
    
}

func openUrl(scheme: String) {
    if let url = URL(string: scheme) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:],
                                      completionHandler: {
                                        (success) in
                                        globalPrint(dataToPrint:"Open \(scheme): \(success)")
                                      })
        } else {
            let success = UIApplication.shared.openURL(url)
            globalPrint(dataToPrint:"Open \(scheme): \(success)")
        }
    }
}

func readJSONFromFile(fileName: String) -> Any?
{
    var json: Any?
    if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
        do {
            let fileUrl = URL(fileURLWithPath: path)
            // Getting data from JSON file using the file URL
            let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
            json = try? JSONSerialization.jsonObject(with: data)
        } catch {
            // Handle error here
        }
    }
    return json
}
func getCurrentLanguagevalue() -> Bool{
    let str = MOLHLanguage.currentAppleLanguage()
    return (str == "en") ? true : false
}
func getCurrentLanguageString() -> String{
       let str = MOLHLanguage.currentAppleLanguage()
        return (str == "en") ? "en" : "ar"
}

func returnAttachedAED(aedStr:String) -> String {
    let aed =  UCAED.localized() + " "
    let mainString = aed + aedStr
    return mainString
}
func dialNumber(number : String) {

 if let url = URL(string: "tel://\(number)"),
   UIApplication.shared.canOpenURL(url) {
      if #available(iOS 10, *) {
        UIApplication.shared.open(url, options: [:], completionHandler:nil)
       } else {
           UIApplication.shared.openURL(url)
       }
   } else {
            // add error message here
   }
}
func getNewDeviceID() -> String? {
    
    var uuid: String
    let keychain = KeychainItemWrapper(identifier: "AC_APP_UUID", accessGroup: nil)
    var keychainUUID: String? = nil
    keychainUUID = keychain[kSecAttrAccount as String] as? String

    if keychainUUID == nil || keychainUUID?.count == 0 {
        uuid = UUID().uuidString
        keychain[kSecAttrAccount as String] = uuid as AnyObject
    } else {
        uuid = keychain[kSecAttrAccount as String] as? String ?? ""
    }
    return uuid
}

var isDeviceJailbroken: Bool {

    guard TARGET_IPHONE_SIMULATOR != 1 else { return false }

    // Check 1 : existence of files that are common for jailbroken devices
    if FileManager.default.fileExists(atPath: "/Applications/Cydia.app")
    || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
    || FileManager.default.fileExists(atPath: "/bin/bash")
    || FileManager.default.fileExists(atPath: "/usr/sbin/sshd")
    || FileManager.default.fileExists(atPath: "/etc/apt")
    || FileManager.default.fileExists(atPath: "/private/var/lib/apt/")
    || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!) {

        return true
    }

    // Check 2 : Reading and writing in system directories (sandbox violation)
    let stringToWrite = "Jailbreak Test"
    do {
        try stringToWrite.write(toFile:"/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
        // Device is jailbroken
        return true
    } catch {
        return false
    }
}

func colorWithRGB(red: CGFloat, green:CGFloat , blue:CGFloat, alpha:CGFloat = 1.0) -> UIColor {
    // Create color object, specifying alpha as well
    let color = UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    return color
}

func colorWithHexString(hexString: String, alpha:CGFloat = 1.0) -> UIColor {
    
    // Convert hex string to an integer
    let hexint = Int(intFromHexString(hexStr: hexString))
    let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
    let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
    let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    
    // Create color object, specifying alpha as well
    let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
    return color
}

func intFromHexString(hexStr: String) -> UInt32 {
    var hexInt: UInt32 = 0
    // Create scanner
    let scanner: Scanner = Scanner(string: hexStr)
    // Tell scanner to skip the # character
    scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
    // Scan hex value
    scanner.scanHexInt32(&hexInt)
    return hexInt
}



func openGoogleMap(withLat:String,withLong:String) {
    
    if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
        
        if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(withLat),\(withLong)&directionsmode=driving") {
            UIApplication.shared.open(url, options: [:])
        }}
    else {
        //Open in browser
        if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(withLat),\(withLong)&directionsmode=driving") {
            UIApplication.shared.open(urlDestination)
        }
    }
    
}
func addBGinBottomBAr(){
    
//    let screenSize: CGRect = UIScreen.main.bounds
//    var bgImage :UIImageView
//    let image: UIImage = UIImage(named: "select_language")!
//    bgImage = UIImageView(image: image)
//    bgImage.frame = screenSize
//    ucTabarVC.view.addSubview(bgImage)
}
//Navigate to Class with respect to deeplink
func toReturnNavigationController(depplinkString:String,withName:String) -> BaseViewController{
    
//    let pageCode = Deeplink.shared.getPageCode(deepLink: depplinkString)
//    let pageValue = Deeplink.shared.getPageValue(deepLink: depplinkString)
  var action = BaseViewController()
//
//    switch (pageCode){
//    case Deeplink.OUR_BRANCH:
//        action = OurBranchesViewController.instantiate(fromAppStoryboard: .generalStoryboard)
//        break;
//    case Deeplink.OUR_PROJECTS:
//        action  = OurProjectsViewController.instantiate(fromAppStoryboard: .generalStoryboard)
//        break;
//    case Deeplink.ORDER_HISTORY:
//        action = GroceryOrderHistoryViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.MY_ORDER:
//        action = GroceryOrderHistoryViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.MY_CARD:
//        action = ProductsListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.ADDRESS_LIST:
//        action = AddressListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.WEB,Deeplink.IMAGE:
//        action = WebViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.MAGAZINE:
//        action =  WebViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.SMART_RECEIPT:
//        action = SmartRecieptVC.instantiate(fromAppStoryboard: .generalStoryboard)
//        break;
//    case Deeplink.NOTIFICATION:
//        action = NotificationsVC.instantiate(fromAppStoryboard: .generalStoryboard)
//        break;
//    case Deeplink.RECIPE_CATEGORY:
//        action = ProductsListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.STORE,Deeplink.ITEMS,Deeplink.VENDOR:
//        action = ProductsListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.SHOPONLINE:
//        ucTabarVC.testController.tabBar?.selectedTabItem(1, animated: true)
//        break;
//    case Deeplink.CATEGORY:
//        action = SubCategoriesDetailsVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        action.withSubCategoriesTitle = 0
//        action.withSubCategoriesID = pageValue
//        break;
//    case Deeplink.HOME:
//        ucTabarVC.testController.tabBar?.selectedTabItem(0, animated: true)
//        break;
//    case Deeplink.SHAREHOLDER:
//        ucTabarVC.testController.tabBar?.selectedTabItem(2, animated: true)
//        break;
//    case Deeplink.CART:
//        ucTabarVC.testController.tabBar?.selectedTabItem(3, animated: true)
//        break;
//    case Deeplink.MORE:
//        ucTabarVC.testController.tabBar?.selectedTabItem(4, animated: true)
//        break;
//    case Deeplink.FAVOURITE:
//        action = FavouriteListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.Complai_Suggestion:
//        action = ComplainSuggestionVC.instantiate(fromAppStoryboard: .generalStoryboard)
//        break;
//    case Deeplink.RECIPE:
//        action = RecipeDetailViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    case Deeplink.All_RECIPES_List:
//        action = RecipeHomeViewController.instantiate(fromAppStoryboard: .mainStoryboard)
//        break;
//    default:
//        action = ProductsListVC.instantiate(fromAppStoryboard: .mainStoryboard)
//        break
//    }
//    action.deeplinkTitle = pageValue
//    action.deeplinkValue = pageCode
//    action.nameTitle = withName
    
  return action
}

func isSmallDevice() -> Bool {
   var isSmall = false
    
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            isSmall = true //"iPhone 5 or 5S or 5C"
        case 1334:
            isSmall = true// "iPhone 6/6S/7/8"
        case 1920, 2208:
            isSmall = true //"iPhone 6+/6S+/7+/8+"
        case 2436:
            isSmall = false //"iPhone X"
        case 2688:
            isSmall = false //"iPhone Xs Max"
        case 1792:
            isSmall = false //"iPhone Xr"
        default:
            isSmall = false // "unknown"
        }
    }
   return isSmall
}

func isGuestModeWithMessage(viewcontroller:UIViewController) ->Bool {
    var isLogin = false
    if LoginOperation.shared.getTamayazUserID() == nil || LoginOperation.shared.getTamayazUserID() == ""{
        
//        let alert = UIAlertController(title: UCAlert.localized(), message: UCGuest_User.localized(), preferredStyle: .alert)
//
//        alert.addAction(UIAlertAction(title: UCYES.localized(), style: .default, handler: { action in
//            viewcontroller.navigationController?.popToLogoutViewController(ofClass: UserLoginVC.self)
//        }))
//        alert.addAction(UIAlertAction(title: UCNO.localized(), style: .cancel, handler: nil))
//
//        viewcontroller.present(alert, animated: true)
        
        isLogin = false
    }else{
        isLogin = true
    }
    return isLogin
}

func isGuestMode() ->Bool {
    var isLogin = true
    if LoginOperation.shared.getTamayazUserID() == nil {
        isLogin = true
    }else{
        isLogin = false
    }
    return isLogin
}

func removeAllStaticData(){
    LoginOperation.shared.setTamayazUserID(token: nil)
    LoginOperation.shared.setCardNumber(cardNumber: nil)
    LoginOperation.shared.setSHNumber(shno: nil)
    LoginOperation.shared.setSHAddressNo(addressNumber: nil)
    ucSharedUserDefaults.removeObject(forKey: UCLoginUserInfo)
    LoginOperation.shared.setGroceryAreaName(groceryAreaName: nil)
    LoginOperation.shared.setGroceryAreaID(groceryAreaID: nil)
    LoginOperation.shared.setGroceryCityName(groceryCityName: nil)
    LoginOperation.shared.setGroceryCityID(groceryCityID: nil)
    LoginOperation.shared.setGrocerySelectedAddress(address_id: nil)
    LoginOperation.shared.setSingleSignOnuserID(userid: nil)

}

func replaceAEDtoEmpty(depplinkString:String) -> String{
    var replaceStr = depplinkString
    replaceStr = replaceStr.replacingOccurrences(of: "AED", with: "")
    return replaceStr
}



func validateEntry(_ checkText: String?, msg messageText: String?) -> Bool {
    if ((checkText?.trimmingCharacters(in: CharacterSet.whitespaces).count ?? 0) == 0) {
        Utility.UtilityMethod.showUIAlertView(title: UCAlert.localized(), message: messageText!, alertTypes: UCFAIL)
        return false
    }
    return true
}

func isGoldCard() -> Bool {
    let isGold = false
    let strSecondChar = String(LoginOperation.shared.getCardNumber().prefix(2))
    if strSecondChar == "99" {
        return true
    }
    return isGold
}

func langSwift() -> Bool {
    return (MOLHLanguage.currentAppleLanguage() == "en") ? true : false
}

class MyUserDefaults: NSObject{
    static let instance = MyUserDefaults()
    override init() {}
    
    
    func setObject<T:Mappable>(_ object:T?,forKey:String){
        if(object == nil){
            ucSharedUserDefaults.set("", forKey: forKey)
        }else{
            ucSharedUserDefaults.set(object?.toJSONString(), forKey: forKey)
        }
    }
    
    func setObjectArray<T: Mappable>(_ array:[T],forKey:String){
        if(array.count == 0){
            ucSharedUserDefaults.set("", forKey: forKey)
        }else{
            ucSharedUserDefaults.set(array.toJSONString(), forKey: forKey)
        }
    }
    
    func getObject<T:Mappable>(_ type: T.Type,forKey:String) -> T?{
        let text = ucSharedUserDefaults.string(forKey: forKey)
        if(text == nil || text?.count == 0){
            return nil
        }else{
            return Mapper<T>().map(JSONString: text!)
        }
    }
    
    func getObjectArray<T:Mappable>(_ type: [T].Type,forKey:String) -> [T]?{
        let text = ucSharedUserDefaults.string(forKey: forKey)
        if(text == nil || text?.count == 0){
            return nil
        }else{
            return Mapper<T>().mapArray(JSONString: text!)
        }
    }
}


func takeScreenShot(_ view: UIView?) -> UIImage? {
    if UIScreen.main.responds(to: #selector(getter: UIScreen.scale)) {
        UIGraphicsBeginImageContextWithOptions(view?.bounds.size ?? CGSize.zero, false, UIScreen.main.scale)
    } else {
        UIGraphicsBeginImageContext(view?.bounds.size ?? CGSize.zero)
    }
    
    if let context = UIGraphicsGetCurrentContext() {
        view?.layer.render(in: context)
    }
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let imgData = image?.pngData()
    if let imgData = imgData {
        //        [imgData writeToFile:@"screenshot.png" atomically:YES];
        return UIImage(data: imgData)
    }
    return nil
}
extension UIStackView {

    func removeFirstArrangedView() {
           
           for item in arrangedSubviews {
               removeArrangedSubview(item)
               item.removeFromSuperview()
               break
           }
       }
     
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}


func convertImageToBase64String (img: UIImage) -> String {
    return img.jpegData(compressionQuality: 0.5)?.base64EncodedString() ?? ""
}


func globalPrint(dataToPrint:Any){
    print(dataToPrint)
}

func loadJson(filename fileName: String) -> NSDictionary? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        do {
            let data = try Data(contentsOf: url)
            let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            if let dictionary = object as? NSDictionary {
                return dictionary
            }
        } catch {
            print("Error!! Unable to parse  \(fileName).json")
        }
    }
    return nil
}
