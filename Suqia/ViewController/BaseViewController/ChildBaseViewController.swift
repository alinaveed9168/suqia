//
//  BaseViewController.swift
//  Bringness
//
//  Created by Union Coop on 3/20/20.
//  Copyright © 2020 Techwardsolutions. All rights reserved.
//

import UIKit

class ChildBaseViewController: UIViewController {
    let regularFont = UIFont(name:"Poppins-Regular",size:13)
     let boldFont = UIFont(name:"Poppins-Bold",size:15)
    
    
    @IBOutlet weak var headerUIView:HeaderView!{
        didSet{
            self.headerUIView.bckArrow.addTapGesture(tapNumber: 1, target: self, action:  #selector(btnDismiss))
        }
    }
    @objc func btnDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarColor(color: UIColor(named: "BBOrange")!)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                
//        let backBtn = UIBarButtonItem.rightbarItem(with: UIImage(named: "back_white"), target: self, action: #selector(calledBack))
//        navigationItem.leftBarButtonItem = backBtn
    }

    @objc func calledBack()  {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backClicked(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func ConvertDictToString(dictionary:NSDictionary) -> String {
           let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
           let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
           return jsonString
           
       }
       
    
    func validateEntry(_ checkText: String?, msg messageText: String?) -> Bool {
        if ((checkText?.trimmingCharacters(in: CharacterSet.whitespaces).count ?? 0) == 0) {
            Utility.UtilityMethod.showUIAlertView(title: "ALERT".localized(), message: messageText!, alertTypes: BBFAIL)
            return false
        }
        return true
    }

    @objc func pressBack()
    {
        _ = navigationController?.popViewController(animated: true)
    }


}
