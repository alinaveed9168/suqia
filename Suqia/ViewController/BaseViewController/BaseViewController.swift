//
//  BaseViewController.swift
//  Bringness
//
//  Created by Union Coop on 3/20/20.
//  Copyright © 2020 Techwardsolutions. All rights reserved.
//

import UIKit
class BaseViewController: UIViewController {
    let regularFont = UIFont(name:"Poppins-Regular",size:13)
     let boldFont = UIFont(name:"Poppins-Bold",size:15)

    var nameTitle = ""
    var deeplinkTitle = ""
    var deeplinkValue = ""
    var magazinePdfUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarColor(color: UIColor(named: "BBOrange")!)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    }
    
   
    
        
    @IBAction func backClicked(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validateEntry(_ checkText: String?, msg messageText: String?) -> Bool {
        if ((checkText?.trimmingCharacters(in: CharacterSet.whitespaces).count ?? 0) == 0) {
            Utility.UtilityMethod.showUIAlertView(title: "ALERT".localized(), message: messageText!, alertTypes: BBFAIL)
            return false
        }
        return true
    }
    func ConvertDictToString(dictionary:NSDictionary) -> String {
              let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
              let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
              return jsonString
              
          }
  
    
    
    
    
    
    func callFavouriteRestaurant(RestaurantID : String,serviceName : String,completionHandler: @escaping (Any?) -> Swift.Void) {

           
         }
  }
