//
//  BaseViewController.swift
//  Bringness
//
//  Created by Union Coop on 3/20/20.
//  Copyright © 2020 Techwardsolutions. All rights reserved.
//

import UIKit

class UserBaseViewController: UIViewController {
    let regularFont = UIFont(name:"Poppins-Regular",size:13)
    let boldFont = UIFont(name:"Poppins-Bold",size:15)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func backClicked(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validateEntry(_ checkText: String?, msg messageText: String?) -> Bool {
        if ((checkText?.trimmingCharacters(in: CharacterSet.whitespaces).count ?? 0) == 0) {
            Utility.UtilityMethod.showUIAlertView(title: "ALERT".localized(), message: messageText!, alertTypes: BBFAIL)
            return false
        }
        return true
    }
    
    /// pop back n viewcontroller
    ///self.popBack(3)
    func popNumberOfControllerBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    
    /// pop back to specific viewcontroller
    //    self.popBack(toControllerType: MyViewController.self)
    func popPerticularBack<T: UIViewController>(toControllerType: T.Type) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: toControllerType) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
    
    func ConvertDictToString(dictionary:NSDictionary) -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        return jsonString
        
    }
    
    
    
      func addBackBtn()
           {
    //           let image = Localize.currentLanguage() == "en" ? UIImage(named: "back_left") : UIImage(named: "back_right")
            let image =  UIImage(named: "iconBack")

               let backBtn = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(UserBaseViewController.pressBack))
               backBtn.tintColor = UIColor.white
               backBtn.isAccessibilityElement = true
               backBtn.accessibilityLabel = "BACK_BUTTON".localized()
    //           if isDeviceLanguageArabic()
    //           {
    //               navigationItem.rightBarButtonItem = backBtn
    //               navigationItem.setHidesBackButton(true, animated: false)
    //           }
    //           else
    //           {
                   navigationItem.leftBarButtonItem = backBtn
                   //navigationItem.rightBarButtonItem = nil
    //           }
           }
           
           @objc func pressBack()
           {
               _ = navigationController?.popViewController(animated: true)
           }

}
