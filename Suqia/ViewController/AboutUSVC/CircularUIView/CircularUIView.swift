//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class CircularUIView: UIView{
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var widgetData : [WidgetData] = []
    var collectionType: Int = 0
    
    
    var bannerWidget: Int = 0
    
   
    var naviController = UINavigationController()
    
    @IBOutlet weak var view: UIView!

    let kCONTENT_XIB_NAME = "CircularUIView"

    @IBOutlet weak var view1: MainCircularView!
    @IBOutlet weak var view2: MainCircularView!
    @IBOutlet weak var view3: MainCircularView!
    @IBOutlet weak var view4: MainCircularView!


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
    }
    
    func setTimer(){
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in
            
            UIView.animate(withDuration: 0.5) {
                self.view1.progressBar.value = 100.0 - self.view2.progressBar.value;
                self.view2.progressBar.value = 100.0 - self.view2.progressBar.value;
                self.view3.progressBar.value = 100.0 - self.view3.progressBar.value;
                self.view4.progressBar.value = 100.0 - self.view4.progressBar.value;
            }
        }
        
      
    }
    
}

