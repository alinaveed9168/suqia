//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class imageAndDescView: UIView{
    
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var innerview: UIView!

    let kCONTENT_XIB_NAME = "imageAndDescView"

    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var titleMain: UILabel?
    @IBOutlet weak var titleDesc: UILabel?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
        innerview.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 8)
    }
    
    
}

