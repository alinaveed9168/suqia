//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//
import UIKit

class ourProgramCell: UITableViewCell {
    
    // MARK: Private Outlets
    @IBOutlet  var labelDesc: UILabel!

    // MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelDesc.text = ""
        backgroundColor = .white
    }
    
    // MARK: Public
    func configure(with title: String,desc: String,withImage:String,withFontSize:String) {
        labelDesc.text = "Engaing Youth"
    }
}
