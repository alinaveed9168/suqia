//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//
import UIKit

class roundImgTitleDescCell: UITableViewCell {
    
    // MARK: Private Outlets
    @IBOutlet  var labelTitle: UILabel!
    @IBOutlet  var labelDesc: UILabel!
    @IBOutlet  var img: UIImageView!
    @IBOutlet  var imgView: UIView!
    
    @IBOutlet  var imgWidth: NSLayoutConstraint!

    // MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelTitle.text = ""
        labelDesc.text = ""
        img.image = UIImage(named: "logonMain")
        backgroundColor = .white
    }
    
    // MARK: Public
    func configure(with title: String,desc: String,withImage:String,withFontSize:String) {
        labelTitle.text = title
        labelDesc.text = desc
        imgWidth.constant  =  (withImage.count == 0) ? 0 : 140
        img.imageFromURL(urlString: withImage)
        
        imgView.dropShadow()
        
//        imgView.clipsToBounds = true
        if(withFontSize == "1"){
            labelTitle.font.withSize(labelTitle.font.pointSize - 5)
            labelTitle.textAlignment = .left
        }
    }
}
