//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class whyWaterView: UIView{
    
    
    @IBOutlet weak var segmentView1: UIView!
    @IBOutlet weak var segmentView2: UIView!
    @IBOutlet weak var segmentView3: UIView!

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var innerview: UIView!

    let kCONTENT_XIB_NAME = "whyWaterView"

    var circleWidgetview = CircularUIView()
    var imageAndDescWidgetview = imageAndDescView()
    var topBanner_view = topBannerView()

    @IBOutlet weak var main_circleWidgetview: UIView!
    @IBOutlet weak var imgWidgetView: UIView!

    var arr:[aboutUS] = []
    var dashboard_Blocks = [Blocks]()
    @IBOutlet private weak var tableViews: UITableView!


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
        innerview.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 8)
    }
    
    func setWhyWaterDate(arrts:[aboutUS] ,setBlocks:[Blocks] )  {
        
        
        
        segmentView1.shadowP(2, height: 3)
        segmentView2.shadowP(2, height: 3)
        segmentView3.shadowP(2, height: 3)

        
        CellFactory.registerCells(for: self.tableViews)
        self.tableViews.register(UINib(nibName: "imgTitleDescCell", bundle: nil), forCellReuseIdentifier: "imgTitleDescCell")
        
         arr = arrts
         dashboard_Blocks = setBlocks
        self.tableViews.reloadData()
        self.Add3WidgetUIView()
        self.imgAndDescWidgetUIView()
    }
    
    
     
    
}



// MARK: - UITableViewDataSource
extension whyWaterView: UITableViewDataSource,UITableViewDelegate {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : imgTitleDescCell = tableView.dequeueReusableCell(withIdentifier: "imgTitleDescCell", for: indexPath) as! imgTitleDescCell
       
        let myArr = arr[indexPath.row] as aboutUS
        
        cell.configure(with: myArr.Title,desc: myArr.Desc,withImage: myArr.image,withFontSize: "0")
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
  
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        topBanner_view =  topBannerView(frame: CGRect(x: 0, y: 0, width: innerview.frame.width - 50, height: innerview.frame.height))
        
        
        let RedisgnData:Blocks = self.dashboard_Blocks[1]
        let typeForBlocks = Int(RedisgnData.type!)
        let getHeight:CGFloat = 155
        let hideView =  38
        
        let myArr = arr[section]

        
        topBanner_view.setWidgetDataCollection(dt: myArr, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        
        topBanner_view.btnTitle.tag = section
        topBanner_view.innerView.removeShadowP()
        topBanner_view.titlelblb.textAlignment = .left
        topBanner_view.view.backgroundColor = .white
        let myfont = UIFont.mySystemFont(ofSize: 20)
        topBanner_view.titlelblb.font = myfont
        return topBanner_view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 80
   }
}




extension whyWaterView {
    
    func Add3WidgetUIView()  {
        
        circleWidgetview =  CircularUIView(frame: CGRect(x: 0, y: 0, width: main_circleWidgetview.frame.width - 50, height: main_circleWidgetview.frame.height))
        
//        circleWidgetview.bannerWidget = 1
//        let RedisgnData:Blocks = self.dashboard_Blocks[1]
//        let typeForBlocks = Int(RedisgnData.type!)
//        let getHeight:CGFloat = 155
//        let hideView =  38
//        circleWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        circleWidgetview.setTimer()
        main_circleWidgetview.addSubview(circleWidgetview)
    }
    
    func imgAndDescWidgetUIView()  {
        
        imageAndDescWidgetview =  imageAndDescView(frame: CGRect(x: 0, y: 0, width: imgWidgetView.frame.width - 50, height: imgWidgetView.frame.height))
        
//        addWidgetview.bannerWidget = 1
//        let RedisgnData:Blocks = self.dashboard_Blocks[1]
//        let typeForBlocks = Int(RedisgnData.type!)
//        let getHeight:CGFloat = 155
//        let hideView =  38
//        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        imgWidgetView.addSubview(imageAndDescWidgetview)
    }
    
    
}
