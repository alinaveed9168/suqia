//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class ourProgramsView: UIView{
    
    
    @IBOutlet weak var segmentView1: UIView!
    @IBOutlet weak var segmentView2: UIView!
    @IBOutlet weak var segmentView3: UIView!

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var innerview: UIView!

    let kCONTENT_XIB_NAME = "ourProgramsView"

    var circleWidgetview = CircularUIView()
    var imageAndDescWidgetview = imageAndDescView()
    var topBanner_view = topBannerView()


    var arr:[aboutUS] = []
    var dashboard_Blocks = [Blocks]()
    @IBOutlet private weak var tableViews: UITableView!


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
        innerview.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 8)
    }
    
    func setOurPrograms(arrts:[aboutUS] ,setBlocks:[Blocks] )  {
        
        
        
        segmentView1.shadowP(2, height: 3)
        segmentView2.shadowP(2, height: 3)
        segmentView3.shadowP(2, height: 3)
        self.tableViews.sectionHeaderHeight = UITableView.automaticDimension
        self.tableViews.estimatedSectionHeaderHeight = 25;
        self.tableViews.tableFooterView = UIView()

        
        CellFactory.registerCells(for: self.tableViews)
        self.tableViews.register(UINib(nibName: "ourProgramCell", bundle: nil), forCellReuseIdentifier: "ourProgramCell")
        
         arr = arrts
         dashboard_Blocks = setBlocks
        self.tableViews.reloadData()
       
    }
    
    
     
    
}



// MARK: - UITableViewDataSource
extension ourProgramsView: UITableViewDataSource,UITableViewDelegate {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 3
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ourProgramCell = tableView.dequeueReusableCell(withIdentifier: "ourProgramCell", for: indexPath) as! ourProgramCell
       
        let myArr = arr[0] as aboutUS
        
        cell.configure(with: myArr.Title,desc: myArr.Desc,withImage: myArr.image,withFontSize: "0")
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
  
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        topBanner_view =  topBannerView(frame: CGRect(x: 0, y: 0, width: innerview.frame.width - 50, height: innerview.frame.height))
        
        
        let RedisgnData:Blocks = self.dashboard_Blocks[1]
        let typeForBlocks = Int(RedisgnData.type!)
        let getHeight:CGFloat = 155
        let hideView =  38
        
        var myArr = arr[section]
        myArr.Title =
        "Water is a fundamental human need and a driver for sustainable growth, yet water scarcity affects more than 40% of the global population and is projected to rise. While 2.6 billion people have gained access to improved drinking water sources since 1990, yet 844 million people are still struggling with water source"
        
        topBanner_view.setWidgetDataCollection(dt: myArr, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        
        topBanner_view.btnTitle.tag = section
        topBanner_view.innerView.removeShadowP()
        topBanner_view.titlelblb.textAlignment = .center
        topBanner_view.view.backgroundColor = .white
        let myfont = UIFont.mySystemFont(ofSize: 12)
        topBanner_view.titlelblb.font = myfont
        return topBanner_view
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//
//
//       return 80
//   }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 73
    }
}


