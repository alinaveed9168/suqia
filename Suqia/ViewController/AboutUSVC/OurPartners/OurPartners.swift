//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation
import AZExpandable


class OurPartners: BaseViewController {
    
    @IBOutlet weak var segmentView1: UIView!
    @IBOutlet weak var segmentView2: UIView!
    @IBOutlet weak var segmentView3: UIView!
   
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerWidgetView: UIView!

    
    @IBOutlet weak var bannerImage: UIImageView!
    // MARK: IBOutlet's
    @IBOutlet private weak var tableViews: UITableView!
    var dashboard_Data = HomeRedesign()
    var dashboard_Blocks = [Blocks]()
    
    var addview = AboutBannerUIView()
    var addWidgetview = AboutBannerUIView()

    var topBanner_view = topBannerView()

    
    var twoDimensionalArray = [
        
        ExpandableNames(isExpanded: false),
        ExpandableNames(isExpanded: false),
        ExpandableNames(isExpanded: false)]

    let arr = [aboutUS(Title: "WHO WE ARE", Desc: "Established in 2015, the UAE Water Aid Foundation, Suqia UAE, an entity under the umbrella of the Mohammed bin Rashid Al Maktoum Global Initiatives foundation, is a non-profit organisation that provides humanitarian aid around the world and helps communities that suffer from water scarcity by providing them with potable water", image: "https://suqia.ae/web/image/428/about_1.jpg"),aboutUS(Title: "OUR CORE", Desc: "In October 2015, His Highness Sheikh Mohammed bin Rashid Al Maktoum, Vice President and Prime Minister of the UAE and Ruler of Dubai, established the Mohammed bin Rashid Al Maktoum Global Initiatives foundation (MBRGI)", image: "https://suqia.ae/web/image/431/about_2.jpg"),aboutUS(Title: "OUR FOUNDATION", Desc: "In 2014, His Highness Sheikh Mohammed bin Rashid Al Maktoum, Vice President and Prime Minister of the UAE and Ruler of Dubai launched the Suqia initiative to provide clean water to five million", image: "https://suqia.ae/web/image/430/about_3.jpg")]
    
    // MARK: Private Properties
  
    
    @IBOutlet weak var headerUIView:HeaderView!{
        didSet{
            self.headerUIView.bckArrow.addTapGesture(tapNumber: 1, target: self, action:  #selector(btnDismiss))
        }
    }
    @objc func btnDismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            bannerImage .imageFromURL(urlString: "https://suqia.ae/web/image/427/about_banner.jpg")
            segmentView1.shadowP(2, height: 3)
            segmentView2.shadowP(2, height: 3)
            segmentView3.shadowP(2, height: 3)

        
            CellFactory.registerCells(for: self.tableViews)
            self.tableViews.register(UINib(nibName: "AboutBannerView", bundle: nil), forCellReuseIdentifier: "AboutBannerView")
        
            fetchCategoriesFromServer()

        
    }
    
    func fetchCategoriesFromServer()  {
       
      
        let responseData = loadJson(filename: "AboutUS")!
        self.dashboard_Data = HomeRedesign(JSON: responseData as! [String : Any])!
        
        if self.dashboard_Data.code ==  "1" {
                    self.dashboard_Blocks = (self.dashboard_Data.data?.blocks)!
                    self.AddaboutBannerUIView()
            self.Add3WidgetUIView()
            }
//    }
    //            }
    //        }
}
    
}



// MARK: - UITableViewDataSource
extension OurPartners: UITableViewDataSource,UITableViewDelegate {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arr.count
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CenteredLabelCell = tableView.dequeueReusableCell(withIdentifier: "CenteredLabelCell", for: indexPath) as! CenteredLabelCell
        let myArr = arr[indexPath.row]
        
        cell.configure(with: myArr as! String,withImage: myArr as! String,withFontSize: "0")
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
  
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        topBanner_view =  topBannerView(frame: CGRect(x: 0, y: 0, width: bannerWidgetView.frame.width - 50, height: bannerWidgetView.frame.height))
        
        
        let RedisgnData:Blocks = self.dashboard_Blocks[1]
        let typeForBlocks = Int(RedisgnData.type!)
        let getHeight:CGFloat = 155
        let hideView =  38
        
        let myArr = arr[section]

        
//        topBanner_view.setWidgetDataCollection(dt: myArr, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        
        topBanner_view.btnTitle.tag = section

        topBanner_view.view.backgroundColor = .white
        return topBanner_view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 40
   }
}




extension OurPartners {
    
    func AddaboutBannerUIView()  {
        
        addview =  AboutBannerUIView(frame: CGRect(x: 0, y: 0, width: bannerView.frame.width - 50, height: bannerView.frame.height))
        let RedisgnData:Blocks = self.dashboard_Blocks[0]
        let typeForBlocks = Int(RedisgnData.type!)
        let getHeight:CGFloat = 79
        let hideView = ((RedisgnData.title == "" ||  RedisgnData.title == nil)
                            && (RedisgnData.viewMore == "" ||  RedisgnData.viewMore == nil)) ? 0 : 38
        
        
        addview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
       
        bannerView.addSubview(addview)
    }
    
    
    func Add3WidgetUIView()  {
        
        addWidgetview =  AboutBannerUIView(frame: CGRect(x: 0, y: 0, width: bannerWidgetView.frame.width - 50, height: bannerWidgetView.frame.height))
        
        addWidgetview.bannerWidget = 1
        let RedisgnData:Blocks = self.dashboard_Blocks[1]
        let typeForBlocks = Int(RedisgnData.type!)
        let getHeight:CGFloat = 155
        let hideView =  38
        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
       
        bannerWidgetView.addSubview(addWidgetview)
    }
}
