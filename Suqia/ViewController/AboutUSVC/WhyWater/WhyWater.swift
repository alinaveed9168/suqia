//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit
import Foundation
import AZExpandable


class WhyWater: ChildBaseViewController {
    
   
    
//    @IBOutlet weak var bannerWidgetView: UIView!
//    @IBOutlet weak var imgWidgetView: UIView!

    
    @IBOutlet weak var bannerImage: UIImageView!
    // MARK: IBOutlet's
//    @IBOutlet private weak var tableViews: UITableView!
    var dashboard_Data = HomeRedesign()
    var dashboard_Blocks = [Blocks]()
    
//    var addWidgetview = CircularUIView()
//    var imageAndDescWidgetview = imageAndDescView()
//    var topBanner_view = topBannerView()
    
    var whyWater_view = whyWaterView()
    var ourProgramsView_view = ourProgramsView()
    var ourPartnersView_view = ourPartnersView()

    
    
    @IBOutlet weak var bottomView: ucsCustomView!

    var twoDimensionalArray = [
        
        ExpandableNames(isExpanded: false),
        ExpandableNames(isExpanded: false),
        ExpandableNames(isExpanded: false)]

    let arr = [aboutUS(Title: "Together We Can make Water Available TO ALL", Desc: "Established in 2015, the UAE Water Aid Foundation, Suqia UAE, an entity under the umbrella of the Mohammed bin Rashid Al Maktoum Global Initiatives foundation, is a non-profit organisation that provides humanitarian aid", image: "https://suqia.ae/web/image/428/about_1.jpg"),aboutUS(Title: "OUR CORE", Desc: "In October 2015, His Highness Sheikh Mohammed bin Rashid Al Maktoum, Vice President and Prime Minister of the UAE and Ruler of Dubai, established the Mohammed bin Rashid Al Maktoum Global Initiatives foundation (MBRGI)", image: "https://suqia.ae/web/image/431/about_2.jpg"),aboutUS(Title: "OUR FOUNDATION", Desc: "In 2014, His Highness Sheikh Mohammed bin Rashid Al Maktoum, Vice President and Prime Minister of the UAE and Ruler of Dubai launched the Suqia initiative to provide clean water to five million", image: "https://suqia.ae/web/image/430/about_3.jpg")]
    
    // MARK: Private Properties
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
            bannerImage .imageFromURL(urlString: "https://suqia.ae/web/image/427/about_banner.jpg")

        
//            CellFactory.registerCells(for: self.tableViews)
//            self.tableViews.register(UINib(nibName: "imgTitleDescCell", bundle: nil), forCellReuseIdentifier: "imgTitleDescCell")
        
            fetchCategoriesFromServer()

        
    }
    
    func fetchCategoriesFromServer()  {
       
      
        let responseData = loadJson(filename: "AboutUS")!
        self.dashboard_Data = HomeRedesign(JSON: responseData as! [String : Any])!
        
        if self.dashboard_Data.code ==  "1" {
                    self.dashboard_Blocks = (self.dashboard_Data.data?.blocks)!
                    //self.addWhyWaterView()
                    self.addOurProgramsView()
                    //self.addOurPartnersView()
           
            }
//    }
    //            }
    //        }
}
    
}


extension WhyWater
{
    func addWhyWaterView()  {
        
        whyWater_view =  whyWaterView(frame: CGRect(x: 0, y: 0, width: bottomView.frame.width - 50, height: bottomView.frame.height))
    
    //        let RedisgnData:Blocks = self.dashboard_Blocks[1]
    //        let typeForBlocks = Int(RedisgnData.type!)
    //        let getHeight:CGFloat = 155
    //        let hideView =  38
    //        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        whyWater_view.setWhyWaterDate(arrts: arr, setBlocks: self.dashboard_Blocks)
        bottomView.addSubview(whyWater_view)
        }
    
    
    func addOurProgramsView()  {
        
        ourProgramsView_view =  ourProgramsView(frame: CGRect(x: 0, y: 0, width: bottomView.frame.width - 50, height: bottomView.frame.height))
    
    //        let RedisgnData:Blocks = self.dashboard_Blocks[1]
    //        let typeForBlocks = Int(RedisgnData.type!)
    //        let getHeight:CGFloat = 155
    //        let hideView =  38
    //        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        ourProgramsView_view.setOurPrograms(arrts: arr, setBlocks: self.dashboard_Blocks)
        bottomView.addSubview(ourProgramsView_view)
        }
    
    func addOurPartnersView()  {
        
        ourPartnersView_view =  ourPartnersView(frame: CGRect(x: 0, y: 0, width: bottomView.frame.width - 50, height: bottomView.frame.height))
    
    //        let RedisgnData:Blocks = self.dashboard_Blocks[1]
    //        let typeForBlocks = Int(RedisgnData.type!)
    //        let getHeight:CGFloat = 155
    //        let hideView =  38
    //        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
        ourPartnersView_view.setOurPartners(arrts: arr, setBlocks: self.dashboard_Blocks)
        bottomView.addSubview(ourPartnersView_view)
        }
}

//
//// MARK: - UITableViewDataSource
//extension WhyWater: UITableViewDataSource,UITableViewDelegate {
//
//
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView,
//                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell : imgTitleDescCell = tableView.dequeueReusableCell(withIdentifier: "imgTitleDescCell", for: indexPath) as! imgTitleDescCell
//
//        let myArr = arr[indexPath.row] as aboutUS
//
//        cell.configure(with: myArr.Title,desc: myArr.Desc,withImage: myArr.image,withFontSize: "0")
//        return cell
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
//
//
//
//     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        topBanner_view =  topBannerView(frame: CGRect(x: 0, y: 0, width: bannerWidgetView.frame.width - 50, height: bannerWidgetView.frame.height))
//
//
//        let RedisgnData:Blocks = self.dashboard_Blocks[1]
//        let typeForBlocks = Int(RedisgnData.type!)
//        let getHeight:CGFloat = 155
//        let hideView =  38
//
//        let myArr = arr[section]
//
//
//        topBanner_view.setWidgetDataCollection(dt: myArr, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
//
//        topBanner_view.btnTitle.tag = section
//        topBanner_view.innerView.removeShadowP()
//        topBanner_view.titlelblb.textAlignment = .left
//        topBanner_view.view.backgroundColor = .white
//        return topBanner_view
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//       return 80
//   }
//}
//
//
//
//
//extension WhyWater {
//
//    func Add3WidgetUIView()  {
//
//        addWidgetview =  CircularUIView(frame: CGRect(x: 0, y: 0, width: bannerWidgetView.frame.width - 50, height: bannerWidgetView.frame.height))
//
////        addWidgetview.bannerWidget = 1
////        let RedisgnData:Blocks = self.dashboard_Blocks[1]
////        let typeForBlocks = Int(RedisgnData.type!)
////        let getHeight:CGFloat = 155
////        let hideView =  38
////        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
//
//        bannerWidgetView.addSubview(addWidgetview)
//    }
//
//    func imgAndDescWidgetUIView()  {
//
//        imageAndDescWidgetview =  imageAndDescView(frame: CGRect(x: 0, y: 0, width: imgWidgetView.frame.width - 50, height: imgWidgetView.frame.height))
//
////        addWidgetview.bannerWidget = 1
////        let RedisgnData:Blocks = self.dashboard_Blocks[1]
////        let typeForBlocks = Int(RedisgnData.type!)
////        let getHeight:CGFloat = 155
////        let hideView =  38
////        addWidgetview.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 2 ,height:getHeight,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
//        imgWidgetView.addSubview(imageAndDescWidgetview)
//    }
//
//
//}
