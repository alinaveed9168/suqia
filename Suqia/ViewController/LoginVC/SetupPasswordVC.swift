//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

class SetupPasswordVC: BaseViewController {

    @IBOutlet weak var passwordContainerView: UIView!

    @IBOutlet weak var lbltitle: UILabel!

    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet var txtConfPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnBck: UIButton!
    @IBOutlet weak var btnInfoPassword: UIImageView!
    @IBOutlet weak var btnInfoConfPassword: UIImageView!
    @IBOutlet weak var lblminChar: UILabel!

    var isMobile = true

    @IBOutlet weak var headerHeight:NSLayoutConstraint!

    var searchValue = ""
    var emailValue = ""
    var authtoken = ""
    var loginModel = LoginUserBaseModel()
    var loginUserModel = LoginUserDataModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        if(!langSwift()){
            txtPassword.isSecureTextEntry = false
            txtConfPassword.isSecureTextEntry = false
        }

        if isSmallDevice(){
            headerHeight.constant = 325
        }

        let searchValueF = loginModel.loginUserDataModel?.searchValueType!

//        if searchValueF != ""{
//            if loginModel.loginUserDataModel?.searchValue?.isValidPhoneNo == true{
//                txtEmail.text = "+971 - "+(loginModel.loginUserDataModel?.searchValue)!
//                isMobile = true
//            }else{
//                txtEmail.text = loginModel.loginUserDataModel?.searchValue
//                isMobile = false
//            }
//        }else{
//            isMobile = false
//        }

//        if searchValueF == "e"{
//            txtEmail.text = loginModel.loginUserDataModel?.searchValue
//            isMobile = false
//        }else{
//            txtEmail.text = "+971 - "+(loginModel.loginUserDataModel?.searchValue)!
//            isMobile = true
//        }
//
//        if loginModel.loginUserDataModel?.allowToUpdateEmail == false{
//            txtEmail.isUserInteractionEnabled = false
//        }

        txtEmail.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft
        txtPassword.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft
        txtConfPassword.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft

        txtPassword.placeholder = UCPassword.localized()
        txtConfPassword.placeholder = UCConfirmPassword.localized()
        txtEmail.placeholder = UCEmail.localized()
//        btnBck.contentHorizontalAlignment = (langSwift()) ? .left : .right

        passwordContainerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        btnSubmit.setTitle(UCButton_Title.localized(), for: .normal)
        self.lbltitle.text  =  UCForgot_Username_Password.localized()
        setFontFamily(self.view, andSubviews: true)
        
//        let bckimage = (langSwift()) ? "back_white" : "back_ar_white"
//        btnBck.setImage(UIImage(named: bckimage), for: .normal)
        
//        btnInfoPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoPassword))
//        btnInfoConfPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoConfPassword))

        txtPassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtConfPassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
//        lblminChar.text = "min_char".localized()

    }

    @objc func textFieldDidChange(_ sender: Any?) {
        let textField = sender as? UITextField
        if textField == txtPassword || textField == txtConfPassword{
            if (textField?.text?.count ?? 0) == 0 {
                setFontFamily(textField!, andSubviews: true)
            } else {
                textField!.font = nil
            }
        }
    }

    @objc func InfoPassword() {
        if(txtPassword.isSecureTextEntry){
            txtPassword.isSecureTextEntry = false
            btnInfoPassword.image = UIImage(named: "hidePassword")//showPassword
        }else{
            txtPassword.isSecureTextEntry = true
            btnInfoPassword.image = UIImage(named: "showPassword")//hidePassword
        }
    }
    @objc func InfoConfPassword() {
        if(txtConfPassword.isSecureTextEntry){
            txtConfPassword.isSecureTextEntry = false
            btnInfoConfPassword.image = UIImage(named: "hidePassword")//showPassword

        }else{
            txtConfPassword.isSecureTextEntry = true
            btnInfoConfPassword.image = UIImage(named: "showPassword")//hidePassword

        }
    }

   

    @IBAction func btnSubmitCliccked() {
        if validation() {
            callWebServiceToValidateUser()
        }
    }

    func callWebServiceToValidateUser()  {

//        guard let email =  txtEmail.text else{
//            return
//        }
//        guard let password =  txtPassword.text else{
//            return
//        }
//        let requestModel = LoginRequestModel(searchValue: email,
//                                             password: password,
//                                             email: email,
//                                             authToken:authtoken)
//
//        NetworkManager.shared.singleSignOnMethod(mainURL: Config.createSetupAccount, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
//            GCDMainThread.async {
//
//            if response == nil {
//                self.showAlertView(msg: UCService_Error.localized(),theme:.error)
//                return
//            }
//            else {
//                var userModel = LoginUserBaseModel()
//                let responseData = response as! NSDictionary
//                userModel = LoginUserBaseModel(JSON: responseData as! [String : Any])!
//
//                if userModel.result == 0 {
//                    self.showAlertView(msg: userModel.msg!,theme:.error)
//                    return
//                }
//                if userModel.result == 99 {
//                    self.showAlertView(msg: userModel.msg!,theme:.error)
//                    self.navigationController?.popViewController(animated: true)
//                    return
//                }
//                if userModel.result == 1 {
//                    self.navigateToOTPScreen(loginModel: userModel, requestModel: requestModel)
//                }
//            }
//        }
//    }
}

//    func navigateToOTPScreen(loginModel:LoginUserBaseModel,requestModel:LoginRequestModel){

//        let otpObj = OTPVC.instantiate(fromAppStoryboard: .userStoryboard)
//        otpObj.delegate = self
//        otpObj.requestModel = requestModel
//        otpObj.userModel = loginModel
//        otpObj.searchValue = searchValue
//        otpObj.requestURL = Config.createSetupAccount
//        present(otpObj, animated: true, completion: nil)
//}

//    func otpVerifiedSuccessfully(_ controller: OTPVC, registrationModel: LoginUserBaseModel, type: Int) {
//        if type == 1 {
//
//            loginUserModel = registrationModel.loginUserDataModel!
//            let cartShipping = loginUserModel.cart_shipping
//
//            if cartShipping?.delivery_address?.address_id == nil || cartShipping?.delivery_address?.address_id == ""{
//                let areaCity = PickUpAreanCity.instantiate(fromAppStoryboard: .generalStoryboard)
//                areaCity.delegate = self
//                areaCity.fromLoginScreen = "UserPassword"
//                areaCity.correctDeliveryType = HOME_DELIVERY
//                self.present(areaCity, animated: true, completion: nil)
//            }else{
//                setDefaultAddress(cartShipping: loginUserModel.cart_shipping!)
//                self.openDashboardScreen()
//            }
//        }
//}

    func openDashboardScreen(){
//        setCustomerData(customerModal: loginUserModel)
//        GlobalObjects.setRefreshHomeScreen(true)
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//        self.navigationController?.pushViewController(ucTabarVC, animated: false)
    }

    func didSaveAddress(){
        openDashboardScreen()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    func validation() -> Bool{

        if isMobile {
            if txtEmail.text?.isEmpty ?? false {
                self.showAlertView(msg: UCEnterValidMobile.localized(),theme:.error)
                return false
            }
        }else{
            if txtEmail.text?.isEmpty ?? false {
                self.showAlertView(msg: UCEnterEmailAddress.localized(),theme:.error)
                return false
            }
            if txtEmail.text?.isValidEmail == false{
                self.showAlertView(msg:UCEnterValidEmail.localized(),theme: .error)
                return false
            }
        }

        if txtPassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterPassword.localized(),theme: .error)
            return false
        }
        if txtConfPassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterConfirmPassword.localized(),theme: .error)
            return false
        }
        if txtPassword.text != txtConfPassword.text {
            self.showAlertView(msg:UCPasswordNotMatch.localized(),theme: .error)
            return false
        }
        return true
    }

}


//
//import UIKit
//
//class SetupPasswordVC: BaseViewController {
//
//
//    @IBOutlet weak var loginContainerView: UIView!
//    @IBOutlet weak var lbltitle: UILabel!
//
//    @IBOutlet var txtEmail: SkyFloatingLabelTextField!
//    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
//    @IBOutlet var txtConfPassword: SkyFloatingLabelTextField!
//
//    @IBOutlet weak var btnSubmit: UIButton!
//    var userDataModel = LoginUserBaseModel()
//    var searchValue = ""
//    private var observer: Any!
//    var loginModelData = LoginUserDataModel()
//
//
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.navigationController?.navigationBar.isHidden = true
//
//        txtPassword.placeholder = UCPassword.localized()
//        txtEmail.placeholder = UCEmail.localized()
//
//
//
//        // Colored Underline Label
//               let labelString = UCForgotButton.localized()
//               let textColor: UIColor = ucsBlueColor
//               let underLineColor: UIColor = ucsBlueColor
//               let underLineStyle = NSUnderlineStyle.single.rawValue
//
//        let labelAtributes:[NSAttributedString.Key : Any]  = [
//            NSAttributedString.Key.foregroundColor: textColor,
//            NSAttributedString.Key.underlineStyle: underLineStyle,
//            NSAttributedString.Key.underlineColor: underLineColor
//               ]
//
//               let underlineAttributedString = NSAttributedString(string: labelString,
//                                                                  attributes: labelAtributes)
//        btnSubmit.setTitle(UCSh_login.localized(), for: .normal)
//
//        loginContainerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
//        setFontFamily(self.view, andSubviews: true)
//
//        loginContainerView.semanticContentAttribute = .forceLeftToRight
//        self.lbltitle.text  =  UCLogin_Register.localized()
//
//        txtPassword.font = nil
//        txtPassword.font = UIFont.systemFont(ofSize: 15)
//
//        txtPassword.resignFirstResponder()
//        txtPassword.isSecureTextEntry = true
//        txtPassword.becomeFirstResponder()
//
//
//        txtEmail.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft
//        txtPassword.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft
//        txtConfPassword.semanticContentAttribute = (langSwift()) ? .forceLeftToRight : .forceRightToLeft
//
//        txtPassword.placeholder = UCPassword.localized()
//        txtConfPassword.placeholder = UCConfirmPassword.localized()
//        txtEmail.placeholder = UCEmail.localized()
//
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//
//    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(true)
////        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
//    }
//
//        @objc func textFieldDidChange(_ sender: Any?) {
//            let textField = sender as? UITextField
//            if textField == txtPassword || textField == txtConfPassword{
//                if (textField?.text?.count ?? 0) == 0 {
//                    setFontFamily(textField!, andSubviews: true)
//                } else {
//                    textField!.font = nil
//                }
//            }
//        }
//
//
//    @IBAction func btnSkipClicked() {
//
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.makeRootViewController()
//    }
//    //        let forgotPWObj = ForgotPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
//    //        self.navigationController?.pushViewController(forgotPWObj, animated: true)
//
//    @IBAction func btnForgotClicked() {
//
//        let loginObj = tabbartUIstoryboard.instantiateViewController(withIdentifier: "UserLoginVC") as! UserLoginVC
//        self.navigationController?.pushViewController(loginObj, animated: false)
//    }
//    @IBAction func btnLoginClicked() {
//
////        if validation() {
////            callWebServiceToValidateUser()
////        }
//    }
//
////    func callWebServiceToValidateUser()  {
////
////        guard let mobile =  txtMobile.text else{
////            return
////        }
////        guard let email =  txtEmail.text else{
////            return
////        }
////
////        if isMobile {
////            searchValue = mobile
////        }else{
////            searchValue = email
////        }
////
////        let requestModel = LoginRequestModel(searchValue: searchValue)
////
////        NetworkManager.shared.singleSignOnMethod(mainURL: Config.checkAccountSetup, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
////
////            GCDMainThread.async {
////                if response == nil {
////                    self.showAlertView(msg: UCService_Error.localized(),theme:.error)
////                    return
////                }
////                else {
////                    let responseData = response as! NSDictionary
////
////                    self.userDataModel = LoginUserBaseModel(JSON: responseData as! [String : Any])!
////                    if self.userDataModel.result == 1 {
////                        self.navigateToPasswordScreen()
////                    }else if self.userDataModel.result == 2 {
////                        self.navigateToAccountSetupScreen()
////                    }else{
////                        self.showAlertView(msg: self.userDataModel.msg!,theme:.error)
////                    }
////                }
////            }
////        }
////    }
//
//    func navigateToPasswordScreen() {
////        let userPassword = UserPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
////        userPassword.searchValue = searchValue
////        self.navigationController?.pushViewController(userPassword, animated: true)
//    }
//
////    func navigateToAccountSetupScreen() {
////        let userPassword = SetupPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
////        userPassword.emailValue = txtEmail.text!
////        userPassword.searchValue = searchValue
////        userPassword.loginModel = self.userDataModel
////        userPassword.authtoken = (self.userDataModel.loginUserDataModel?.authToken)!
////        self.navigationController?.pushViewController(userPassword, animated: true)
////    }
//
//    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        return true
//    }
//
//
//
//    func validation() -> Bool{
//            if txtEmail.text?.isEmpty ?? false {
//                self.showAlertView(msg:UCEnterEmailAddress.localized(),theme:.error)
//                return false
//            }
//            if txtEmail.text?.isValidEmail == false{
//                self.showAlertView(msg:UCEnterValidEmail.localized(),theme: .error)
//                return false
//            }
//        if txtPassword.text?.isEmpty ?? false {
//            self.showAlertView(msg:UCEnterPassword.localized(),theme: .error)
//            return false
//        }
//
//        return true
//    }
//
//
//
//    func didSaveAddress(){
//        self.moveToTabBar()
//    }
//
//    func moveToTabBar()  {
////        self.navigationController?.pushViewController(ucTabarVC, animated: false)
//    }
//
//
//
//
//}
//
//
//// MARK: - Alerts
//extension UserLoginVC {
//
//
//    // MARK: - Actions
//
//    @IBAction func loginButtonClicked(_ sender: Any) {
//        view.endEditing(true)
//
////            showLoginSucessAlert()
//    }
//
//
//    func openDashboardScreen(){
////        setCustomerData(customerModal: loginModelData)
////        GlobalObjects.setRefreshHomeScreen(true)
////        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
////        self.navigationController?.pushViewController(ucTabarVC, animated: false)
//    }
//
//    func showErrorAlert(message: String) {
//        self.showAlertView(msg: message,theme:.error)
//    }
//
//
//}
