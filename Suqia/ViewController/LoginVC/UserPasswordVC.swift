//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

class UserPasswordVC: UIViewController, PickUpAreanCityDelegate {
    
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet var txtPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var toggleBtn: UIButton!
    @IBOutlet weak var btnBck: UIButton!
    @IBOutlet weak var btnForgotPW: UIButton!

    @IBOutlet weak var btnPassword: UIImageView!

    @IBOutlet weak var headerHeight:NSLayoutConstraint!
    var loginModelData = LoginUserDataModel()
    
    var searchValue = ""
    @IBOutlet weak var lblminChar: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtPassword.isSecureTextEntry = true
        
        if(UIDevice.current.checkDevice() && !langSwift()){
            txtPassword.isSecureTextEntry = false
        }
        
        if isSmallDevice(){
            headerHeight.constant = 325
        }
        lbltitle.text  =  UCEnterPassword.localized()
        setFontFamily(self.view, andSubviews: true)
        passwordContainerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        btnSubmit.setTitle(UCButton_Title.localized(), for: .normal)
        let bckimage = (langSwift()) ? "back_white" : "back_ar_white"
        btnBck.setImage(UIImage(named: bckimage), for: .normal)
        btnBck.contentHorizontalAlignment = (langSwift()) ? .left : .right
        btnForgotPW.setTitle(UCForgotButton.localized(), for: .normal)
        txtPassword.placeholder = UCEnterPassword.localized()
        btnPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoPassword))
        txtPassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        lblminChar.text = "min_char".localized()
    }
   
    @objc func textFieldDidChange(_ sender: Any?) {
        let textField = sender as? UITextField
        if textField == txtPassword {
            if (textField?.text?.count ?? 0) == 0 {
                setFontFamily(txtPassword, andSubviews: true)
            } else {
                txtPassword.font = nil
            }
        }
    }
    
    @objc func InfoPassword() {
        
            if(txtPassword.isSecureTextEntry){
                txtPassword.isSecureTextEntry = false
                btnPassword.image = UIImage(named: "hidePassword")//showPassword
            }else{
                txtPassword.isSecureTextEntry = true
                btnPassword.image = UIImage(named: "showPassword")//hidePassword
            }
        
    }
    
    @IBAction func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitCliccked() {
        if validation() {
            callWebServiceTologin()
        }
    }
    
    func callWebServiceTologin()  {
        
        let setDeviceToken = LoginOperationObj()
        let tokenObjC =   (setDeviceToken.getDeviceToken() == nil) ? "" :  setDeviceToken.getDeviceToken() 
        
        guard let passowrd =  txtPassword.text else{ return }
        let requestModel = LoginRequestModel(searchValue: searchValue,
                                             password: passowrd,
                                             deviceToken:tokenObjC)
        
        NetworkManager.shared.singleSignOnMethod(mainURL: Config.loginAccount, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
            
            GCDMainThread.async {
                
                if response == nil {
                    self.showAlertView(msg: UCService_Error.localized(),theme:.error)
                    return
                }
                else {
                    let responseData = response as! NSDictionary
                   
                    let jsonData = try! JSONSerialization.data(withJSONObject: responseData, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                    globalPrint(dataToPrint:jsonString)
                    
                    var loginBase = LoginUserBaseModel()
                    loginBase = LoginUserBaseModel(JSON: responseData as! [String : Any])!
                    if loginBase.result == 1 {

                        self.loginModelData = loginBase.loginUserDataModel!
                        let cartShipping = self.loginModelData.cart_shipping

                        if cartShipping?.delivery_address?.address_id == nil || cartShipping?.delivery_address?.address_id == ""{
                            let areaCity = PickUpAreanCity.instantiate(fromAppStoryboard: .generalStoryboard)
                            areaCity.delegate = self
                            areaCity.fromLoginScreen = "UserPassword"
                            self.present(areaCity, animated: true, completion: nil)
                        }else{
                            setDefaultAddress(cartShipping: self.loginModelData.cart_shipping!)
                            self.openDashboardScreen()
                        }
                        return
                    }else{
                        self.showAlertView(msg: loginBase.msg!,theme:.error)
                    }
                }
            }
        }
    }
    
    func openDashboardScreen(){
        setCustomerData(customerModal: loginModelData)
        GlobalObjects.setRefreshHomeScreen(true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.pushViewController(ucTabarVC, animated: false)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

    
    func didSaveAddress(){
        openDashboardScreen()
    }
    
    func validation() -> Bool{
        
        if txtPassword.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterValidPassword.localized(),theme:.error)
            return false
        }
        return true
    }
    
    @IBAction func btnForgotClicked() {
        let forgotPWObj = ForgotPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
        self.navigationController?.pushViewController(forgotPWObj, animated: true)
    }
}

