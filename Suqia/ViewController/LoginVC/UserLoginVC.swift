//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

class UserLoginVC: BaseViewController {
    
    @IBOutlet weak var MainContanierView: UIView!
    @IBOutlet weak var loginContainerView: UIView!
    
    @IBOutlet var txtEmail_login: UITextField!
    @IBOutlet var txtPassword_login: UITextField!

    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var txtIMG_Email_login: UIImageView!
    @IBOutlet weak var txtIMG_Password_login: UIImageView!
    
    
    
    var userDataModel = LoginUserBaseModel()
    var searchValue = ""
    private var observer: Any!
    var loginModelData = LoginUserDataModel()

    @IBOutlet var heightView: NSLayoutConstraint!
    var selectionBtn = false

    @IBOutlet weak var btnSelctionRegister: UIButton!
    @IBOutlet weak var btnSelctionLogin: UIButton!
    
  //==Singup
    @IBOutlet weak var signup_containerView: UIView!
    @IBOutlet var signup_txtFname: UITextField!
    @IBOutlet var signup_txtLname: UITextField!
    @IBOutlet var signup_txtMobile: UITextField!
    @IBOutlet var signup_txtemail: UITextField!
    @IBOutlet var signup_txtpassword: SkyFloatingLabelTextField!
    @IBOutlet var signup_txtConfpassword: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var btnBck: UIButton!
    @IBOutlet weak var btnInfoPassword: UIImageView!
    @IBOutlet weak var btnInfoConfPassword: UIImageView!
    
    var cardType = ""
    var authToken_Registration = ""
    var countryCode = ""
    
    var loginUserModel = LoginUserDataModel()
    @IBOutlet weak var btnRegister: UIButton!

    
    @IBOutlet weak var txtIMG_signup_txtFname: UIImageView!
    @IBOutlet weak var txtIMG_signup_txtLname: UIImageView!
    @IBOutlet weak var txtIMG_signup_txtEmail: UIImageView!
    @IBOutlet weak var txtIMG_signup_txtMobile: UIImageView!
    
    @IBOutlet weak var txtIMG_signup_txtpassword: UIImageView!
    @IBOutlet weak var txtIMG_signup_txtConfpassword: UIImageView!
    @IBOutlet weak var txtIMG_NotRobot: UIImageView!


    
//close
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        txtEmail_login.placeholder =  UCPhoneEmailUsername.localized()
        txtPassword_login.placeholder = UCPassword.localized()
        
        selectionBtn = false
        loginContainerView.isHidden = false
        signup_containerView.isHidden = true
        
//        let strins = UCCA.localized()
//        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: UCCreateAccount.localized())
//        let startRange = attributeString.length - strins.count
//        let endRange = attributeString.length - startRange
//        let captionAttribute = [
//                NSAttributedString.Key.foregroundColor: ucsBlueColor
//            ]
//        
//        attributeString.addAttributes(captionAttribute, range: NSMakeRange(startRange, endRange))
//        attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1,
//                                     range: NSMakeRange(startRange, endRange))
//        lblCreateAccount.attributedText = attributeString
//        
     
        
        // Colored Underline Label
               let labelString = UCForgotButton.localized()
               let textColor: UIColor = suqiaBlue
               let underLineColor: UIColor = suqiaBlue
               let underLineStyle = NSUnderlineStyle.single.rawValue

        let labelAtributes:[NSAttributedString.Key : Any]  = [
            NSAttributedString.Key.foregroundColor: textColor,
            NSAttributedString.Key.underlineStyle: underLineStyle,
            NSAttributedString.Key.underlineColor: underLineColor
               ]

               let underlineAttributedString = NSAttributedString(string: labelString,
                                                                  attributes: labelAtributes)
        btnReset.setAttributedTitle(underlineAttributedString, for: .normal)
        btnLogin.setTitle(UCSh_login.localized(), for: .normal)
        btnSkip.setTitle(UCSkip.localized(), for: .normal)
//        loginContainerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
//        setFontFamily(self.view, andSubviews: true)
        
        loginContainerView.semanticContentAttribute = .forceLeftToRight
        
        txtPassword_login.font = nil
        txtPassword_login.font = UIFont.systemFont(ofSize: 15)
        
        
        txtPassword_login.isSecureTextEntry = true
        
        txtIMG_Email_login.layer.borderWidth = 1
        txtIMG_Password_login.layer.borderWidth = 1

        txtIMG_Email_login.layer.borderColor = suqiaBlue.cgColor
        txtIMG_Password_login.layer.borderColor = suqiaBlue.cgColor

        txtIMG_signup_txtFname.layer.borderColor = suqiaBlue.cgColor
        txtIMG_signup_txtLname.layer.borderColor = suqiaBlue.cgColor

        txtIMG_signup_txtFname.layer.borderWidth = 1
        txtIMG_signup_txtLname.layer.borderWidth = 1
        
        
        txtIMG_signup_txtEmail.layer.borderColor = suqiaBlue.cgColor
        txtIMG_signup_txtEmail.layer.borderWidth = 1
        
        txtIMG_signup_txtMobile.layer.borderColor = suqiaBlue.cgColor
        txtIMG_signup_txtMobile.layer.borderWidth = 1

        txtIMG_signup_txtpassword.layer.borderColor = suqiaBlue.cgColor
        txtIMG_signup_txtpassword.layer.borderWidth = 1

        txtIMG_signup_txtConfpassword.layer.borderColor = suqiaBlue.cgColor
        txtIMG_signup_txtConfpassword.layer.borderWidth = 1

        txtIMG_NotRobot.layer.borderColor = suqiaBlue.cgColor
        txtIMG_NotRobot.layer.borderWidth = 1

        
        
        
        self.signup_viewDidLoad()
        //Signup
        MainContanierView.shadowP(2, height: 3)
        
//        MainContanierView.dropShadow2(color: .gray,
//                                      opacity: 1,
//                                      offSet: CGSize(width: 2, height: 2),
//                                      radius: 3, scale: true)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func btnSelection(sender:UIButton) {
        let color2 = suqiaBlue
        
        
        if(!selectionBtn && sender.tag == 1){

            UIView.transition(with: signup_containerView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                                self.loginContainerView.isHidden = true
                                self.signup_containerView.isHidden = false
                          })
            selectionBtn = true
            btnSelctionRegister.backgroundColor = color2
            btnSelctionRegister.setTitleColor(.white, for: .normal)
            btnSelctionLogin.backgroundColor = .white
            btnSelctionLogin.setTitleColor(color2, for: .normal)
            
            
            self.heightView.constant =  412 + 30

            UIView.animate(withDuration: 1.0, animations: {
                     self.MainContanierView.layoutIfNeeded()
                    
                })
                
        } else if(selectionBtn && sender.tag == 0){
            UIView.transition(with: loginContainerView, duration: 0.4,
                              options: .transitionCrossDissolve,
                              animations: {
                             
                                self.loginContainerView.isHidden = false
                                self.signup_containerView.isHidden = true
                          })
            
            selectionBtn = false
            
           
            btnSelctionRegister.backgroundColor = .white
            btnSelctionRegister.setTitleColor(color2, for: .normal)
            btnSelctionLogin.backgroundColor = color2
            btnSelctionLogin.setTitleColor(.white, for: .normal)
           
            
            self.heightView.constant = 315
            UIView.animate(withDuration: 1.0) {
                    self.MainContanierView.layoutIfNeeded()
                
                
              
                }
            
        }
        
        self.MainContanierView.removeShadowP()
        self.MainContanierView.shadowP(2, height: 3)

        
    }
    @IBAction func btnRegisterClicked() {
        print("btnRegisterClicked")
    }
    @IBAction func btnSkipClicked() {
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.makeRootViewController()
    }
    //        let forgotPWObj = ForgotPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
    //        self.navigationController?.pushViewController(forgotPWObj, animated: true)

    @IBAction func btnForgotClicked() {
        
        let loginObj = tabbartUIstoryboard.instantiateViewController(withIdentifier: "SetupPasswordVC") as! SetupPasswordVC
        self.navigationController?.pushViewController(loginObj, animated: false)
    }
    
    @IBAction func btnSignupClicked() {
        
        let loginObj = tabbartUIstoryboard.instantiateViewController(withIdentifier: "RegistrationVC") as! RegistrationVC
        self.navigationController?.pushViewController(loginObj, animated: false)
    }
    
    @IBAction func btnLoginClicked() {
        
//        if validation() {
//            callWebServiceToValidateUser()
//        }
    }
    
//    func callWebServiceToValidateUser()  {
//
//        guard let mobile =  txtMobile.text else{
//            return
//        }
//        guard let email =  txtEmail.text else{
//            return
//        }
//
//        if isMobile {
//            searchValue = mobile
//        }else{
//            searchValue = email
//        }
//
//        let requestModel = LoginRequestModel(searchValue: searchValue)
//
//        NetworkManager.shared.singleSignOnMethod(mainURL: Config.checkAccountSetup, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
//
//            GCDMainThread.async {
//                if response == nil {
//                    self.showAlertView(msg: UCService_Error.localized(),theme:.error)
//                    return
//                }
//                else {
//                    let responseData = response as! NSDictionary
//
//                    self.userDataModel = LoginUserBaseModel(JSON: responseData as! [String : Any])!
//                    if self.userDataModel.result == 1 {
//                        self.navigateToPasswordScreen()
//                    }else if self.userDataModel.result == 2 {
//                        self.navigateToAccountSetupScreen()
//                    }else{
//                        self.showAlertView(msg: self.userDataModel.msg!,theme:.error)
//                    }
//                }
//            }
//        }
//    }
    
    func navigateToPasswordScreen() {
//        let userPassword = UserPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
//        userPassword.searchValue = searchValue
//        self.navigationController?.pushViewController(userPassword, animated: true)
    }
    
//    func navigateToAccountSetupScreen() {
//        let userPassword = SetupPasswordVC.instantiate(fromAppStoryboard: .userStoryboard)
//        userPassword.emailValue = txtEmail.text!
//        userPassword.searchValue = searchValue
//        userPassword.loginModel = self.userDataModel
//        userPassword.authtoken = (self.userDataModel.loginUserDataModel?.authToken)!
//        self.navigationController?.pushViewController(userPassword, animated: true)
//    }
    
  
    
   
    
    func validation() -> Bool{
            if txtEmail_login.text?.isEmpty ?? false {
                self.showAlertView(msg:UCEnterEmailAddress.localized(),theme:.error)
                return false
            }
            if txtEmail_login.text?.isValidEmail == false{
                self.showAlertView(msg:UCEnterValidEmail.localized(),theme: .error)
                return false
            }
        if txtPassword_login.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterPassword.localized(),theme: .error)
            return false
        }
        
        return true
    }
    
   
    
    func didSaveAddress(){
        self.moveToTabBar()
    }
    
    func moveToTabBar()  {
//        self.navigationController?.pushViewController(ucTabarVC, animated: false)
    }
    
    
   
  
}


// MARK: - Alerts
extension UserLoginVC {
    
    
    // MARK: - Actions
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        view.endEditing(true)
        
//            showLoginSucessAlert()
    }
    
    func showErrorAlert(message: String) {
        self.showAlertView(msg: message,theme:.error)
    }
    
    
}

//Singup
extension UserLoginVC {
    
  
    func signup_viewDidLoad() {
        
        signup_txtMobile.placeholder = UCMobile.localized()
//        signup_containerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        btnRegister.setTitle(UCRegister.localized(), for: .normal)

        
        
        
        if cardType == GoldCard {
            signup_txtFname.text = loginUserModel.fName
            signup_txtLname.text = loginUserModel.lName
            signup_txtemail.text = loginUserModel.email
            if loginUserModel.fName != nil {
                signup_txtFname.isUserInteractionEnabled = false
            }
            if loginUserModel.lName != nil {
                signup_txtLname.isUserInteractionEnabled = false
            }
        }
        //setFontFamily(self.view, andSubviews: true)
        let bckimage = (langSwift()) ? "new_back" : "new_back_ar"
        btnBck.setImage(UIImage(named: bckimage), for: .normal)
        
        signup_txtFname.placeholder = UCFName.localized()
        signup_txtLname.placeholder = UCLName.localized()
        signup_txtpassword.placeholder = UCPassword.localized()
        signup_txtConfpassword.placeholder = UCConfirmPassword.localized()
        signup_txtemail.placeholder = UCLogin_Email.localized()
        signup_txtMobile.textAlignment = .left
        btnInfoPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoPassword))
        btnInfoConfPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoConfPassword))

        signup_txtpassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        signup_txtConfpassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        if(!langSwift()){
            signup_txtpassword.isSecureTextEntry = false
            signup_txtConfpassword.isSecureTextEntry = false
        }
    }
    
    @objc func textFieldDidChange(_ sender: Any?) {
        let textField = sender as? UITextField
        if textField == signup_txtpassword || textField == signup_txtConfpassword{
            if (textField?.text?.count ?? 0) == 0 {
                setFontFamily(textField!, andSubviews: true)
            } else {
                textField!.font = nil
            }
        }
    }
    
    @objc func InfoPassword() {
        if(signup_txtpassword.isSecureTextEntry){
            signup_txtpassword.isSecureTextEntry = false
            btnInfoPassword.image = UIImage(named: "hidePassword")//showPassword
        }else{
            signup_txtpassword.isSecureTextEntry = true
            btnInfoPassword.image = UIImage(named: "showPassword")//hidePassword
        }
    }
    @objc func InfoConfPassword() {
        if(signup_txtConfpassword.isSecureTextEntry){
            signup_txtConfpassword.isSecureTextEntry = false
            btnInfoConfPassword.image = UIImage(named: "hidePassword")//showPassword

        }else{
            signup_txtConfpassword.isSecureTextEntry = true
            btnInfoConfPassword.image = UIImage(named: "showPassword")//hidePassword
        }
    }
    
    @IBAction func btnSubmitClicked() {
        
        if validation() {
            callWebServiceToVerify()
        }
    }
    
    func callWebServiceToVerify()  {
        
//        guard let fName =  txtFname.text else{ return }
//        guard let lName =  txtLname.text else{ return }
//        guard let email =  txtemail.text else{ return }
//        guard let mobile =  txtMobile.text else{ return }
//        guard let password =  txtpassword.text else{ return }
//        guard let dob =  txtDOB.text else{ return }
//        let genderText = (txtgender.text == UCMale.localized()) ? "M" :  "F"
//        let gender =  genderText
//        guard let taxNumber =  txttaxRegNo.text else{ return }
//
//        let requestModel = RegisterUserModel(fName: fName,
//                                             lName: lName,
//                                             mobileNo: mobile,
//                                             email: email,
//                                             password: password,
//                                             dob: dob,
//                                             nationality: countryCode,
//                                             gender: gender,
//                                             taxRegNo: taxNumber,
//                                             deviceToken: "242ascsac",
//                                             authToken:authToken_Registration)
//
//        NetworkManager.shared.singleSignOnMethod(mainURL: Config.register_user, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
//
//            if response == nil {
//                self.showAlertView(msg: UCService_Error.localized(),theme:.error)
//                return
//            }
//            else {
//
//                let responseData = response as! NSDictionary
//                var loginBase = LoginUserBaseModel()
//                loginBase = LoginUserBaseModel(JSON: responseData as! [String : Any])!
//                if loginBase.result == 1 {
//                    //Please check address list
//                    self.loginUserModel = loginBase.loginUserDataModel!
//                    let cartShipping = self.loginUserModel.cart_shipping
//                    if cartShipping?.delivery_address?.address_id != nil || cartShipping?.delivery_address?.address_id != ""{
//                        self.cartShipped = self.loginUserModel.cart_shipping!
//                    }
//                    setCustomerData(customerModal: self.loginUserModel)
//
//                    let selectCardObj = RegisterConfirmationVC.instantiate(fromAppStoryboard: .userStoryboard)
//                    selectCardObj.delegate = self
//                    selectCardObj.modalPresentationStyle = .overCurrentContext
//                    self.definesPresentationContext = true
//                    selectCardObj.modalTransitionStyle = .crossDissolve
//                    self.present(selectCardObj, animated: true, completion: nil)
//
//                    return
//                }else{
//                    self.showAlertView(msg: loginBase.msg!,theme:.error)
//                }
//            }
//        }
    }
    
    func openDashboardScreen(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.makeRootViewController()
//        setCustomerData(customerModal: loginUserModel)
//        GlobalObjects.setRefreshHomeScreen(true)
//        self.navigationController?.pushViewController(ucTabarVC, animated: false)
    }

    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField  == signup_txtMobile {
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 9
        }
        return true
    }
    
    func signup_validation() -> Bool{
        
        
        if signup_txtFname.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterFirstName.localized(),theme:.error)
            return false
        }
        if signup_txtLname.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterLastName.localized(),theme:.error)
            return false
        }
        if signup_txtemail.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterEmailAddress.localized(),theme:.error)
            return false
        }
        if signup_txtemail.text?.isValidEmail == false{
            self.showAlertView(msg:UCEnterValidEmail.localized(),theme: .error)
            return false
        }
        if signup_txtMobile.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterValidMobile.localized(),theme:.error)
            return false
        }
        if signup_txtpassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterPassword.localized(),theme: .error)
            return false
        }
        if signup_txtConfpassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterConfirmPassword.localized(),theme: .error)
            return false
        }
        if signup_txtpassword.text != signup_txtConfpassword.text {
            self.showAlertView(msg:UCPasswordNotMatch.localized(),theme: .error)
            return false
        }
        
        return true
    }
  
}


extension UIView {

    func shadowP(_ width: Int = 5,height: Int = 5) {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: width , height: height)
    }

    func removeShadowP() {
        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        self.layer.shadowColor = UIColor.clear.cgColor
        self.layer.cornerRadius = 0.0
        self.layer.shadowRadius = 0.0
        self.layer.shadowOpacity = 0.0
    }
}
