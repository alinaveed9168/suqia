//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

class RegistrationVC: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet var txtFname: SkyFloatingLabelTextField!
    @IBOutlet var txtLname: SkyFloatingLabelTextField!
    @IBOutlet var txtMobile: SkyFloatingLabelTextField!
    @IBOutlet var txtemail: SkyFloatingLabelTextField!
    @IBOutlet var txtpassword: SkyFloatingLabelTextField!
    @IBOutlet var txtConfpassword: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnBck: UIButton!
    @IBOutlet weak var btnInfoPassword: UIImageView!
    @IBOutlet weak var btnInfoConfPassword: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var cardType = ""
    var searchValue = ""
    var authToken_Registration = ""
    var countryCode = ""
    
    var loginUserModel = LoginUserDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbltitle.text  =  UCCreate_Account.localized()
        
        txtMobile.placeholder = UCMobile.localized()
        containerView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
        btnSubmit.setTitle(UCButton_Title.localized(), for: .normal)
//        btnBck.contentHorizontalAlignment = (langSwift()) ? .left : .right

        
        txtMobile.text = searchValue
        txtMobile.isUserInteractionEnabled = false
        
        if cardType == GoldCard {
            txtFname.text = loginUserModel.fName
            txtLname.text = loginUserModel.lName
            txtemail.text = loginUserModel.email
            if loginUserModel.fName != nil {
                txtFname.isUserInteractionEnabled = false
            }
            if loginUserModel.lName != nil {
                txtLname.isUserInteractionEnabled = false
            }
        }
        setFontFamily(self.view, andSubviews: true)
        let bckimage = (langSwift()) ? "back_white" : "back_ar_white"
     //   btnBck.setImage(UIImage(named: bckimage), for: .normal)
        
        txtFname.placeholder = UCFName.localized()
        txtLname.placeholder = UCLName.localized()
        txtpassword.placeholder = UCPassword.localized()
        txtConfpassword.placeholder = UCConfirmPassword.localized()
        txtemail.placeholder = UCLogin_Email.localized()
        txtMobile.textAlignment = .left
        btnInfoPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoPassword))
        btnInfoConfPassword.addTapGesture(tapNumber: 1, target: self, action:  #selector(InfoConfPassword))

        txtpassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        txtConfpassword.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
        
        if(!langSwift()){
            txtpassword.isSecureTextEntry = false
            txtConfpassword.isSecureTextEntry = false
        }
    }
    
    @objc func textFieldDidChange(_ sender: Any?) {
        let textField = sender as? UITextField
        if textField == txtpassword || textField == txtConfpassword{
            if (textField?.text?.count ?? 0) == 0 {
                setFontFamily(textField!, andSubviews: true)
            } else {
                textField!.font = nil
            }
        }
    }
    
    @objc func InfoPassword() {
        if(txtpassword.isSecureTextEntry){
            txtpassword.isSecureTextEntry = false
            btnInfoPassword.image = UIImage(named: "hidePassword")//showPassword
        }else{
            txtpassword.isSecureTextEntry = true
            btnInfoPassword.image = UIImage(named: "showPassword")//hidePassword
        }
    }
    @objc func InfoConfPassword() {
        if(txtConfpassword.isSecureTextEntry){
            txtConfpassword.isSecureTextEntry = false
            btnInfoConfPassword.image = UIImage(named: "hidePassword")//showPassword

        }else{
            txtConfpassword.isSecureTextEntry = true
            btnInfoConfPassword.image = UIImage(named: "showPassword")//hidePassword
        }
    }
    
    @IBAction func btnSubmitCliccked() {
        
        if validation() {
            callWebServiceToVerify()
        }
    }
    
    func callWebServiceToVerify()  {
        
//        guard let fName =  txtFname.text else{ return }
//        guard let lName =  txtLname.text else{ return }
//        guard let email =  txtemail.text else{ return }
//        guard let mobile =  txtMobile.text else{ return }
//        guard let password =  txtpassword.text else{ return }
//        guard let dob =  txtDOB.text else{ return }
//        let genderText = (txtgender.text == UCMale.localized()) ? "M" :  "F"
//        let gender =  genderText
//        guard let taxNumber =  txttaxRegNo.text else{ return }
//
//        let requestModel = RegisterUserModel(fName: fName,
//                                             lName: lName,
//                                             mobileNo: mobile,
//                                             email: email,
//                                             password: password,
//                                             dob: dob,
//                                             nationality: countryCode,
//                                             gender: gender,
//                                             taxRegNo: taxNumber,
//                                             deviceToken: "242ascsac",
//                                             authToken:authToken_Registration)
//
//        NetworkManager.shared.singleSignOnMethod(mainURL: Config.register_user, parameters: requestModel.asDictionary, methodType: .post, callerView: self) { response in
//
//            if response == nil {
//                self.showAlertView(msg: UCService_Error.localized(),theme:.error)
//                return
//            }
//            else {
//
//                let responseData = response as! NSDictionary
//                var loginBase = LoginUserBaseModel()
//                loginBase = LoginUserBaseModel(JSON: responseData as! [String : Any])!
//                if loginBase.result == 1 {
//                    //Please check address list
//                    self.loginUserModel = loginBase.loginUserDataModel!
//                    let cartShipping = self.loginUserModel.cart_shipping
//                    if cartShipping?.delivery_address?.address_id != nil || cartShipping?.delivery_address?.address_id != ""{
//                        self.cartShipped = self.loginUserModel.cart_shipping!
//                    }
//                    setCustomerData(customerModal: self.loginUserModel)
//
//                    let selectCardObj = RegisterConfirmationVC.instantiate(fromAppStoryboard: .userStoryboard)
//                    selectCardObj.delegate = self
//                    selectCardObj.modalPresentationStyle = .overCurrentContext
//                    self.definesPresentationContext = true
//                    selectCardObj.modalTransitionStyle = .crossDissolve
//                    self.present(selectCardObj, animated: true, completion: nil)
//
//                    return
//                }else{
//                    self.showAlertView(msg: loginBase.msg!,theme:.error)
//                }
//            }
//        }
    }
    
    func openDashboardScreen(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.makeRootViewController()
//        setCustomerData(customerModal: loginUserModel)
//        GlobalObjects.setRefreshHomeScreen(true)
//        self.navigationController?.pushViewController(ucTabarVC, animated: false)
    }
    
    func didSaveAddress(){
        openDashboardScreen()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField  == txtMobile {
            let currentCharacterCount = textField.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 9
        }
        return true
    }
    
    func validation() -> Bool{
        
        
        if txtFname.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterFirstName.localized(),theme:.error)
            return false
        }
        if txtLname.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterLastName.localized(),theme:.error)
            return false
        }
        if txtemail.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterEmailAddress.localized(),theme:.error)
            return false
        }
        if txtemail.text?.isValidEmail == false{
            self.showAlertView(msg:UCEnterValidEmail.localized(),theme: .error)
            return false
        }
        if txtMobile.text?.isEmpty ?? false {
            self.showAlertView(msg: UCEnterValidMobile.localized(),theme:.error)
            return false
        }
        if txtpassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterPassword.localized(),theme: .error)
            return false
        }
        if txtConfpassword.text?.isEmpty ?? false {
            self.showAlertView(msg:UCEnterConfirmPassword.localized(),theme: .error)
            return false
        }
        if txtpassword.text != txtConfpassword.text {
            self.showAlertView(msg:UCPasswordNotMatch.localized(),theme: .error)
            return false
        }
        
        return true
    }
  
}
