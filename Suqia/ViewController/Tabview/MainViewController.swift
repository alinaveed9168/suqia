//
//  ViewController.swift
//  SwiftRaisedTabDemo
//
//  Created by Kaynine on 9/23/2015.
//  Copyright (c) 2015 Kaynine. All rights reserved.
//

import UIKit

class MainViewController: RoundedTabBarController {
    var isActivated: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        // Insert empty tab item at center index. In this case we have 5 tabs.
//        self.insertEmptyTabItem("", atIndex: 2)
//
//        // Raise the center button with image
//        let img = UIImage(named: "icon_camera")
//        self.addRaisedButton(img, highlightImage: nil, offset: -30.0)
        
        
        // ADD THIS IF CONDITION
             if !isActivated {
                isActivated = true
                 // Insert empty tab item at center index. In this case we have 5 tabs.
                // self.insertEmptyTabItem("", atIndex: 2)

                 // Raise the center button with image
                 let img = UIImage(named: "icon_camera")
                    // self.addRaisedButton(img, highlightImage: nil)
                self.addRaisedButton(img, highlightImage: nil, offset: -30.0)

             }
        

    }
    
    // Handler for raised button
    override func onRaisedButton(_ sender: UIButton!) {
        super.onRaisedButton(sender)
        self.selectedIndex = 2
        print("Raised button tapped")
    }


}



import UIKit

class RoundedTabBarController: RaisedTabBarController {

  override func viewDidLoad() {
    super.viewDidLoad()
    
        let layer = CAShapeLayer()
//           layer.path = UIBezierPath(roundedRect: CGRect(x: 10, y: self.tabBar.bounds.minY + 5, width: self.tabBar.bounds.width - 20, height: self.tabBar.bounds.height - 25), cornerRadius: (self.tabBar.frame.width/2)).cgPath
            layer.path = UIBezierPath(roundedRect: CGRect(x: 10, y: self.tabBar.bounds.minY + 5, width: self.tabBar.bounds.width - 20, height: self.tabBar.bounds.height - 25), cornerRadius: 8).cgPath
           layer.shadowColor = UIColor.lightGray.cgColor
           layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
           layer.shadowRadius = 25.0
           layer.shadowOpacity = 0.3
           layer.borderWidth = 1.0
           layer.opacity = 1.0
           layer.isHidden = false
           layer.masksToBounds = false
           layer.fillColor = UIColor.white.cgColor
           self.tabBar.layer.insertSublayer(layer, at: 0)
    
    if let items = self.tabBar.items {
      items.forEach { item in item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: -25, right: 0) }
    }

    self.tabBar.itemWidth = 40.0
    self.tabBar.itemPositioning = .centered
  }
}
