//
//  LanguageSelectViewController.swift
//  unionCoop
//
//  Created by Union Coop on 6/18/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

//var ucTabarVC = CustomTabbarVC.instantiate(fromAppStoryboard: .mainStoryboard)
//var authModal = GetAuthModal()

class SplashScreenVC: UIViewController {
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var LanguageView: UIView!
    @IBOutlet weak var btnEnLanguage: UIButton!
    @IBOutlet weak var btnArLanguage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
        #else
        if isDeviceJailbroken {
            let cardObj = JailbreakView()
            view.addSubview(cardObj!)
            return;
        }
        #endif
        

        // if Yes Hide button and do rest work
        // if Not show buton to select the language
        // POPUP screen for service under maintanance and will be manage from firebase
        
        if UserDefaults.standard.value(forKey: "intlangSelected") == nil {
            LanguageView.isHidden = false
        }else{
            self.checkIfRegisteredAndNext()
        }
    }
    

    @IBAction func englishSelected() {

        MOLH.setLanguageTo("en")
        UserDefaults.standard.set("en", forKey: "strlangSelected")
        UserDefaults.standard.set("1", forKey: "intlangSelected")
        UserDefaults.standard.set(NSNumber(value: true), forKey:UCDO_NOT_Prefered_Language)
        UserDefaults.standard.synchronize()
        Localize.setCurrentLanguage("en")
        
        MOLH.shared.activate(true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText  = UCDone.localized()

            self.checkIfRegisteredAndNext()
}
    
    @IBAction func arabicSelected() {
        
        MOLH.setLanguageTo("ar")
        UserDefaults.standard.set("ar", forKey: "strlangSelected")
        UserDefaults.standard.set("0", forKey: "intlangSelected")
        UserDefaults.standard.set(NSNumber(value: true), forKey: UCDO_NOT_Prefered_Language)
        UserDefaults.standard.synchronize()

        MOLH.shared.activate(true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText  = UCDone.localized()
        Localize.setCurrentLanguage("ar")
        self.checkIfRegisteredAndNext()
    }
    
    
    func checkIfRegisteredAndNext(){
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.makeRootViewController()
        //makeRootViewController
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Tabbar", bundle: nil)
//        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
////        let navigationVC = UINavigationController(rootViewController: tableViewController)
////        appdelegate.window!.rootViewController = navigationVC
//
////        let viewController = MainViewController()
////        let navViewController = UINavigationController(rootViewController: viewController)
//        self.navigationController?.navigationBar.isHidden = false
//        self.navigationController?.pushViewController(viewController, animated: false)
//        let areaID = LoginOperation.shared.getGroceryAreaID()
//
//        if((MyUserDefaults.instance.getObject(LoginUserDataModel.self, forKey: UCLoginUserInfo) != nil)){
//            if areaID == nil || areaID == "" {
//                let vc = UserLoginVC.instantiate(fromAppStoryboard: .userStoryboard)
//                self.navigationController?.pushViewController(vc, animated: false)
//            }else{
//                addBGinBottomBAr()
//                self.navigationController?.pushViewController(ucTabarVC, animated: false)
//            }
//        }else{
//            let vc = UserLoginVC.instantiate(fromAppStoryboard: .userStoryboard)
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


//MARK:- Other ApppDelegate Methods
extension AppDelegate {
    
    func makeViewController(uina:UINavigationController) {
        
        window?.rootViewController = uina
        window?.makeKeyAndVisible()
        
    }
    //Make Root View Controller
    func makeRootViewController() {
//        let storyboard = UIStoryboard(name: StoryboardID.main, bundle: nil)
        guard let customTabbarViewController = storyboard.instantiateViewController(withIdentifier: ViewControllerID.customTabbarViewController) as? MainViewController,
            let leftMenuViewController = storyboard.instantiateViewController(withIdentifier: ViewControllerID.leftMenuViewController) as? LeftMenuViewController,
            let rightMenuController = storyboard.instantiateViewController(withIdentifier: ViewControllerID.leftMenuViewController) as? LeftMenuViewController else {
            return
        }
        
        let navigationViewController: UINavigationController = UINavigationController(rootViewController: customTabbarViewController)

        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.sideMenuViewController = SlideMenuController(mainViewController: navigationViewController, leftMenuViewController: leftMenuViewController, rightMenuViewController: rightMenuController)
        
        let lang:String = UserDefaults.standard.value(forKey: "intlangSelected") as! String
        if ( lang == "1") {
            appdelegate.sideMenuViewController.rightPanGesture?.isEnabled = false
            appdelegate.sideMenuViewController.leftPanGesture?.isEnabled = true
        }else{
            appdelegate.sideMenuViewController.rightPanGesture?.isEnabled = true
            appdelegate.sideMenuViewController.leftPanGesture?.isEnabled = false

        }
        //Create Side Menu View Controller with main, left and right view controller
        window?.rootViewController = appdelegate.sideMenuViewController
        window?.makeKeyAndVisible()
    }
}
