 

import Foundation
import ObjectMapper

struct Blocks : Mappable {
	var type : String?
	var title : String?
	var viewMore : String?
    var widgetData : [WidgetData]?
    var sectionData : [MoreSectionData]?
    var desc : String?
    var color_code : String?
    var image : String?
    
    init()
    {
    
    }
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		type <- map["type"]
		title <- map["title"]
        desc <- map["desc"]
		viewMore <- map["viewMore"]
		widgetData <- map["widgetData"]
        sectionData <- map["sectionData"]
        
        color_code <- map["color_code"]
        image <- map["image"]
        
	}

}
