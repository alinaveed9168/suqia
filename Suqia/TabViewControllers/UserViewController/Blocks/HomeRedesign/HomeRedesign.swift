 

import Foundation
import ObjectMapper

struct HomeRedesign : Mappable {
    var code : String?
    var message : String?
    var data : HomeRedesignData?
    init()
    {
        
    }
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

        code <- map["code"]
        message <- map["message"]
		data <- map["data"]
	}

}
