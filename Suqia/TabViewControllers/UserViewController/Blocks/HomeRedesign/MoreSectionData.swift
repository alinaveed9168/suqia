 

import Foundation
import ObjectMapper

struct MoreSectionData : Mappable {
	var deeplink : String?
	var name : String?
	var image : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		deeplink <- map["deeplink"]
		name <- map["name"]
		image <- map["image"]
	}

}
