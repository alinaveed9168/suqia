 

import Foundation
import ObjectMapper

struct HomeRedesignData : Mappable {
	var blocks : [Blocks]?
	var pages : Int?

    init()
    {
        
    }
	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		blocks <- map["blocks"]
		pages <- map["pages"]
	}

}
