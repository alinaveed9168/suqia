 

import Foundation
import ObjectMapper
struct WidgetData : Mappable {
    
    init?(map: Map) {
        
    }
    
    var product_id : String?
    var name : String?
    var sku : String?
    var packing : String?
    var short_description : String?
    var is_grocery_product : String?
    var qty : Int?
    var available_stock : Bool?
    var tamayaz_price : String?
    var is_favourite : Bool?
    var selling_unit : String?
    var udstr4 : String?
    var image : String?
    var is_custom_option_available : Bool?
    var custom_option : [Custom_option]?
    var custom_id : String?
    var label : String?
    var vendor_id : String?
    var vendor_name : String?
    var description : String?
    var deeplink : String?
    var color_code : String?
    var api : String?
    var uom : String?
    var images : [String]?
    var item_id : String?
    var item_qty : String?
    var row_total : String?
    var pro_price : String?
    var option_id : String?
    var seller_name : String?
    var seller_id : String?
    var item_delivery_time : String?
    var magazine_pdf_url : String?
    var min_qty : Float?

    
    var price : String? //always set
    var stash_price : String? // show with strip if available
    var express : Bool?
    var offer_percentage : String?
    
    //===
    var avg_rating : Int?
    var total_review : Int?
    
    var is_login_required : String?
    
    var order : String?
    var offer_text : String?
    
   
    
    //==
    init(){
        
    }
    
    mutating func mapping(map: Map) {

        images <- map["images"]
        description <- map["description"]
        deeplink <- map["deeplink"]
        color_code <- map["color_code"]
        uom <- map["uom"]
        product_id <- map["product_id"]
        name <- map["name"]
        sku <- map["sku"]
        packing <- map["packing"]
        short_description <- map["short_description"]
        is_grocery_product <- map["is_grocery_product"]
        qty <- map["qty"]
        available_stock <- map["available_stock"]
        tamayaz_price <- map["tamayaz_price"]
        is_favourite <- map["is_favourite"]
        selling_unit <- map["selling_unit"]
        udstr4 <- map["udstr4"]
        image <- map["image"]
        is_custom_option_available <- map["is_custom_option_available"]
        custom_option <- map["custom_option"]
        custom_id <- map["custom_id"]
        label <- map["label"]
        vendor_id <- map["vendor_id"]
        vendor_name <- map["vendor_name"]
        api <- map["api"]        
        item_id  <- map["item_id"]
        item_qty  <- map["item_qty"]
        row_total  <- map["row_total"]
        pro_price  <- map["pro_price"]
        option_id  <- map["option_id"]
        seller_name  <- map["seller_name"]
        seller_id  <- map["seller_id"]
        item_delivery_time  <- map["item_delivery_time"]
        magazine_pdf_url <- map["magazine_pdf_url"]
        
        min_qty  <- map["min_qty"]
        avg_rating <- map["avg_rating"]
        total_review <- map["total_review"]

       
        
        stash_price <- map["stash_price"]
        price <- map["price"]
        express <- map["express"]
        offer_percentage <- map["offer_percentage"]
        is_login_required <- map["is_login_required"]
        
        order <- map["order"]
        offer_text <- map["offer_text"]
    }
    
    var dictionaryRepresentation : [String:Any] {
      let mirror = Mirror(reflecting: self)
      let dict = Dictionary(uniqueKeysWithValues: mirror.children.lazy.map({ (label:String?, value:Any) -> (String, Any)? in
        guard let label = label else { return nil }
        return (label, value)
      }).compactMap { $0 })
      return dict
    }

}
