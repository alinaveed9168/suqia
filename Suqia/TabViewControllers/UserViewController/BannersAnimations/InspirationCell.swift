//
//  InspirationCell.swift
//  ExpandingCollectionView
//
//  Created by Vamshi Krishna on 30/04/17.
//  Copyright © 2017 VamshiKrishna. All rights reserved.
//

import UIKit

class InspirationCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var imageCoverView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var timeAndRoomLabel: UILabel!
    
    var inspiration:WidgetData?{
        didSet{
            if let inspiration = inspiration{
//                imageView.image = inspiration.backgroundImage
//                titleLabel.text = inspiration.title
//                timeAndRoomLabel.text = inspiration.roomAndTime
//                speakerLabel.text = inspiration.speaker
                
                var imgUrl = inspiration.image! as String
                imgUrl = imgUrl.makeSpaceToPercent()
                
                let productImg = (imgUrl .count == 0) ? "" :  imgUrl
                imageView.setImageFromURL(urlString: productImg)
                titleLabel.text = inspiration.name
                timeAndRoomLabel.text = inspiration.description

            }
        }
    }
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        // 1
        let standardHeight = UltravisualLayoutConstants.Cell.standardHeight
        let featuredHeight = UltravisualLayoutConstants.Cell.featuredHeight
        
        // 2
        let delta = 1 - ((featuredHeight - frame.height) / (featuredHeight - standardHeight))
        
        // 3
        let minAlpha: CGFloat = 0.3
        let maxAlpha: CGFloat = 0.75
        imageCoverView.alpha = maxAlpha - (delta * (maxAlpha - minAlpha))
        timeAndRoomLabel.alpha = delta
    }
}


