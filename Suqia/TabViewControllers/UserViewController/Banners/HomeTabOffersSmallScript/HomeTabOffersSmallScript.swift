//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit
protocol HomeTabOffersSmallScriptDelegate :class{
    func clickOnClickandCollect()

}
extension UILabel {
    func getSize(constrainedWidth: CGFloat) -> CGSize {
        return systemLayoutSizeFitting(CGSize(width: constrainedWidth, height: UIView.layoutFittingCompressedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
    }
}

class HomeTabOffersSmallScript: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var widgetData : [WidgetData] = []
    var collectionType: Int = 0
    @IBOutlet weak var lblCategory: UILabel!
    var naviController = UINavigationController()
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    weak var editCollectionAddressDelegate: HomeTabOffersSmallScriptDelegate?
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.homeCollectionView.register(UINib(nibName: "CellwithOffersWidgetSmallTitle", bundle: nil), forCellWithReuseIdentifier: "CellwithOffersWidgetSmallTitle")
        setFontFamily(self.topView, andSubviews: true)
//        widgetData.removeAll()
        
        self.homeCollectionView.setNeedsLayout()
        self.homeCollectionView.layoutIfNeeded()
        self.designCollectionType()

    }
    
    func setWidgetDataCollection(dt:[WidgetData],CellsPerRow:CGFloat,height:CGFloat,hideTopView:CGFloat,cT:Int,RD:Blocks)  {
       
        widgetData = dt
        numberOfCellsPerRow = CellsPerRow
        CellsHeight = height
        collectionType = cT
        lblCategory.isHidden = (RD.title == "" || RD.title == nil) ? true : false
        lblDescription.isHidden = (RD.title == "" || RD.title == nil) ? true : false
        
        self.homeCollectionView.reloadData()
        self.myCollectionViewHeight.constant = CellsHeight
        
        let width = self.contentView.frame.width
        let hCate = self.lblCategory.getSize(constrainedWidth:width).height
        let DCate = self.lblDescription.getSize(constrainedWidth:width).height
        self.topViewHeight.constant = hCate + DCate + 50

    }
    
    func designCollectionType()  {
        let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 5, left: 7, bottom: 10, right: 0)
            layout.scrollDirection = .horizontal;
            layout.minimumLineSpacing = 4
            layout.minimumInteritemSpacing = 4
            self.homeCollectionView?.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat((collectionView.frame.size.width / 3) - 8 ), height: CellsHeight)
//        return CGSize(width: 180, height: CellsHeight)

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return widgetData.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        
        if ((context.nextFocusedIndexPath) != nil)  {
            if(!collectionView.isScrollEnabled){
                collectionView.scrollToItem(at: context.nextFocusedIndexPath!, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
            let cell : CellwithOffersWidgetSmallTitle = collectionView.dequeueReusableCell(withReuseIdentifier: "CellwithOffersWidgetSmallTitle", for: indexPath) as! CellwithOffersWidgetSmallTitle
            let RData:WidgetData = widgetData[indexPath.row]
            cell.lblProductName.text = RData.name
            let imageUrl = RData.image == nil ? "" :  RData.image
            cell.imgProduct.setImageFromURL(urlString: imageUrl!)
            //let color_code = RData.color_code ?? UCNoColor
           
//            cell.layer.cornerRadius = 12
            
            return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dataDic:WidgetData = widgetData[indexPath.row]
        
        if(dataDic.deeplink == "click_collect"){
            editCollectionAddressDelegate?.clickOnClickandCollect()
        }else{
            let name = (dataDic.name == nil) ? UCProduct.localized() :  dataDic.name!
            let action = toReturnNavigationController(depplinkString: dataDic.deeplink!, withName: name)
            naviController.pushViewController(action, animated: true)

        }
    }
}
