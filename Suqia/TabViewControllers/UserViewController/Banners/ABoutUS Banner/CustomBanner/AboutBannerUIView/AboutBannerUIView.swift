//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class AboutBannerUIView: UIView{
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var widgetData : [WidgetData] = []
    var collectionType: Int = 0
    
    
    var bannerWidget: Int = 0
    
   
    var naviController = UINavigationController()
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var view: UIView!

    let kCONTENT_XIB_NAME = "AboutBannerUIView"


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
    }
    
    
}



extension AboutBannerUIView :UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
   
    
    
    
    func LoadData(){
        
        homeCollectionView.isPagingEnabled = true
        homeCollectionView.setNeedsLayout()
        homeCollectionView.layoutIfNeeded()
        designCollectionType()
        homeCollectionView.register(UINib(nibName: "CellwithMediumWidgetImage", bundle: nil), forCellWithReuseIdentifier: "CellwithMediumWidgetImage")
        
        homeCollectionView.register(UINib(nibName: "CellwithImageWidget", bundle: nil), forCellWithReuseIdentifier: "CellwithImageWidget")
    }
    
    
    func setWidgetDataCollection(dt:[WidgetData],CellsPerRow:CGFloat,height:CGFloat,hideTopView:CGFloat,cT:Int,RD:Blocks)  {
        
        LoadData()
        widgetData = dt
        numberOfCellsPerRow = CellsPerRow
        CellsHeight = height + 10
        collectionType = cT
        
//        addview.myCollectionViewHeight.constant = height + 40
        homeCollectionView.reloadData()
    }
    
    func designCollectionType()  {
        homeCollectionView.delegate = self
        homeCollectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
//        layout.minimumLineSpacing = 15
//        layout.minimumInteritemSpacing = 15
        if(bannerWidget == 1){
            layout.minimumLineSpacing = 0
            layout.minimumInteritemSpacing = 0
        }else{
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right:5)
        }
        layout.scrollDirection = .horizontal
        homeCollectionView?.collectionViewLayout = layout
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(bannerWidget == 1){
            let sqr = (collectionView.frame.size.width/3)
            return CGSize(width: sqr, height: CellsHeight)
        }
        let sqr = (collectionView.frame.size.width-10)
        return CGSize(width: sqr, height: CellsHeight+10)
       
       
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return widgetData.count
    }
 
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if(bannerWidget == 1){
            
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellwithImageWidget", for: indexPath) as! CellwithImageWidget
            let RData:WidgetData = widgetData[indexPath.row]
            let imageUrl = RData.image == nil ? "" :  RData.image
            cell.imgProduct.setImageFromURL(urlString: imageUrl!)
            cell.lblTitle.text = RData.name
            
            if(indexPath.row == 0 ||  indexPath.row == 2 ){
                cell.setAllVAlues()
            }
            
            return cell
            
        }
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellwithMediumWidgetImage", for: indexPath) as! CellwithMediumWidgetImage
            let RData:WidgetData = widgetData[indexPath.row]
            let imageUrl = RData.image == nil ? "" :  RData.image
            cell.imgProduct.setImageFromURL(urlString: imageUrl!)
            cell.lblTitle.text = RData.name
            cell.lblDesc.text = RData.description
            return cell
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let RData:WidgetData = widgetData[indexPath.row]
//        let name = (RData.name == nil) ? UCStore.localized() :  RData.name!
//        let action = toReturnNavigationController(depplinkString: RData.deeplink!, withName: name)
//        naviController.pushViewController(action, animated: true)
        
    }
}
