//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class topBannerView: UIView{
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var collectionType: Int = 0
    
    var bannerWidget: Int = 0
    
   
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var titlelblb: UILabel!

    @IBOutlet weak var innerView: UIView!

    @IBOutlet weak var view: UIView!

    let kCONTENT_XIB_NAME = "topBannerView"


    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        view.fixInView(self)
    }
    
    
}



extension topBannerView {
    
    func setWidgetDataCollection(dt:aboutUS,CellsPerRow:CGFloat,height:CGFloat,hideTopView:CGFloat,cT:Int,RD:Blocks)  {
        
        numberOfCellsPerRow = CellsPerRow
        CellsHeight = height + 10
        collectionType = cT
        titlelblb.text = dt.Title
        innerView.shadowP(2, height: 3)
    }

}
