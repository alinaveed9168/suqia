//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit

class CellwithMediumWidgetImage: UICollectionViewCell{

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var dataView: UIView!
    
    override func awakeFromNib() {
         super.awakeFromNib()
           
        dataView.addBottomShadow()
        imgView.shadowP(2, height: 3)
        
        imgView.layer.cornerRadius = 8
     }
    /**/

}


extension UIView {
func addBottomShadow() {
    
    layer.masksToBounds = false
       layer.shadowRadius = 2
       layer.shadowOpacity = 1
       layer.shadowColor = UIColor.gray.cgColor
       layer.shadowOffset = CGSize(width: 0 , height:2)
}
}
