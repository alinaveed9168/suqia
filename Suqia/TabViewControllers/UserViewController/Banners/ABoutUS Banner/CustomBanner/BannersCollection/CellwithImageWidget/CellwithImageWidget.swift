//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit

class CellwithImageWidget: UICollectionViewCell{

    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var innerView: UIView!
    
    @IBOutlet weak var imgView: UIView!
    @IBOutlet weak var dataView: UIView!
    
    @IBOutlet weak var LeftTop: NSLayoutConstraint!
    @IBOutlet weak var RightTop: NSLayoutConstraint!
    @IBOutlet weak var LeftBottom: NSLayoutConstraint!
    @IBOutlet weak var RightBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
         super.awakeFromNib()
           
//        dataView.addBottomShadow()
        imgView.shadowP(2, height: 3)
        imgView.layer.cornerRadius = 8
     }
    /**/
    func setAllVAlues(){
        
        LeftTop.constant = 12
         RightTop.constant = 12
        LeftBottom.constant = 12
        RightBottom.constant = 12
    }

}

