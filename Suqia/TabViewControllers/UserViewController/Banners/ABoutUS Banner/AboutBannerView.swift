//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit


class AboutBannerView: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var widgetData : [WidgetData] = []
    var collectionType: Int = 0
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblViewMore: UIButton!
    var naviController = UINavigationController()
    @IBOutlet weak var homeCollectionView: UICollectionView!
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblViewMore.backgroundColor = .clear
        lblViewMore.layer.cornerRadius = 5
        lblViewMore.layer.borderWidth = 1
        lblViewMore.layer.borderColor = UIColor.white.cgColor
        self.homeCollectionView.isPagingEnabled = true
        self.homeCollectionView.setNeedsLayout()
        self.homeCollectionView.layoutIfNeeded()
        self.designCollectionType()

        
        self.homeCollectionView.register(UINib(nibName: "CellwithMediumWidgetImage", bundle: nil), forCellWithReuseIdentifier: "CellwithMediumWidgetImage")
        setFontFamily(self.topView, andSubviews: true)

    }
    
    func setWidgetDataCollection(dt:[WidgetData],CellsPerRow:CGFloat,height:CGFloat,hideTopView:CGFloat,cT:Int,RD:Blocks)  {
        widgetData = dt
        numberOfCellsPerRow = CellsPerRow
        CellsHeight = height + 10
        collectionType = cT
        lblCategory.isHidden = (RD.title == "" || RD.title == nil) ? true : false
        lblViewMore.isHidden = (RD.viewMore == "" || RD.viewMore == nil)  ? true : false
        
        self.myCollectionViewHeight.constant = height + 40
        self.homeCollectionView.reloadData()
        self.topViewHeight.constant = hideTopView
    }
    
    func designCollectionType()  {
        let layout = UICollectionViewFlowLayout()
//        layout.minimumLineSpacing = 15
//        layout.minimumInteritemSpacing = 15
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right:5)
        layout.scrollDirection = .horizontal
        self.homeCollectionView?.collectionViewLayout = layout
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sqr = (collectionView.frame.size.width-10)
        return CGSize(width: sqr, height: CellsHeight+10)
       
       
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return widgetData.count
    }
 
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "CellwithMediumWidgetImage", for: indexPath) as! CellwithMediumWidgetImage
            let RData:WidgetData = widgetData[indexPath.row]
            let imageUrl = RData.image == nil ? "" :  RData.image
            cell.imgProduct.setImageFromURL(urlString: imageUrl!)
            cell.lblTitle.text = RData.name
            cell.lblDesc.text = RData.description
            return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let RData:WidgetData = widgetData[indexPath.row]
//        let name = (RData.name == nil) ? UCStore.localized() :  RData.name!
//        let action = toReturnNavigationController(depplinkString: RData.deeplink!, withName: name)
//        naviController.pushViewController(action, animated: true)
        
    }
}
