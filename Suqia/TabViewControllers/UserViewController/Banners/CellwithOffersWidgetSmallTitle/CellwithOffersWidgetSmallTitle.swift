//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit

class CellwithOffersWidgetSmallTitle: UICollectionViewCell{

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var innerView: UIView!


    override func awakeFromNib() {
         super.awakeFromNib()
        innerView.layer.cornerRadius = 6
        imgProduct.layer.cornerRadius = imgProduct.frame.width/2
        innerView.backgroundColor = .lightText
//        innerView.dropShadow()
        innerView.layer.shadowOffset = .zero
        innerView.layer.shadowRadius = 5
        innerView.layer.shadowOpacity = 0.3
        innerView.layer.shouldRasterize = true
        innerView.layer.rasterizationScale = UIScreen.main.scale
        innerView.layer.shadowPath = CGPath(rect: innerView.bounds,
                                       transform: nil)
        
     }
    /**/
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        // 1
        let standardHeight = 300.0
        let featuredHeight = UltravisualLayoutConstants.Cell.featuredHeight
        
        // 2
        let delta = 1 - ((featuredHeight - frame.height) / (featuredHeight - CGFloat(standardHeight)))
        
        // 3
        let minAlpha: CGFloat = 0.3
        let maxAlpha: CGFloat = 0.75
//        imageCoverView.alpha = maxAlpha - (delta * (maxAlpha - minAlpha))
//        timeAndRoomLabel.alpha = delta
    }

}

