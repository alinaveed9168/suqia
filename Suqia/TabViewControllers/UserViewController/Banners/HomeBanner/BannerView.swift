//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit

class BannerView: UICollectionReusableView{

    var myPromotionsList = [WidgetData]()
    var naviController = UINavigationController()
    let uivController = UIViewController()
    var stringindex = 0;
    var arrString: NSMutableArray = []
    var widgetData : [WidgetData]?
    @IBOutlet var carousel: ZKCarousel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.purple

        // Customize here

     }

     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

     }

    func bannerView(_ bannerView: LCBannerView!, didClickedImageIndex index: Int){
          let dataDic = self.myPromotionsList[index]
          let name = (dataDic.name == nil) ? UCProductList.localized() :  dataDic.name!
          let action = toReturnNavigationController(depplinkString: dataDic.deeplink!, withName: name)
          if(action.deeplinkTitle == action.deeplinkValue){
            action.deeplinkTitle = dataDic.magazine_pdf_url!
          }
           action.magazinePdfUrl = dataDic.magazine_pdf_url!
          naviController.pushViewController(action, animated: true)
          
      }
     
    
    
    func fetchHomeBanner(getBlocks:Blocks)  {
        
        self.carousel.stop()
        self.carousel.slides.removeAll()
        
       let arrBannerImage: NSMutableArray = []
        
        if(getBlocks.widgetData == nil ) { globalPrint(dataToPrint:"WidgetData ======= "); return }
        widgetData = getBlocks.widgetData!
        for widget in getBlocks.widgetData!{
            var imgUrl = widget.image! as String
            imgUrl = imgUrl.makeSpaceToPercent()
            arrBannerImage.add(imgUrl)
            myPromotionsList.append(widget)
            
            let productImg = (imgUrl .count == 0) ? "" :  imgUrl
            let slide = ZKCarouselSlide(image: productImg, title: widget.description)
            self.carousel.slides.append(slide)
        }

        setupCarousel()
                
    }
    
    private func setupCarousel() {
        
        // You can optionally use the 'interval' property to set the timing for automatic slide changes. The default is 1 second.
        self.carousel.interval = 2.5
        
        // OPTIONAL - use this function to automatically start traversing slides.
        self.carousel.start()
        
        // OPTIONAL - use this function to stop automatically traversing slides.
        // self.carousel.stop()
    }
}
