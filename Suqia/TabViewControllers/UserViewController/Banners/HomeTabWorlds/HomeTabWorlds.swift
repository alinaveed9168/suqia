//
//  HomeBannerView.swift
//  unionCoop
//
//  Created by Union Coop on 25/06/2020.
//  Copyright © 2020 Union Coop. All rights reserved.
//

import UIKit
protocol HomeTabWorldsDelegate :class{
    func clickOnClickandCollect()

}


class HomeTabWorlds: UITableViewCell{
    
    
    var numberOfCellsPerRow: CGFloat = 0
    var CellsHeight: CGFloat = 0
    var widgetData : [WidgetData] = []
    var collectionType: Int = 0
    @IBOutlet weak var lblCategory: UILabel!
    var naviController = UINavigationController()
    @IBOutlet weak var myCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    weak var editCollectionAddressDelegate: HomeTabWorldsDelegate?
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        setFontFamily(self.topView, andSubviews: true)

    }
    
    func setWidgetDataCollection(dt:[WidgetData],CellsPerRow:CGFloat,height:CGFloat,hideTopView:CGFloat,cT:Int,RD:Blocks)  {
       
        widgetData = dt
        numberOfCellsPerRow = CellsPerRow
        CellsHeight = height
        collectionType = cT
        lblCategory.isHidden = (RD.title == "" || RD.title == nil) ? true : false
        lblDescription.isHidden = (RD.title == "" || RD.title == nil) ? true : false
        
        self.myCollectionViewHeight.constant = CellsHeight
        
        self.topViewHeight.constant = hideTopView

    }
    
}
