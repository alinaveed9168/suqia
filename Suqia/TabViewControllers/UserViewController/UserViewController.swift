//
//  UserViewController.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 22/06/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

import UIKit


class UserViewController: SideBaseViewController,HomeTabOffersSmallScriptDelegate,GllMainCellDelegate,GllCollectionViewFlowLayoutDelegate {
    let mainScreenWidth = CGFloat(UIScreen.main.bounds.size.width)
    let mainScreenHeight = CGFloat(UIScreen.main.bounds.size.height)
    
    var dashboard_Data = HomeRedesign()
    var stringindex = 1;
    var dashboard_Blocks = [Blocks]()
    var tblCustom: UITableView = UITableView()
    @IBOutlet weak var viewtblCustom: UIView!
    var inspirations = [WidgetData]()
    var backView: UIView!
    var gllCollectionView:UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())

    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.navigationBar.topItem?.title = LeftMenuItems.settingsTab.rawValue
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (self.navigationController?.navigationBar.frame.height ?? 0.0)
        navigationBarHeight = topBarHeight
        
        creatTableView()
    }
    
    func creatTableView()  {
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { [self] _ in
            
           
            
            self.stringindex = 1
            self.fetchCategoriesFromServer()
            
            if let patternImage = UIImage(named: "Pattern") {
                view.backgroundColor = UIColor(patternImage: patternImage)
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func clickOnClickandCollect(){
        globalPrint(dataToPrint:"clickOnClickandCollect")
    }
    
    //MARK:- IBAction Methods
    @IBAction func btnPushViewControllerTapped(_ sender: UIButton) {
        //Push SecondViewController by Segue
    }
    
    
}

extension UserViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.dashboard_Blocks.count == 0){return 0}
        return self.dashboard_Blocks.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var genuineCell = UITableViewCell()
        let pageCountFromServer = self.dashboard_Data.data?.pages
        if((indexPath.row == self.dashboard_Blocks.count) ){
            globalPrint(dataToPrint:"self.dashboard_Blocks.count")
            if(stringindex != pageCountFromServer!){
                stringindex += 1
                self.fetchCategoriesFromServer()
            }
            genuineCell.backgroundColor = .clear
            
            return genuineCell
        }
        globalPrint(dataToPrint:indexPath.row)
        let RedisgnData:Blocks = self.dashboard_Blocks[indexPath.row]
        var tableViewCellIdendifier = ""
        let typeForBlocks = Int(RedisgnData.type!)
        switch typeForBlocks {
        
        case WizardConstant.BANNER_WIDGET:
            tableViewCellIdendifier = "HomeBannerView"
            let cell = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdendifier, for: indexPath) as? HomeBannerView
            cell?.fetchHomeBanner(getBlocks: RedisgnData)
            cell?.naviController = self.navigationController!
            genuineCell = cell!
            break
        case WizardConstant.OFFER_WIDGET_BIG: //show 3 x n View Recipes
            tableViewCellIdendifier = "HomeTabOffersSmallScript"
            let  cell2 = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdendifier, for: indexPath) as? HomeTabOffersSmallScript
            cell2?.lblCategory.text = ((RedisgnData.title == "" ||  RedisgnData.title == nil)) ? "" : RedisgnData.title!
            cell2?.lblDescription.text = ((RedisgnData.desc == "" ||  RedisgnData.desc == nil)) ? "" : RedisgnData.desc!

            let hideView = ((RedisgnData.title == "" ||  RedisgnData.title == nil)
                                && (RedisgnData.viewMore == "" ||  RedisgnData.viewMore == nil)) ? 10 : 38
            cell2?.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 1 ,height:160,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
            cell2?.naviController = self.navigationController!
            cell2?.editCollectionAddressDelegate = self
            cell2?.homeCollectionView.reloadData()
            genuineCell = cell2!
            break
        case WizardConstant.HOME_WORLD: //show 3 x n View Recipes
            tableViewCellIdendifier = "HomeTabWorlds"
            let  cell2 = tableView.dequeueReusableCell(withIdentifier: tableViewCellIdendifier, for: indexPath) as? HomeTabWorlds
            cell2?.lblCategory.text = ((RedisgnData.title == "" ||  RedisgnData.title == nil)) ? "" : RedisgnData.title!
            cell2?.lblDescription.text = ((RedisgnData.desc == "" ||  RedisgnData.desc == nil)) ? "" : RedisgnData.desc!

            let hideView = ((RedisgnData.title == "" ||  RedisgnData.title == nil)
                                && (RedisgnData.viewMore == "" ||  RedisgnData.viewMore == nil)) ? 0 : 38
            cell2?.setWidgetDataCollection(dt: RedisgnData.widgetData!, CellsPerRow: 1 ,height:100,hideTopView:CGFloat(hideView),cT: typeForBlocks!,RD:RedisgnData)
            cell2?.naviController = self.navigationController!
            genuineCell = cell2!
            break
        default:
            break
        }
        genuineCell.backgroundColor = .clear
        genuineCell.selectionStyle = .none
        return genuineCell
        
    }
    func reloadCallerClass(){
        
        self.dashboard_Data = HomeRedesign()
        self.dashboard_Blocks.removeAll()
        tblCustom.reloadData()
        tblCustom.isHidden = true
        tblCustom.separatorStyle = .none
        tblCustom.removeFromSuperview()
        creatTableView()
        globalPrint(dataToPrint:"reloadCallerClass")
        self.stringindex = 1
        self.fetchCategoriesFromServer()
    }
    
    func fetchCategoriesFromServer()  {
      
        let responseData = loadJson(filename: "SampleJson")!
        self.dashboard_Data = HomeRedesign(JSON: responseData as! [String : Any])!
        
        self.tblCustom.isHidden = false
        
        
        if self.dashboard_Data.code ==  "0" {
            self.showAlertView(msg: UCService_Error.localized(), theme: .error)
            return
        }
        if self.dashboard_Data.code ==  "1" {
            if(stringindex == 1) {self.dashboard_Blocks.removeAll()}
                
                if(self.dashboard_Blocks.count > 0){
                    self.dashboard_Blocks.append(contentsOf: (self.dashboard_Data.data?.blocks)!)
                }else{
                    self.dashboard_Blocks = (self.dashboard_Data.data?.blocks)!
                }
                MyUserDefaults.instance.setObject(self.dashboard_Data, forKey: UCDashboardData)
                MyUserDefaults.instance.setObjectArray(self.dashboard_Blocks, forKey: UCDashboardBlock)
           
            let RedisgnData:Blocks = self.dashboard_Blocks[0]
            inspirations = RedisgnData.widgetData!
            self.tblCustom.reloadData()
            

            
            let layout = GllCollectionViewFlowLayout()
            layout.delegate = self
            
            
            layout.setContentSize(UInt(inspirations.count))
            let myREct = CGRect(x: 0, y: 0, width: mainScreenWidth, height: mainScreenHeight)
            gllCollectionView = UICollectionView(frame: myREct, collectionViewLayout: layout)
            gllCollectionView.backgroundColor = .white
            gllCollectionView.register(GllCollectionViewCell.self, forCellWithReuseIdentifier: "myCell")
            

            let float = layout.collectionViewContentSize.height - CGFloat(SC_IMAGEVIEW_HEIGHT) + 32
            let floatHeight = CGFloat(SC_IMAGEVIEW_HEIGHT) - 32
//            backView = UIView(frame: CGRect(x: 0, y: float, width: mainScreenWidth, height: floatHeight))
  
            //This code will run in the main thread:
            var frame = self.tblCustom.frame;
            frame.size.height = self.tblCustom.contentSize.height;
            self.tblCustom.frame = CGRect(x: 0, y: 0, width: self.tblCustom.frame.width,
                                      height: frame.size.height)

//            let fullHeight  =  floatHeight + frame.size.height
            let fullHeight  = 100.0
            backView = UIView(frame: CGRect(x: 0, y: float, width: mainScreenWidth, height:CGFloat(fullHeight)))
            
            let mylbl = UILabel(frame: CGRect(x: 0, y: 0 , width: mainScreenWidth, height:CGFloat(fullHeight)))
            mylbl.text = ".a,jsdbj,fasbdfj,asbdfj,ahsdfjk"
            backView.addSubview(mylbl)

            backView.backgroundColor  = .yellow
            
            
            gllCollectionView.addSubview(backView)
            gllCollectionView.delegate = self
            gllCollectionView.dataSource = self
            viewtblCustom.addSubview(gllCollectionView)
            self.gllCollectionView.reloadData()

        }
    }
    
    
}




extension UserViewController{
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}

extension UserViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
        if(indexPath.row == 0){
            return CGSize(width: mainScreenWidth, height: 0)
        }else if(indexPath.row == 1){
            return CGSize(width: mainScreenWidth, height: CGFloat(CELL_CURRHEIGHT))
        }else{
            return CGSize(width: mainScreenWidth, height: CGFloat(CELL_HEIGHT))
        }
        
       }
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func updateBotteomViewFrame(withMaxY MaxY: CGFloat) {
        
        var rect = backView.frame;
        rect.origin.y = MaxY;
        backView.frame = rect;
        
    }
    
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return inspirations.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell:GllCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! GllCollectionViewCell
       
        cell.delegate = self;
        cell.tag = indexPath.row;
        cell.setIndex(UInt(indexPath.row))
        cell.reset()
        
        
        if (indexPath.row == 0) {
            
        } else {
            
            if(indexPath.row == 1){
                cell.revisePositionAtFirstCell()
            }
            
            
            let RData:WidgetData = inspirations[0]
            let imageUrl = RData.image == nil ? "" :  RData.image
            cell.imageView.setImageFromURL(urlString: imageUrl!)
            
            cell.leftImageView.image = UIImage(named: "Icon_add")
            cell.title.text =  RData.name;
            cell.desc.text =  RData.name;
            cell.mask.backgroundColor = .gray
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
       //GllCollectionViewCell
       let cell = collectionView.cellForItem(at: indexPath) as! GllCollectionViewCell
       collectionView.deselectItem(at: indexPath, animated: true)
       print(cell.title.text);

       
   }
   
//   func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//      if (kind == UICollectionView.elementKindSectionFooter) {
//
//
//          let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FlickrPhotoHeaderView", for: indexPath) as! FlickrPhotoHeaderView
//          // Customize footerView here
//       footerView.textLabel.text = "asm,dhfa,hsdbf"
//          return footerView
//      }
//      fatalError()
//  }

  
}
