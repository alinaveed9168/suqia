//
//  AppDelegate.swift
//  Suqia
//
//  Created by Union Coop on 18/01/2021.


import UIKit
import Firebase
import GoogleMaps
//https://github.com/itsKaynine/SwiftRaisedTab
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let storyboard = UIStoryboard(name: StoryboardID.main, bundle: nil)
    var sideMenuViewController = SlideMenuController()
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if UserDefaults.standard.value(forKey: "firstTime") == nil {
            NotificationCenter.default.removeObserver(self)
            
            if let bundleID = Bundle.main.bundleIdentifier {
                UserDefaults.standard.removePersistentDomain(forName: bundleID)
            }
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            UserDefaults.standard.removeObject(forKey: "TamayazUSERID")
            UserDefaults.standard.setValue(nil, forKey: "TamayazUSERID")
            resetDefaults()
            Utility.UtilityMethod.removeAllSavedInformation()
            UserDefaults.standard.synchronize()
            UserDefaults.standard.setValue("Delete", forKey: "firstTime")
        }
        
                
        _ = LocationManager.shared
        Messaging.messaging().delegate = self

//        FirebaseApp.configure()

        if UserDefaults.standard.value(forKey: "strlangSelected") == nil {
            MOLHLanguage.setDefaultLanguage("en")
            Localize.setCurrentLanguage("en")
        }else{
            let lang = UserDefaults.standard.value(forKey: "strlangSelected")
            MOLHLanguage.setDefaultLanguage(lang as! String)
            Localize.setCurrentLanguage(lang as! String)
        }
        
        LocationManager.start()
        MOLH.shared.activate(true)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText  = UCDone.localized()
        GMSServices.provideAPIKey(ConstantsURL.googleApiKey)

        let pushManager = PushNotificationManager(userID: "currently_logged_in_user_id")
        pushManager.registerForPushNotifications()

      

        UIFont.overrideDefaultTypography()
        return true
    }

    // MARK: UISceneSession Lifecycle

//    @available(iOS 13.0, *)
//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    @available(iOS 13.0, *)
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func applicationDidBecomeActive(_ application: UIApplication) {

        LocationManager.start()
        callWebServiceToConfig()
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    func applicationDidEnterBackground(_ application: UIApplication) {
        LocationManager.stop()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {

    }
}

extension UINavigationController {

 func setNavigationBarColor(color : UIColor){
        self.navigationBar.barTintColor = color
    }
}
