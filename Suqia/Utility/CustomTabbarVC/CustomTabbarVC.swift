//
//  BATabBarController.swift
//  unionCoop
//
//  Created by Union Coop on 6/26/19.
//  Copyright © 2019 Union Coop. All rights reserved.
//


import UIKit

class CustomTabbarVC: UIViewController {
    let testController = BATabBarController()
    let vc1 = HomeMenuViewController.instantiate(fromAppStoryboard: .mainStoryboard)
    let vc2 = CategoryViewController.instantiate(fromAppStoryboard: .mainStoryboard)
    let vc3 = GeneralShareTradingViewController.instantiate(fromAppStoryboard: .shStoryboard)
    let vc4 = MyCartViewController.instantiate(fromAppStoryboard: .mainStoryboard)
    let vc5 = MoreViewController.instantiate(fromAppStoryboard: .mainStoryboard)

   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        var tabBarItem, tabBarItem2, tabBarItem3,tabBarItem4,tabBarItem5: BATabBarItem
        
        let option1 = NSMutableAttributedString(string: UCHome.localized())
        let option2 = NSMutableAttributedString(string: UCCategories.localized())
        let option3 = NSMutableAttributedString(string: UCShareholder.localized())
        let option4 = NSMutableAttributedString(string: UCCart.localized())
        let option5 = NSMutableAttributedString(string: UCMore.localized())
         
        option1.addAttribute(.foregroundColor, value:ucsGreenColor, range: NSRange(location: 0, length: option1.length))
        option2.addAttribute(.foregroundColor, value: ucsGreenColor, range: NSRange(location: 0, length: option2.length))
        option3.addAttribute(.foregroundColor, value: ucsGreenColor, range: NSRange(location: 0, length: option3.length))
        option4.addAttribute(.foregroundColor, value: ucsGreenColor, range: NSRange(location: 0, length: option4.length))
        option5.addAttribute(.foregroundColor, value: ucsGreenColor, range: NSRange(location: 0, length: option5.length))
        
        tabBarItem  = BATabBarItem(image: UIImage(named: "home")!, selectedImage: UIImage(named: "home_selected")!, title: option1)
        tabBarItem2 = BATabBarItem(image: UIImage(named: "category")!, selectedImage: UIImage(named: "category_selected")!, title: option2)
        tabBarItem3 = BATabBarItem(image: UIImage(named: "shareholder")!, selectedImage: UIImage(named: "shareholder_selected")!, title: option3)
        tabBarItem4 = BATabBarItem(image: UIImage(named: "cart")!, selectedImage: UIImage(named: "cart_selected")!, title: option4)
        tabBarItem5 = BATabBarItem(image: UIImage(named: "more")!, selectedImage: UIImage(named: "more_selected")!, title: option5)
        
        //let badge = BATabBarBadge(value:0, badgeColor: .red)
        //tabBarItem4.badge = badge
                

        let HomeNC = UINavigationController(rootViewController: vc1)
        let CategoryNC = UINavigationController(rootViewController: vc2)
        let ShareholderNC = UINavigationController(rootViewController: vc3)
        let MyCartNC = UINavigationController(rootViewController: vc4)
        let MoreNC = UINavigationController(rootViewController: vc5)
        
        
        testController.viewControllers = [HomeNC, CategoryNC, ShareholderNC,MyCartNC,MoreNC]
        testController.tabBarItems = [tabBarItem,tabBarItem2,tabBarItem3,tabBarItem4,tabBarItem5]
        testController.delegate = self
        
        //OPTIONAL SETTINGS
        
        //initial view controller
        //        testController.initialViewController = vc2
        
        //tab bar background color example
        testController.tabBarBackgroundColor = .white
        
        //tab bar item stroke color example
        testController.tabBarItemStrokeColor = ucsGreenColor
        
        //Tab bar line width example
        //testController.tabBarItemLineWidth = 9.0
                
        //Hides the tab bar when true
        //testController.hidesBottomBarWhenPushed = true
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { _ in
            self.view.addSubview(self.testController.view)
            globalPrint(dataToPrint:self.testController.view.frame.height)
            globalPrint(dataToPrint:self.testController.tabBar!.frame.height)

            
        }
    }
    
}

extension CustomTabbarVC: BATabBarControllerDelegate {
    func tabBarController(_ tabBarController: BATabBarController, didSelect: UIViewController) {
        globalPrint(dataToPrint:didSelect)
        
        if(didSelect.isKind(of: UINavigationController.self)){
            globalPrint(dataToPrint:"UINavigationController")
            
            let selectNavController = didSelect as! UINavigationController
            let selectViewController = selectNavController.rootViewController!
            selectNavController.popToRootViewController(animated: false)

             if(selectViewController.isKind(of: MoreViewController.self)){
                selectViewController.viewWillAppear(true)
            }else if(selectViewController.isKind(of: MyCartViewController.self)){
                selectViewController.viewWillAppear(true)
            }
            else if(selectViewController.isKind(of: CategoryViewController.self)){
                selectViewController.viewWillAppear(true)
            }
            else if(selectViewController.isKind(of: HomeMenuViewController.self)){
                selectViewController.viewWillAppear(true)
            }else if(selectViewController.isKind(of: GeneralShareTradingViewController.self)){
                let myConsteoll  = selectViewController as! GeneralShareTradingViewController
                myConsteoll.loadService = true
                myConsteoll.viewWillAppear(true)
                testController.hidesBottomBarWhenPushed = true
            }

        }
        
    }
    
}

import UIKit

extension UINavigationController {

    var rootViewController: UIViewController? {
        return viewControllers.first
    }

}
