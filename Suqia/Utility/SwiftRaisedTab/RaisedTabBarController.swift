//
//  RaisedTabBarController.swift
//  SwiftRaisedTabDemo
//
//  Created by Kaynine on 9/23/2015.
//  Copyright (c) 2015 Kaynine. All rights reserved.
//

import UIKit



open class RaisedTabBarController: UITabBarController {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
//        UITabBar.appearance().tintColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
//        // For WHITE color:
//        UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
//
//        tabBarController?.tabBar.tintColor = UIColor.red
//        tabBarController?.tabBar.unselectedItemTintColor = UIColor.green
    }
    
//    open override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        tabBar.invalidateIntrinsicContentSize()
//    }
    open func insertEmptyTabItem(_ title: String, atIndex: Int) {
        let vc = UIViewController()
        vc.tabBarItem = UITabBarItem(title: title, image: nil, tag: 0)
        vc.tabBarItem.isEnabled = false
        
        self.viewControllers?.insert(vc, at: atIndex)
    }
    
    open func addRaisedButton(_ buttonImage: UIImage?, highlightImage: UIImage?, offset:CGFloat? = nil) {
        if let buttonImage = buttonImage {
            let button = UIButton(type: UIButton.ButtonType.custom)
            button.autoresizingMask = [UIView.AutoresizingMask.flexibleRightMargin, UIView.AutoresizingMask.flexibleLeftMargin, UIView.AutoresizingMask.flexibleBottomMargin, UIView.AutoresizingMask.flexibleTopMargin]
            
            button.frame = CGRect(x: 0.0, y: 0.0, width: buttonImage.size.width, height: buttonImage.size.height)
            button.setBackgroundImage(buttonImage, for: UIControl.State())
            button.setBackgroundImage(highlightImage, for: UIControl.State.highlighted)
            
            let heightDifference = buttonImage.size.height - self.tabBar.frame.size.height
            
            if (heightDifference < 0) {
                button.center = self.tabBar.center
            }
            else {
                var center = self.tabBar.center
                center.y -= heightDifference / 2.0
                
                button.center = center
            }
            
            if offset != nil
            {
                //Add offset
                var center = button.center
                center.y = center.y + offset!
                button.center = center
            }
            
            button.addTarget(self, action: #selector(RaisedTabBarController.onRaisedButton(_:)), for: UIControl.Event.touchUpInside)
            self.view.addSubview(button)
        }
    }
    
    @objc open func onRaisedButton(_ sender: UIButton!) {
        
    }
}
