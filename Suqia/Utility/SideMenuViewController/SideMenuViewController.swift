//
//  SideMenuViewController.swift
//  SSASideMenuExample
//
//  Created by Sebastian Andersen on 20/10/14.
//  Copyright (c) 2015 Sebastian Andersen. All rights reserved.
//

import Foundation
import UIKit
//import Kingfisher

class SideMenuViewController: BaseViewController {
    @IBOutlet weak var tblUsers: UITableView!
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblphoneNumber: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    var usersArray : Array =
        [["title" : "rate_amp_review".localized(),"image": "revenue_icon","id":11]]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        usersArray  =
            [["title": "home".localized(),"image": "my_order_icon","id":0],
             ["title" : "history".localized(),"image": "my_restaurant_icon","id":1],
             ["title" : "my_profile".localized(),"image": "promotions_offers_icon","id":2],
             ["title" : "my_earning".localized(),"image": "revenue_icon","id":3],
             ["title" : "change_language".localized(),"image": "settings_icon","id":4],
             ["title" : "Login".localized(),"image": "logout_icon","id":5]]
        
//        lblname.text = getloginModal().staffName
//        lblphoneNumber.text = getloginModal().staffPhone
//
//        imgProfile.addTapGesture(tapNumber: 1, target: self, action: #selector(openUserProfile))
//        KingfisherManager.shared.cache.clearMemoryCache()
//        KingfisherManager.shared.cache.clearDiskCache()
//        KingfisherManager.shared.cache.cleanExpiredDiskCache()
//
//               imgProfile.addTapGesture(tapNumber: 1, target: self, action: #selector(openUserProfile))
//               KingfisherManager.shared.cache.clearMemoryCache()
//               KingfisherManager.shared.cache.clearDiskCache()
//               KingfisherManager.shared.cache.cleanExpiredDiskCache()
//               var url = URL(string:"")
//               imgProfile.kf.indicatorType = .activity
//               imgProfile.kf.setImage(with: url, placeholder: UIImage(named: "profile"), options: [.memoryCacheExpiration(.expired)],
//                                      progressBlock: { receivedSize, totalSize in
//
//               },
//                                      completionHandler: { result in
//
//               })
        tblUsers.delegate = self
        tblUsers.dataSource = self
        tblUsers.reloadData()
    }
    
    func SelectLanguage()  {
        
        let lang = LoginOperation().getCurrentLanguage()!
        if ( lang == "en") {
            LoginOperation().setCurrentLanguage(lang: "de")
            Localize.setCurrentLanguage("de")
        }else{
            LoginOperation().setCurrentLanguage(lang: "en")
            Localize.setCurrentLanguage("en")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblUsers.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK : TableViewDataSource & Delegate Methods

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        let dict = usersArray[indexPath.row]
        cell.lblTitle?.text = dict["title"] as? String
        cell.imgTitle?.image = UIImage(named: dict["image"] as! String)
        //   cell.imgProfile.addTapGesture(tapNumber: 1, target: self, action: #selector(openUserProfile))
        
        cell.separatorInset = UIEdgeInsets.zero;
        return cell
    }
    @objc func showLoginScreen()  {
           
//           let loginObj = mainUIStoryboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController
//           var CData =  RestaurantOwnerData(object: "")
//           CData.staffID = nil
//           setloginModal(login: CData)
//           let frontVC = revealViewController().frontViewController as? UINavigationController
//           frontVC?.pushViewController(loginObj, animated: false)
           
       }
    @objc func openUserProfile() {
//        let userProfile = mainUIStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//        let frontVC = revealViewController().frontViewController as? UINavigationController
//        frontVC?.pushViewController(userProfile, animated: false)
//        self.revealController .pushFrontViewController(frontVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
//        self.revealViewController().revealToggle(animated: true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.sideMenuViewController.closeLeft()
//        let dict = usersArray[indexPath.row]
//        let idToInt = dict["id"]! as! Int
//        switch  idToInt {
//        case 0:
//            let orderManagement = mainUIStoryboard.instantiateViewController(withIdentifier: "OrderManagementVC") as! OrderManagementVC
//            let frontVC = revealViewController().frontViewController as? UINavigationController
//            frontVC?.pushViewController(orderManagement, animated: false)
//            revealViewController().pushFrontViewController(frontVC, animated: true)
//            break
//        case 1:
//            let myRestObj = mainUIStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
//            let frontVC = revealViewController().frontViewController as? UINavigationController
//            frontVC?.pushViewController(myRestObj, animated: false)
//            self.revealController .pushFrontViewController(frontVC, animated: true)
//            break
//        case 2:
//            let userProfile = mainUIStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
//            userProfile.fromSideMenu  = 1
//            let frontVC = revealViewController().frontViewController as? UINavigationController
//            frontVC?.pushViewController(userProfile, animated: false)
//            self.revealController .pushFrontViewController(frontVC, animated: true)
//
//            break
//        case 3:
//             let myRestObj = mainUIStoryboard.instantiateViewController(withIdentifier: "RevenueVC") as! RevenueVC
//            let frontVC = revealViewController().frontViewController as? UINavigationController
//                                     frontVC?.pushViewController(myRestObj, animated: false)
//                                     self.revealController .pushFrontViewController(frontVC, animated: true)
//                                     break
//
//            case 4:
//
////                let centerVC: SelectLanguageViewController = mainUIStoryboard.instantiateViewController(withIdentifier: "SelectLanguageViewController") as! SelectLanguageViewController
////                      centerVC.fromSettingScreen = 1
////                      let centerNavVC = UINavigationController(rootViewController: centerVC)
////                      centerNavVC.modalPresentationStyle = .fullScreen
////                      self.present(centerNavVC, animated: true) {
////
////                      }
//
//                     let myRestObj = mainUIStoryboard.instantiateViewController(withIdentifier: "SelectLanguageViewController") as! SelectLanguageViewController
//                    myRestObj.fromSettingScreen = 2
//                    let frontVC = revealViewController().frontViewController as? UINavigationController
//                    frontVC?.pushViewController(myRestObj, animated: false)
//                    self.revealController .pushFrontViewController(frontVC, animated: true)
//                    break
//
//
//        case 5:
//            let userIslogin = (getloginModal().staffName == nil) ? true : false
//                       if(userIslogin){
//                          self.showLoginScreen()
//                       }else{
//                      let appearance = SCLAlertView.SCLAppearance(
//                                 kTitleFont: UIFont.Regular(ofSize: 20),
//                                 kTextFont: UIFont.Regular(ofSize: 14),
//                                 kButtonFont: UIFont.Regular(ofSize: 14),
//                                 showCloseButton: false
//                             )
//                             // Initialize SCLAlertView using custom Appearance
//                             let alertView = SCLAlertView(appearance: appearance)
//                             alertView.addButton("yes".localized(), target:self, selector:#selector(showLoginScreen))
//                             alertView.addButton("no".localized()) {
//
//                             }
//                             alertView.showSuccess("app_name".localized(), subTitle: "logout_pop_up".localized())
//                       }
//            break
//        default:
//            break
//        }
    }
}

