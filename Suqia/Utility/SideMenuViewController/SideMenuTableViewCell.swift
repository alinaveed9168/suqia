//
//  SideMenuTableViewCell.swift
//  Market-Survey
//
//  Created by Union Coop on 21/10/2019.
//  Copyright © 2019 Union Coop. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
