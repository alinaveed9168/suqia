//
//  SideBaseViewController.swift
//  TabbarWithSideMenu
//
//  Created by Sunil Prajapati on 22/06/18.
//  Copyright © 2018 sunil.prajapati. All rights reserved.
//

import UIKit

class SideBaseViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Set Left And Right Naviation Bar Items With Image
        addNavBarImage()
        setNavigationBarItem()
        
        
    }
    
    func addNavBarImage() {

        let navController = navigationController!

        let image = UIImage(named: "logonMain.png") //Your logo url here
        let imageView = UIImageView(image: image)

        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height

        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2

        imageView.frame = CGRect(x: bannerX, y: bannerY - 50, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit

        navigationItem.titleView = imageView
    }
    
    func loadJson(filename fileName: String) -> NSDictionary? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let dictionary = object as? NSDictionary {
                    return dictionary
                }
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        return nil
    }
}
/*
 //
 //  SideBaseViewController.swift
 //  TabbarWithSideMenu
 //
 //  Created by Sunil Prajapati on 22/06/18.
 //  Copyright © 2018 sunil.prajapati. All rights reserved.
 //

 import UIKit

 class SideBaseViewController: UIViewController {

     var nameTitle = ""
     var deeplinkTitle = ""
     var deeplinkValue = ""

     var search_type = ""
     var magazinePdfUrl = ""
     var withSubCategoriesTitle = 0
     var withColorCode = ""
     var withSubCategoriesID = ""
     var withShippinType = ""
     
     @objc func onBagdeButtonClick()  {
         
     }
     func setOrderHistory(str:String) {
         UserDefaults.standard.setValue(str, forKey: "TaporderHistory")
     }
     func getOrderHistory() -> String {
         let value = UserDefaults.standard.value(forKey: "TaporderHistory") == nil ? "" : UserDefaults.standard.value(forKey: "TaporderHistory") as! String
         return value
     }
     
     @IBAction func btnHome() {
        // ucTabarVC.testController.tabBar?.selectedTabItem(0, animated: true)
     }
     
     @objc func popBack()  {
         addGradientToNavigationBar()
         self.navigationController?.popViewController(animated: true)
     }

     func addGradientToNavigationBar() {
         
         self.navigationController?.navigationBar.tintColor = .white
         self.navigationController?.navigationBar.titleTextAttributes = [
             NSAttributedString.Key.foregroundColor : UIColor.white
         ]
         let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 38, height: 38))
         imageView.contentMode = .scaleAspectFit
         let images = UIImage(named: "logoTop")
         imageView.image = images
         navigationItem.titleView = imageView
         
         let gradient = CAGradientLayer()
         var gradientFrame = navigationController?.navigationBar.bounds
         gradientFrame?.size.height += UIApplication.shared.statusBarFrame.size.height
         gradient.frame = gradientFrame ?? CGRect.zero
         
         //white
         let topColor = UIColor.white.cgColor
         let middleColor = UIColor.white.cgColor
     
         gradient.colors = [topColor, middleColor].compactMap { $0 }
         navigationController?.navigationBar.setBackgroundImage(image(from: gradient), for: .default)
     }
     
     func addGradientToNavigationBarBlue(){
         
 //        let imageName = (LANG_INT == 1) ? "back_arrow_black" : "backButton_ar"
 //        let customButton =  UIBarButtonItem(image: UIImage(named: imageName), style: .plain, target: self, action:  #selector(self.popBack))
 //        self.navigationItem.setHidesBackButton(true, animated: true)
 //        self.navigationItem.leftBarButtonItem = customButton
 //
 //        navigationItem.titleView = nil
 //        let gradient = CAGradientLayer()
 //        var gradientFrame = navigationController?.navigationBar.bounds
 //        gradientFrame?.size.height += UIApplication.shared.statusBarFrame.size.height
 //        gradient.frame = gradientFrame ?? CGRect.zero
 //
 //        //white
 //
 //        let color = ucsBlueColor.cgColor
 //        gradient.colors = [color, color].compactMap { $0 }
 //        navigationController?.navigationBar.setBackgroundImage(image(from: gradient), for: .default)
 //
     }
     
     
     func image(from layer: CALayer?) -> UIImage? {
         UIGraphicsBeginImageContext((layer?.frame.size)!)
         
         if let context = UIGraphicsGetCurrentContext() {
             layer?.render(in: context)
         }
         let outputImage = UIGraphicsGetImageFromCurrentImageContext()
         
         UIGraphicsEndImageContext()
         
         return outputImage
     }
     func addTopHeaderProperties()  {
 //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(popBack))
 //            bckButton.isUserInteractionEnabled = true
 //            bckButton.addGestureRecognizer(tapGesture)
 //
 //            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
 //                (self.navigationController?.navigationBar.frame.height ?? 0.0)
 //            navBarHeight.constant = topBarHeight
 //            navView.addCornerRadiusWithShadow(color: .lightGray, borderColor: .clear, cornerRadius: 15)
     }
     override func viewDidLoad() {
         super.viewDidLoad()
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
         addGradientToNavigationBar()
         self.navigationController?.navigationBar.isHidden = false
         // Do any additional setup after loading the view.
         setFontFamily(self.view, andSubviews: true)
     }
   
     func addNavBarImage() {

         let navController = navigationController!

         let image = UIImage(named: "logonMain.png") //Your logo url here
         let imageView = UIImageView(image: image)

         let bannerWidth = navController.navigationBar.frame.size.width
         let bannerHeight = navController.navigationBar.frame.size.height

         let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
         let bannerY = bannerHeight / 2 - (image?.size.height)! / 2

         imageView.frame = CGRect(x: bannerX, y: bannerY - 50, width: bannerWidth, height: bannerHeight)
         imageView.contentMode = .scaleAspectFit

         navigationItem.titleView = imageView
     }
     
     override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         self.navigationController?.navigationBar.isHidden = true
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
         setFontFamily(self.view, andSubviews: true)
         
 //        addNavBarImage()
 //        setNavigationBarItem()
         
 //        if UIApplication.topMostViewController() is HomeMenuViewController{
 //            addGradientToNavigationBar()
 //        }else{
 //            addGradientToNavigationBarBlue()
 //        }
     }
     
     /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destination.
         // Pass the selected object to the new view controller.
     }
     */
     var noDataLotti = AnimationView()
     func loadNodata(Loader:UIViewController,withName:String) {
         noDataLotti = AnimationView(name: withName)
         noDataLotti.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
         noDataLotti.center = Loader.view.center
         noDataLotti.contentMode = .scaleAspectFill
         noDataLotti.animationSpeed = 0.8
         noDataLotti.backgroundColor = .white
         noDataLotti.layer.cornerRadius = 10
         noDataLotti.loopMode = .loop
         noDataLotti.play()
         Loader.view.addSubview(noDataLotti)
     }


 }

 */
