//
//  LeftMenuViewController.swift
//  SSASideMenuExample
//
//  Created by Sebastian Andersen on 20/10/14.
//  Copyright (c) 2015 Sebastian Andersen. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuViewController: UIViewController {
    @IBOutlet weak var tableLeftMenu: UITableView!
    @IBOutlet weak var lblname: UILabel!
       @IBOutlet weak var lblphoneNumber: UILabel!
       @IBOutlet weak var imgProfile: UIImageView!
    
    var usersArray : Array =
        [["title": "About_Us".localized(),"image": "imagename","id":1],
         ["title" : "Our_Mission".localized(),"image": "imagename","id":2],
         ["title" : "Our_Vision".localized(),"image": "imagename","id":3],
         ["title" : "Our_Programmes".localized(),"image": "imagename","id":4],
         ["title" : "Our_Partners".localized(),"image": "imagename","id":5],
         ["title" : "Innovation_Water".localized(),"image": "imagename","id":6],
         ["title" : "Contact_US".localized(),"image": "imagename","id":11],
         ["title" : "Login".localized(),"image": "imagename","id":13]]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        usersArray  =
            [["title": "About_Us".localized(),"image": "About_Us","id":1],
             ["title" : "Our_Mission".localized(),"image": "Our_Mission","id":2],
             ["title" : "Our_Vision".localized(),"image": "Our_Vision","id":3],
             ["title" : "Our_Programmes".localized(),"image": "Our_Programmes","id":4],
             ["title" : "Our_Partners".localized(),"image": "Our_Partners","id":5],
             ["title" : "Innovation_Water".localized(),"image": "Innovation_Water","id":6],
             ["title" : "Contact_US".localized(),"image": "Contact_US","id":11],
             ["title" : "Login".localized(),"image": "Logout","id":13]]
        
        lblname.text = ""
        lblphoneNumber.text = ""
        
        self.tableLeftMenu.register(UITableViewCell.self, forCellReuseIdentifier: "SideMenuTableViewCell")

        
        tableLeftMenu.reloadData()
    }
    
    func SelectLanguage()  {
        
        let lang = LoginOperation().getCurrentLanguage()!
        if ( lang == "en") {
            LoginOperation().setCurrentLanguage(lang: "ar")
            Localize.setCurrentLanguage("ar")
        }else{
            LoginOperation().setCurrentLanguage(lang: "en")
            Localize.setCurrentLanguage("en")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableLeftMenu.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


// MARK : TableViewDataSource & Delegate Methods

extension LeftMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        let dict = usersArray[indexPath.row]
        cell.lblTitle?.text = dict["title"] as? String
        cell.separatorInset = UIEdgeInsets.zero;
        tableView.separatorStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.sideMenuViewController.closeLeft()
        appdelegate.sideMenuViewController.closeRight()
        
        switch indexPath.row {
        case 0:
            
            //
//            self.revealViewController().revealToggle(animated: true)
//
            let orderManagement = mainUIStoryboard.instantiateViewController(withIdentifier: "AboutUSVC") as! AboutUSVC
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.sideMenuViewController.present(orderManagement, animated: true, completion: nil)
            
//            let frontVC = revealViewController().frontViewController as? UINavigationController
//            frontVC?.pushViewController(orderManagement, animated: false)
//            revealViewController().pushFrontViewController(frontVC, animated: true)
            break
//        case 1:
//            self.revealViewController().revealToggle(animated: true)
//
//            //            SelectLanguage()
//            //            self.revealViewController().revealToggle(animated: true)
//  
//            let sw = mainUIStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//            self.view.window?.rootViewController = sw
//            let destinationController = AddRestaurantUIStoryboard.instantiateViewController(withIdentifier: "MyRestaurantVC") as! MyRestaurantVC
//            let navigationController = UINavigationController(rootViewController: destinationController)
//            sw.pushFrontViewController(navigationController, animated: true)
//
//            
////            let sw = mainUIStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
////            self.view.window?.rootViewController = sw
////            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "LoginController") as! LoginController
////            let navigationController = UINavigationController(rootViewController: destinationController)
////            sw.pushFrontViewController(navigationController, animated: true)
            break
            
            case 7:
                self.showLoginScreen()
//
//                let userIslogin = (getloginModal().staffName == nil) ? true : false
//                           if(userIslogin){
//                              self.showLoginScreen()
//                           }else{
//                          let appearance = SCLAlertView.SCLAppearance(
//                                     kTitleFont: UIFont.Regular(ofSize: 20),
//                                     kTextFont: UIFont.Regular(ofSize: 14),
//                                     kButtonFont: UIFont.Regular(ofSize: 14),
//                                     showCloseButton: false
//                                 )
//                                 // Initialize SCLAlertView using custom Appearance
//                                 let alertView = SCLAlertView(appearance: appearance)
//                                 alertView.addButton("yes".localized(), target:self, selector:#selector(showLoginScreen))
//                                 alertView.addButton("no".localized()) {
//
//                                 }
//                                 alertView.showSuccess("app_name".localized(), subTitle: "logout_pop_up".localized())
//                           }
                break
        default:
            break
        }
    }
    
    @objc func showLoginScreen()  {
           
           let loginObj = tabbartUIstoryboard.instantiateViewController(withIdentifier: "UserLoginVC") as! UserLoginVC
//           var CData =  RestaurantOwnerData(object: "")
//           CData.staffID = nil
//           setloginModal(login: CData)
//           let frontVC = revealViewController().frontViewController as? UINavigationController
//           frontVC?.pushViewController(loginObj, animated: false)
//        self.present(loginObj, animated: true, completion: nil)
        
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let naviController = UINavigationController()
        appdelegate.makeViewController(uina: naviController)
        naviController.pushViewController(loginObj, animated: true)
//        let appdelegate = UIApplication.shared.delegate as! AppDelegate
//        appdelegate.sideMenuViewController.navigationController?.pushViewController(loginObj,
//                                                                                    animated: true)
           
       }
}

